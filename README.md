<img src="https://code.freedombone.net/bashrc/argograph/raw/master/img/argograph.png?raw=true" width=500/>

A command line utility for generating oceanographic graphs from the Argo observation system. It is intended for educational or research purposes.


Installation
------------

On a Debian based system:

``` bash
sudo apt-get install build-essential gnuplot netcdf-bin
```

Or on Arch/Parabola:

``` bash
sudo pacman -S gnuplot netcdf-cxx
```

Then compile and install with:

``` bash
make
sudo make install
```


Usage
-----

For details of usage see the man page.

``` bash
man argograph
```
