/*
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "utils.h"

string utils::string_to_lower(string str)
{
    const char* ch = str.c_str();
    string s = "";
    for (int i = 0; i < (int)str.size(); i++) {
        s += tolower(ch[i]);
    }
    return s;
}

void utils::parse_string(string str, vector<string> &v)
{
    const char* ch = str.c_str();
    string s = "";
    for (int i = 0; i < (int)str.size(); i++) {
        if (!((ch[i] == '|') || (ch[i] == ','))) {
            if (!((ch[i] == ' ') && (s == ""))) {
                s += tolower(ch[i]);
            }
        }
        else {
            if (s != "") {
                v.push_back(s);
                s = "";
            }
        }
    }
    if (s != "") {
        if (s != "") {
            v.push_back(s);
        }
    }
}

void utils::parse_string(string str, vector<float> &v)
{
    const char* ch = str.c_str();
    string s = "";
    bool negate = false;
    for (int i = 0; i < (int)str.size(); i++) {
        if (!((ch[i] == '|') || (ch[i] == ','))) {
            if (!((ch[i] == ' ') && (s == ""))) {
                if ((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'E')) negate = true;
                if (!((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'E') ||
                      (toupper(ch[i]) == 'N') || (toupper(ch[i]) == 'W'))) {
                    s += tolower(ch[i]);
                }
            }
        }
        else {
            if (s != "") {
                if (!negate)
                    v.push_back(atof(s.c_str()));
                else
                    v.push_back(-atof(s.c_str()));
                s = "";
                negate = false;
            }
        }
    }
    if (s != "") {
        if (s != "") {
            if (!negate)
                v.push_back(atof(s.c_str()));
            else
                v.push_back(-atof(s.c_str()));
        }
    }
}

void utils::parse_latitude(string str, float &min, float &max)
{
    const char* ch = str.c_str();
    string s = "";
    int ctr = 0;
    bool negate = false;
    for (int i = 0; i < (int)str.size(); i++) {
        if (!((ch[i] == '|') || (ch[i] == ','))) {
            if (!((ch[i] == ' ') && (s == ""))) {
                if ((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'E')) negate = true;
                if (!((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'E') ||
                      (toupper(ch[i]) == 'N') || (toupper(ch[i]) == 'W'))) {
                    s += tolower(ch[i]);
                }
            }
        }
        else {
            if (s != "") {
                float v = 0;
                if (!negate)
                    v = atof(s.c_str());
                else
                    v = -atof(s.c_str());
                if (ctr == 0)
                    min = v;
                else
                    max = v;
                s = "";
                negate = false;
                ctr++;
            }
        }
    }
    if (s != "") {
        float v = 0;
        if (!negate)
            v = atof(s.c_str());
        else
            v = -atof(s.c_str());
        if (ctr == 0)
            min = v;
        else
            max = v;
    }
}

void utils::parse_view(string str, float &longitude, float &latitude)
{
    const char* ch = str.c_str();
    string s = "";
    int ctr = 0;
    int north=-1;
    bool negate = false;
    for (int i = 0; i < (int)str.size(); i++) {
        if (!((ch[i] == '|') || (ch[i] == ','))) {
            if (!((ch[i] == ' ') && (s == ""))) {
                if ((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'E')) {
                    negate = true;
                }
                if (!((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'E') ||
                      (toupper(ch[i]) == 'N') || (toupper(ch[i]) == 'W'))) {
                    s += tolower(ch[i]);
                }
                if ((toupper(ch[i]) == 'S') || (toupper(ch[i]) == 'N')) {
                    north=1;
                }
                if ((toupper(ch[i]) == 'E') || (toupper(ch[i]) == 'W')) {
                    north=0;
                }

            }
        }
        else {
            if (s != "") {
                float v = 0;
                if (!negate)
                    v = atof(s.c_str());
                else
                    v = -atof(s.c_str());
                if (north==-1) {
                    if (ctr == 0)
                        longitude = v;
                    else
                        latitude = v;
                }
                else {
                    if (north==1) {
                        latitude = v;
                    }
                    else {
                        longitude = v;
                    }
                }
                s = "";
                negate = false;
                ctr++;
                north=-1;
            }
        }
    }
    if (s != "") {
        float v = 0;
        if (!negate)
            v = atof(s.c_str());
        else
            v = -atof(s.c_str());
        if (north==-1) {
            if (ctr == 0)
                longitude = v;
            else
                latitude = v;
        }
        else {
            if (north==1) {
                latitude = v;
            }
            else {
                longitude = v;
            }
        }
    }
}

void utils::parse_range(string str, float &min, float &max)
{
    const char* ch = str.c_str();
    string s = "";
    int ctr = 0;
    for (int i = 0; i < (int)str.size(); i++) {
        if (!((ch[i] == '|') || (ch[i] == ','))) {
            if (!((ch[i] == ' ') && (s == ""))) {
                s += tolower(ch[i]);
            }
        }
        else {
            if (s != "") {
                float v = 0;
                v = atof(s.c_str());
                if (ctr == 0)
                    min = v;
                else
                    max = v;
                s = "";
                ctr++;
            }
        }
    }
    if (s != "") {
        float v = 0;
        v = atof(s.c_str());
        if (ctr == 0)
            min = v;
        else
            max = v;
    }
}

string utils::append_time_to_title(string title, int start_year, int end_year, int month)
{
    char str[256];
    string monthstr[] = {
        "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
    };

    title += " for ";

    if ((month>=1) && (month<=12)) {
        title += monthstr[month-1] + " ";
    }

    if (start_year==end_year) {
        sprintf(str,"%d",start_year);
    }
    else {
        sprintf(str,"%d-%d",start_year,end_year);
    }
    title += string(str);

    return title;
}

string utils::append_area_to_title(string title, vector<float> &area)
{
    if (area.size()==4) {
        if ((area[1] == 0.0f) && (area[3] == 0.0f)) {
            title += " between latitudes ";
            char str[10];
            sprintf(str, "%.1f", fabs(area[0]));
            if (area[0] >= 0) {
                title += string(str) + "N - ";
            }
            else {
                title += string(str) + "S - ";
            }
            sprintf(str, "%.1f", fabs(area[2]));
            if (area[2] >= 0) {
                title += string(str) + "N,";
            }
            else {
                title += string(str) + "S,";
            }
        }
        else {
            title += " within geographical area ";
            char str[10];
            sprintf(str, "%.1f", fabs(area[0]));
            if (area[0] >= 0) {
                title += string(str) + "N,";
            }
            else {
                title += string(str) + "S,";
            }
            sprintf(str, "%.1f", fabs(area[1]));
            if (area[1] >= 0) {
                title += string(str) + "W - ";
            }
            else {
                title += string(str) + "E - ";
            }

            sprintf(str, "%.1f", fabs(area[2]));
            if (area[2] >= 0) {
                title += string(str) + "N,";
            }
            else {
                title += string(str) + "S,";
            }
            sprintf(str, "%.1f", fabs(area[3]));
            if (area[3] >= 0) {
                title += string(str) + "W";
            }
            else {
                title += string(str) + "E";
            }
        }
    }

    return title;
}

// Returns the filename with a number appended to it
string utils::append_number_to_filename(string filename, int number)
{
    const char * str = filename.c_str();
    int pos, i;
    string result = "";
    char numberstr[128];

    for (pos = (int)strlen(str)-1; pos > 0; pos--) {
        if (str[pos] == '.') {
            break;
        }
    }
    if (pos == 0) return filename;

    sprintf(numberstr,"%d", number);

    for (i = 0; i < pos; i++) {
        result += str[i];
    }
    for (i = 0; i < (int)strlen(numberstr); i++) {
        result += numberstr[i];
    }
    for (i = pos; i < (int)strlen(str); i++) {
        result += str[i];
    }
    return result;
}
