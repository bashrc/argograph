/*
    plot ocean data using gnuplot
    Copyright (C) 2013-2015 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GNUPLOT_H_
#define GNUPLOT_H_

#define ENTITY_COUNTRY 0
#define ENTITY_STATION 1

#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "argodata.h"
#include "gridcell.h"
#include "globalgrid.h"
#include "utils.h"

using namespace std;

class gnuplot {
private:
    static void save_world_map(string filename);

public:
    static int plot_data_centres(
        string plot_script_filename,
        string plot_data_filename,
        string image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        int start_year,
        int end_year,
        int image_width,
        int image_height);
    static int plot_active_platforms(
        string plot_script_filename,
        string plot_data_filename,
        string image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        int start_year,
        int end_year,
        int image_width,
        int image_height);
    static int plot_profile(
        string plot_script_filename,
        string plot_data_filename,
        string image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<gridcell> &grid,
        int start_year,
        int end_year,
        int image_width,
        int image_height,
        float year_step,
        int graph_type,
        float max_pressure,
        int plot_change);

    static int plot_averages(
         string plot_script_filename,
         string plot_data_filename,
         string image_filename,
         string title,
         string subtitle,
         float subtitle_indent,
         vector<gridcell> &grid,
         int start_year,
         int end_year,
         bool show_running_average,
         int image_width,
         int image_height,
         float pressure_step,
         float max_pressure,
         int plot_change);

    static int plot_anomalies(
        string plot_script_filename,
        string plot_data_filename,
        string anomalies_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<gridcell> &grid,
        int start_year,
        int end_year,
        int reference_start_year,
        int reference_end_year,
        bool show_minmax,
        bool show_running_average,
        bool best_fit_line,
        int image_width,
        int image_height,
        float pressure,
        int graph_type);

    static int plot_distribution(
        string plot_script_filename,
        string plot_data_filename,
        string anomalies_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<gridcell> &grid,
        int start_year,
        int end_year,
        int year_step,
        int image_width,
        int image_height,
        float pressure);

    static int plot_diurnal(string plot_script_filename,
                            string plot_data_filename,
                            string diurnal_image_filename,
                            string title,
                            string subtitle,
                            float subtitle_indent,
                            vector<gridcell> &grid,
                            int start_year,
                            int end_year,
                            int year_step,
                            int month,
                            int image_width,
                            int image_height,
                            float pressure,
                            int graph_type);

    static int plot_anomalies_world_map(
        string plot_script_filename,
        string plot_data_filename,
        string world_data_filename,
        string world_map_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<gridcell> &grid,
        int year,
        int reference_start_year,
        int reference_end_year,
        bool threed,
        float view_longitude, float view_latitude,
        bool show_cell_centres,
        int image_width,
        int image_height,
        float pressure,
        int graph_type,
        float range_min, float range_max);

    static int plot_averages_world_map(
        string plot_script_filename,
        string plot_data_filename,
        string world_data_filename,
        string world_map_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<gridcell> &grid,
        int start_year,int end_year,
        bool threed,
        float view_longitude, float view_latitude,
        int image_width,
        int image_height,
        float pressure,
        int graph_type,
        float range_min, float range_max);

    static int plot_platforms(
        string plot_script_filename,
        string plot_data_filename,
        string world_data_filename,
        string world_map_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<gridcell> &grid,
        int start_year,int end_year,
        bool threed,
        float view_longitude, float view_latitude,
        int image_width,
        int image_height);

    static int plot_cycles(
        string plot_script_filename,
        string plot_data_filename,
        string cycles_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        int image_width,
        int image_height,
        float max_pressure,
        int graph_type);

    static int plot_latitudes(
        string plot_script_filename,
        string plot_data_filename,
        string latitudes_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        vector<float> &area,
        int image_width,
        int image_height,
        float max_pressure,
        int graph_type,
        float range_min, float range_max,
        int start_year, int end_year, int month);

    static int plot_latitude_anomalies(
        string plot_script_filename,
        string plot_data_filename,
        string latitudes_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        vector<float> &area,
        int image_width,
        int image_height,
        float max_pressure,
        int graph_type,
        int start_year,
        int end_year,
        int reference_start_year,
        int reference_end_year,
        float range_min, float range_max);

    static int plot_currents(
        string plot_script_filename,
        string plot_data_filename,
        string world_data_filename,
        string world_map_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        bool threed,
        float view_longitude, float view_latitude,
        int image_width,
        int image_height,
        int graph_type,
        float range_min, float range_max);
    static int plot_speeds(
        string plot_script_filename,
        string plot_data_filename,
        string world_data_filename,
        string world_map_image_filename,
        string title,
        string subtitle,
        float subtitle_indent,
        vector<argodata> &data,
        bool threed,
        float view_longitude, float view_latitude,
        int image_width,
        int image_height,
        float range_min, float range_max);

    static void graph_averages(vector<gridcell> &grid, string filename,
                               string title, string subtitle,
                               float subtitle_indent,
                               int start_year, int end_year, int month,
                               bool show_running_average,
                               int image_width, int image_height,
                               float pressure_step, float max_pressure,
                               vector<float> &area,
                               int plot_change);

    static void graph_active_platforms(vector<argodata> &data,
                                       string filename,
                                       string title, string subtitle,
                                       float subtitle_indent,
                                       int start_year, int end_year,
                                       int image_width, int image_height,
                                       vector<float> &area);

    static void graph_data_centres(vector<argodata> &data, string filename,
                                   string title, string subtitle,
                                   float subtitle_indent,
                                   int start_year, int end_year,
                                   int image_width, int image_height,
                                   vector<float> &area);

    static void graph_profiles(vector<gridcell> &grid, string filename,
                               string title, string subtitle,
                               float subtitle_indent,
                               int start_year, int end_year, int month,
                               int image_width, int image_height,
                               float year_step, int graph_type,
                               float max_pressure,
                               vector<float> &area,
                               int plot_change);

    static void graph_anomalies(vector<gridcell> &grid, string filename,
                                string title, string subtitle,
                                float subtitle_indent,
                                int start_year, int end_year, int month,
                                int reference_start_year,
                                int reference_end_year,
                                bool show_minmax,
                                bool show_running_average,
                                bool best_fit_line,
                                int image_width, int image_height,
                                float pressure, vector<float> &area,
                                int graph_type);

    static void graph_distribution(vector<gridcell> &grid, string filename,
                                   string title, string subtitle,
                                   float subtitle_indent,
                                   int start_year, int end_year, int month,
                                   int image_width, int image_height,
                                   float pressure, int year_step,
                                   vector<float> &area);

    static void graph_diurnal(vector<gridcell> &grid, string filename,
                              string title, string subtitle,
                              float subtitle_indent,
                              int start_year, int end_year, int month,
                              int image_width, int image_height,
                              float pressure, int year_step,
                              vector<float> &area, int graph_type);

    static void graph_map_averages(vector<gridcell> &grid, string filename,
                                   string title, string subtitle,
                                   float subtitle_indent,
                                   int start_year, int end_year,
                                   bool threed, int month,
                                   float view_longitude, float view_latitude,
                                   int image_width, int image_height,
                                   float pressure, vector<float> &area,
                                   int graph_type,
                                   float range_min, float range_max);

    static void graph_platforms(vector<int> &platform_numbers,
                                vector<gridcell> &grid, string filename,
                                string title, string subtitle,
                                float subtitle_indent,
                                int start_year, int end_year,
                                bool threed, int month,
                                float view_longitude, float view_latitude,
                                int image_width, int image_height,
                                vector<float> &area);

    static void graph_cycles(vector<argodata> &data, string filename,
                             string title, string subtitle,
                             float subtitle_indent,
                             vector<int> &platforms,
                             int image_width, int image_height,
                             vector<float> &area,
                             float max_pressure,
                             int graph_type);

    static void graph_latitudes(vector<argodata> &data, string filename,
                                string title, string subtitle,
                                float subtitle_indent,
                                int image_width, int image_height,
                                vector<float> &area,
                                float max_pressure,
                                int graph_type,
                                int start_year, int end_year, int month,
                                float range_min, float range_max);

    static void graph_latitude_anomalies(vector<argodata> &data,
                                         string filename,
                                         string title, string subtitle,
                                         float subtitle_indent,
                                         int image_width, int image_height,
                                         vector<float> &area,
                                         float max_pressure,
                                         int graph_type,
                                         int start_year, int end_year,
                                         int reference_start_year,
                                         int reference_end_year,
                                         float range_min, float range_max);

    static void graph_map_anomalies(vector<gridcell> &grid, string filename,
                                    string title, string subtitle,
                                    float subtitle_indent,
                                    int image_width, int image_height,
                                    float pressure,
                                    int graph_type,
                                    int year, bool threed,
                                    float view_longitude, float view_latitude,
                                    int reference_start_year,
                                    int reference_end_year,
                                    float range_min, float range_max);

    static void graph_currents(vector<argodata> &data, string filename,
                               string title, string subtitle,
                               float subtitle_indent,
                               bool threed,
                               int start_year, int end_year,
                               float view_longitude, float view_latitude,
                               int image_width, int image_height,
                               int graph_type,
                               float range_min, float range_max);

    static void graph_speeds(vector<argodata> &data, string filename,
                             string title, string subtitle,
                             float subtitle_indent,
                             bool threed,
                             int start_year, int end_year, int month,
                             float view_longitude, float view_latitude,
                             int image_width, int image_height,
                             float range_min, float range_max);


    gnuplot();
    virtual ~gnuplot();
};

#endif /* GNUPLOT_H_ */
