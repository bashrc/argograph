/*
  Data structure for Argo observations
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ARGODATA_H_
#define ARGODATA_H_

#include <string>
#include <vector>

#define MAX_PROFILE_RECORDS 64

using namespace std;

class argodata {
public:

    int platform_number;
    int cycle_number;
    int wmo_inst_type;
    char direction;
    int year, month, day;
    int hour, min;
    float latitude; // degrees north
    float longitude; // degrees east
    char data_centre[4];
    int records;
    float pressure[MAX_PROFILE_RECORDS];
    float temperature[MAX_PROFILE_RECORDS];
    float salinity[MAX_PROFILE_RECORDS];
    float oxygen[MAX_PROFILE_RECORDS];

    argodata();
    virtual ~argodata();
};

#endif /* ARGODATA_H_ */
