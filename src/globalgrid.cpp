/*
    functions for gridding temperature data

    This doesn't use a grid containing squares defined by latitude and
    longitude, as seems to be the standard, but instead distributes
    grid cells evenly across the globe.  This avoids any bias due
    to unequal land surface areas and also the problem of
    singularities at the poles.

    Copyright (C) 2014 Bob Mottram
    bob@freedombone.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "globalgrid.h"

globalgrid::globalgrid() {
}

globalgrid::~globalgrid() {
}

void globalgrid::threed_to_latlong(float x, float y, float z,
                                   float &longitude, float &latitude)
{
    latitude = (float)asin(z) * 180.0f / 3.1415927f;
    longitude = (float)atan2(y, x) * 180.0f / 3.1415927f;
}

/**
 * @brief Converts a lat/long position to a 3D point on a globe
 * @param longitude Longitude
 * @param latitude Latitude
 * @param x Returned x coordinate
 * @param y Returned y coordinate
 * @param z Returned z coordinate
 */
void globalgrid::latlong_to_threed(float longitude, float latitude,
                                   float &x, float &y, float &z)
{
    float lng = longitude*3.1415927f/180.0f;
    float lat = latitude*3.1415927f/180.0f;
    x = (float)(cos(lat) * cos(lng));
    y = (float)(cos(lat) * sin(lng));
    z = (float)sin(lat);
}

/**
 * @brief Produces a simple ascii display of grid coverage
 * @param grid The grid of data
 * @param dimension Dimension of the grid (across)
 */
void globalgrid::show_coverage(vector<gridcell> &grid, int dimension)
{
    for (int latitude=0; latitude<(dimension/2); latitude++) {
        for (int longitude=0; longitude<dimension; longitude++) {
            int y,index = (latitude * dimension)+longitude;
            for (y = 1700; y < 3000; y++) {
                if (grid[index].samples[y].size()>0) {
                    printf("O");
                    break;
                }
            }
            if (y==3000) {
                printf(".");
            }
        }
        printf("\n");
    }
}

/**
 * @brief Checks how many grid cells contain data and produces a warning
 *        if there is not enough coverage
 * @param grid The grid of data
 * @param dimension Dimension of the grid (across)
 */
void globalgrid::check_grid_coverage(vector<gridcell> &grid, int dimension)
{
    bool * grid_hits = new bool[dimension*(dimension/2)];

    for (int i = 0; i < dimension*(dimension/2); i++) {
        grid_hits[i]=false;
    }

    for (int latitude=-89; latitude<90; latitude++) {
        for (int longitude=-179; longitude<180; longitude++) {
            int index = get_closest_grid_cell((float)longitude, (float)latitude, grid);
            grid_hits[index] = true;
        }
    }

    int tot=0;
    for (int i = 0; i < dimension*(dimension/2); i++) {
        if (grid_hits[i]) tot++;
    }

    float coverage = tot*100/(dimension*(dimension/2));
    printf("Grid centre point coverage with closest cell: %.1f%%\n",coverage);
    if (coverage<50) {

        for (int latitude=0; latitude<(dimension/2); latitude++) {
            for (int longitude=0; longitude<dimension; longitude++) {
                int index = (latitude * dimension)+longitude;
                if (grid_hits[index]) {
                    printf("O");
                }
                else {
                    printf(".");
                }
            }
            printf("\n");
        }
        printf("\nERROR: Not enough coverage using get_closest_grid_cell\n");

    }
    delete [] grid_hits;
}

/**
 * @brief Checks the conversion between lat/long and 3D coordinates
 * @param grid The grid of data
 */
void globalgrid::check_latlong_conversion(vector<gridcell> &grid)
{
    float longitude2=0,latitude2=0,x=0,y=0,z=0;
    const float tollerance = 2;

    for (int latitude=-89; latitude<90; latitude++) {
        for (int longitude=-179; longitude<180; longitude++) {
            latlong_to_threed(longitude, latitude,
                              x, y, z);

            threed_to_latlong(x, y, z,
                              longitude2, latitude2);

            if ((latitude2<-90) || (latitude2>90)) {
                printf("ERROR: Latitude out of range: %.1f\n",latitude2);
            }
            if ((longitude2<-180) || (longitude2>180)) {
                printf("ERROR: Longitude out of range: %.1f\n",longitude2);
            }

            float dlong = (float)longitude - longitude2;
            float dlat = (float)latitude - latitude2;
            if (dlong<0) dlong = -dlong;
            if (dlat<0) dlat = -dlat;

            if (dlong>tollerance) {
                dlong = (float)longitude - -longitude2;
                if (dlong<0) dlong = -dlong;
            }

            if (dlat>tollerance) {
                dlat = (float)latitude - -latitude2;
                if (dlat<0) dlat = -dlat;
            }


            if ((dlong>tollerance) || (dlat>tollerance)) {
                if (dlong>tollerance) {
                    printf("Longitude error: |%d => %.2f| = %.2f\n",
                           longitude,longitude2,dlong);
                }
                if (dlat>tollerance) {
                    printf("Latitude error:  |%d => %.2f| = %.2f\n",
                           latitude,latitude2,dlat);
                }
                printf("ERROR: latitude/longitude conversion " \
                       "is not reversible\n");
            }

        }
    }
}

/*
 * @brief  Returns the grid index of the given lat/long point
 * @param longitude Longitude
 * @param latitude Latitude
 * @param grid The grid of data
 * @return grid cell index
 */
int globalgrid:: get_closest_grid_cell(float longitude, float latitude,
                                       vector<gridcell> &grid)
{
    float x=0,y=0,z=0,min_dist = 99999;
    int cell_index = 0;

    latlong_to_threed(longitude, latitude, x, y, z);

    for (int i = 0; i < (int)grid.size(); i++) {
        float dx = x - grid[i].x;
        float dy = y - grid[i].y;
        float dz = z - grid[i].z;
        float dist = (dx*dx) + (dy*dy) + (dz*dz);
        if ((i==0) || (dist < min_dist)) {
            cell_index = i;
            min_dist = dist;
        }
    }
    return cell_index;
}

/*
 * @brief Sets the centre point of each grid cell
 * @param no_of_points The number of grid cells
 * @param grid The grid of data
 */
void globalgrid::points_on_sphere(int no_of_points,
                                  vector<gridcell> &grid)
{
    float inc = 3.1415927f * (3.0f - (float)sqrt(5.0));
    float off = 2.0f / no_of_points;
    float x,y,z,r,phi,latitude=0,longitude=0;

    for (int i = 0; i < no_of_points; i++) {
        y = i * off - 1 + (off /2.0f);
        r = (float)sqrt(1 - y * y);
        phi = i * inc;
        x = (float)cos(phi) * r;
        z = (float)sin(phi) * r;

        threed_to_latlong(x, y, z, longitude, latitude);

        grid[i].x = x;
        grid[i].y = y;
        grid[i].z = z;

        grid[i].latitude = latitude;
        grid[i].longitude = longitude;
    }

    check_point_distribution(grid);
}


/*
 * @brief Checks the distribution of points on the unit sphere
 * @param grid The grid of data
 */
void globalgrid::check_point_distribution(vector<gridcell> &grid)
{
    float average_separation = 0;
    float min_separation = 99999;
    float max_separation = -999999;

    for (int i = 0; i < (int)grid.size(); i++) {
        float min_dist = 999999;
        bool first = true;
        for (int j = 0; j < (int)grid.size(); j++) {
            if (i!=j) {
                float dx = grid[j].x - grid[i].x;
                float dy = grid[j].y - grid[i].y;
                float dz = grid[j].z - grid[i].z;
                float dist = dx*dx + dy*dy + dz*dz;
                if ((first) || (dist < min_dist)) {
                    min_dist = dist;
                    first=false;
                }
            }
        }
        average_separation += min_dist;
        if (min_dist < min_separation) min_separation = min_dist;
        if (min_dist > max_separation) max_separation = min_dist;
    }
    average_separation = (float)sqrt(average_separation / (float)grid.size());
    min_separation = (float)sqrt(min_separation);
    max_separation = (float)sqrt(max_separation);

    printf("  Average Grid Cell Separation: %.3f\n",average_separation);
    printf("  Min Grid Cell Separation: %.3f\n", min_separation);
    printf("  Max Grid Cell Separation: %.3f\n", max_separation);
    if (max_separation-min_separation > 0.02) {
        printf("  ERROR: Grid cells not evenly spaced\n");
    }
}

/*
 * @brief Generates the grid cells
 * @param grid The grid of data
 * @param dimension Dimension of the grid (across)
 */
void globalgrid::generate(vector<gridcell> &grid, int dimension)
{
    printf("Generating grid\n");

    grid.clear();
    for (int x = 0; x < dimension; x++) {
        for (int y = 0; y < dimension/2; y++) {
            gridcell cell;
            grid.push_back(cell);
        }
    }

    // set the 3D centre coordinates of each grid cell
    points_on_sphere(dimension*(dimension/2),grid);

    //check_grid_coverage(grid);
    check_latlong_conversion(grid);
}

/*
 * @brief Loads argo data into grid cells
 * @param data The data points to be inserted
 * @param grid The grid of data
 * @param area The bounding lat/long area to insert data within
 * @param dimension Dimension of the grid (across)
 * @return The number of occupied grid cells
 */
int globalgrid::load(vector<argodata> &data,
                     vector<gridcell> &grid,
                     vector<float> &area,
                     int dimension)
{
    float current_latitude=-9999;
    float current_longitude=-9999;
    int grid_cell_index=-1;

    generate(grid, dimension);

    printf("Gridding data...");
    for (int i = 0; i < (int)data.size(); i++) {
        if ((data[i].latitude!=current_latitude) ||
            (data[i].latitude!=current_longitude)) {
            grid_cell_index =
                get_closest_grid_cell(-data[i].longitude,
                                      data[i].latitude,grid);
            current_latitude = data[i].latitude;
            current_longitude = data[i].longitude;
        }
        if (grid_cell_index != -1) {
            grid[grid_cell_index].samples[data[i].year].push_back(data[i]);
        }
    }
    printf("done\n");

    int hits=0;
    for (int i = 0; i < (int)grid.size(); i++) {
        for (int y = 1900; y < 3000; y++) {
            if (grid[i].samples[y].size()>0) {
                hits++;
                break;
            }
        }
    }

    printf("%.2f%% Coverage\n",hits*100.0f/grid.size());
    return hits;
}

/*
 * @brief clears all temporary values
 * @param grid The grid of data
 */
void globalgrid::clear_temp_values(vector<gridcell> &grid)
{
    for (unsigned int i = 0; i < grid.size(); i++) {
        grid[i].temp_value = -9999;
    }
}
