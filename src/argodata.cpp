/*
  Data structure for Argo observations
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "argodata.h"

argodata::argodata() {
    platform_number=0;
    cycle_number=0;
    wmo_inst_type=0;
    year=0;
    month=0;
    day=0;
    hour=0;
    min=0;
    latitude=-9999;
    longitude=-9999;
    data_centre[0]=0;
    direction = 0;
}

argodata::~argodata() {
}
