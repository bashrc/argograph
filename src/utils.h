/*
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILS_H_
#define UTILS_H_

#include <vector>
#include <stdio.h>
#include <string>
#include <string.h>
#include <math.h>

using namespace std;

class utils {
public:
    static string string_to_lower(string str);
    static void parse_string(string str, vector<string> &v);
    static void parse_string(string str, vector<float> &v);
    static void parse_latitude(string str, float &min, float &max);
    static void parse_view(string str, float &longitude, float &latitude);
    static void parse_range(string str, float &min, float &max);
    static string append_time_to_title(string title, int start_year, int end_year, int month);
    static string append_area_to_title(string title, vector<float> &area);
    static string append_number_to_filename(string filename, int number);

    utils() {};
    virtual ~utils() {};
};

#endif /* UTILS_H_ */
