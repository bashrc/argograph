/*
  Interface for Argo observations
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ARGO_H_
#define ARGO_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <regex.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <err.h>
#include <assert.h>
#include <sys/time.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <vector>
#include <math.h>
#include <iostream>
#include <omp.h>

#include "argodata.h"

// Number of CPU cores which the system is optimised for
#define ARGO_CPU_CORES     8

// maximum platform speed in metres per second
#define MAX_SPEED          0.4f

#define GRAPH_TEMPERATURE  0
#define GRAPH_SALINITY     1
#define GRAPH_DENSITY      2
#define GRAPH_OXYGEN       3
#define GRAPH_DIRECTION    4
#define GRAPH_TURBULENCE   5
#define GRAPH_OHC          6

// convert 24 hours into this number of samples
#define DIURNAL_SAMPLES   64

using namespace std;

// types of measurement
enum {
    MEASUREMENT_TEMPERATURE = 0,
    MEASUREMENT_SALINITY,
    MEASUREMENT_OXYGEN
};

enum {
    WALK_OK = 0,
    WALK_BADPATTERN,
    WALK_NAMETOOLONG,
    WALK_BADIO
};

// number of columns across the console for showing loading progress
#define CONSOLE_COLUMNS 72

// value used within the quality control section of a profile file to
// indicate good data or no quality control check
#define QC_NONE         '0'
#define QC_GOOD_DATA    '1'

#define WS_NONE0
#define WS_RECURSIVE (1 << 0)
#define WS_DEFAULT WS_RECURSIVE
#define WS_FOLLOWLINK (1 << 1) /* follow symlinks */
#define WS_DOTFILES (1 << 2) /* per unix convention, .file is hidden */
#define WS_MATCHDIRS (1 << 3) /* if pattern is used on dir names too */

class argo {
 private:
    static float sigmat(float temperature, float salinity);
    static void export_to_file(FILE * fp, argodata &data);
    static bool valid_netcdf_profile(string filename);
    static bool valid_character(char c);
    static bool inside_area(argodata &data, vector<float> &area);

    static int walk_dir(string dname, char *pattern, int spec, vector<string> &filenames);
    static int walk_recur(string dname, regex_t *reg, int spec, vector<string> &filenames);

    static void string_to_lower(char * str, char * result);
    static void remove_preceding_spaces(char * str);
    static bool extract_parameter(char * target, char * str, char * result, int max_result_length, FILE * fp);
    static bool extract_parameter_integer(char * target, char * str, int & value, FILE * fp);
    static bool extract_parameter_float(char * target, char * str, float & value, FILE * fp);
    static bool extract_parameter_date_time(char * target, char * str,
                                            int & year, int & month, int & day,
                                            int & hour, int & min, FILE * fp);
    static bool extract_series(char * target, char * str, int &records, float * result, int max_records, FILE * fp);
    static int seconds_between_dates(int Y1, int M1, int D1, int H1, int m1, int S1,
                                     int Y2, int M2, int D2, int H2, int m2, int S2);
    static void sort(vector<argodata> &data);
    static void transfer_values(int &records, float * values, float * result);
 public:
    static void get_data_centres(vector<argodata> &data,
                                 int start_year, int end_year,
                                 vector<string> &names,
                                 vector<vector<int> > &qty);
    static string get_instrument_type(int wmo_instrument_type);
    static string get_data_centre(string data_centre);
    static int active_platforms(vector<argodata> &data, int year, int month);
    static float density(float temperature, float salinity);
    static float ocean_heat_content(float temperature, float salinity, float pressure);
    static void calculate_speeds_subprocess(vector<argodata> &data,
                                            unsigned int data_start,
                                            unsigned int data_end,
                                            vector<vector<float> > &speeds);
    static void calculate_speeds(vector<argodata> &data,
                                 vector<vector<float> > &speeds);
    static void calculate_currents_subprocess(vector<argodata> &data,
                                              unsigned int data_start,
                                              unsigned int data_end,
                                              vector<vector<vector<int> > > &currents);
    static void calculate_currents(vector<argodata> &data,
                                   vector<vector<vector<int> > > &currents);
    static void latitude_model_subprocess(vector<argodata> &data,
                                          vector<vector<vector<vector<float> > > > &model,
                                          unsigned int pressure_steps,
                                          float max_pressure,
                                          int reference_start_year,
                                          int reference_end_year,
                                          unsigned int data_start,
                                          unsigned int data_end);
    static void latitude_model(vector<argodata> &data,
                               vector<vector<vector<vector<float> > > > &model,
                               unsigned int pressure_steps,
                               float max_pressure,
                               int reference_start_year,
                               int reference_end_year);
    static void latitude_anomalies_subprocess(vector<argodata> &data,
                                              vector<vector<vector<float> > > &anomalies,
                                              unsigned int pressure_steps,
                                              float max_pressure,
                                              int start_year,
                                              int end_year,
                                              int reference_start_year,
                                              int reference_end_year,
                                              vector<vector<vector<vector<float> > > > &model,
                                              unsigned int data_start,
                                              unsigned int data_end);
    static void latitude_anomalies(vector<argodata> &data,
                                   vector<vector<vector<float> > > &anomalies,
                                   unsigned int pressure_steps,
                                   float max_pressure,
                                   int start_year,
                                   int end_year,
                                   int reference_start_year,
                                   int reference_end_year);
    static void update_latitudes_subprocess(vector<argodata> &data,
                                            vector<vector<vector<float> > > &latitudedata,
                                            unsigned int pressure_steps,
                                            float max_pressure,
                                            unsigned int data_start,
                                            unsigned int data_end,
                                            int start_year, int end_year, int month);
    static void update_latitudes(vector<argodata> &data,
                                 vector<vector<vector<float> > > &latitudedata,
                                 unsigned int pressure_steps,
                                 float max_pressure,
                                 int start_year, int end_year, int month);
    static bool file_exists(string filename);
    static int date_range(vector<argodata> &data, int &min_year, int &min_month, int &max_year, int &max_month);
    static int cycle_range(vector<argodata> &data, int &min_cycle_number, int &max_cycle_number);
    static float value_at_pressure(float pressure, argodata &data,
                                   int measurement_type);
    static float measurement_at_pressure(float pressure, vector<argodata> &data,
                                         int measurement_type);
    static float temperature_at_pressure(float pressure, argodata &data);
    static float temperature_at_pressure(float pressure, vector<argodata> &data);
    static float salinity_at_pressure(float pressure, argodata &data);
    static float salinity_at_pressure(float pressure, vector<argodata> &data);
    static float oxygen_at_pressure(float pressure, argodata &data);
    static float oxygen_at_pressure(float pressure, vector<argodata> &data);

    static int load(string filename, vector<argodata> &data,
                    int start_year, int end_year, int month,
                    vector<float> &area, vector<int> &platforms);
    static int load_netcdf(string filename, vector<argodata> &data, bool verbose,
                           int start_year, int end_year, int month,
                           vector<float> &area, vector<int> &platforms,
                           string temporary_filename,
                           FILE * fp_export);
    static int load_netcdf_directory(string directory, vector<argodata> &data, bool verbose,
                                     int start_year, int end_year, int month,
                                     vector<float> &area, vector<int> &platforms,
                                     string temporary_filename,
                                     string export_filename);

    argo();
    virtual ~argo();
};

#endif /* ARGO_H_ */
