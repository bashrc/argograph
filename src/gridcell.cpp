/*
  Grid cell representing an area of the globe
  Copyright (C) 2013-2016 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gridcell.h"

gridcell::gridcell() {
}

gridcell::~gridcell() {
}

/*
 * @brief returns an average measurement value within a range of years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure The pressure (corresponding to depth)
 * @param measurement_type The type of measurement
 * @return average value
 */
float gridcell::get_average_measurement_years(int start_year, int end_year,
                                              float pressure,
                                              int measurement_type)
{
    float value, valuesum=0;
    int hits = 0;
    if (temp_value > -9998) return temp_value;

    if (end_year==start_year) end_year++;
    for (unsigned int y = (unsigned int)start_year; y <
             (unsigned int)end_year; y++) {
        for (unsigned int i = 0; i < samples[y].size(); i++) {
            value = -9999.0f;
            int valid = 0;
            switch(measurement_type) {
            case MEASUREMENT_TEMPERATURE: {
                value = argo::temperature_at_pressure(pressure,samples[y][i]);
                if ((value > 0.1f) && (value < 40.0f)) {
                    valid = 1;
                }
                break;
            }
            case MEASUREMENT_SALINITY: {
                value = argo::salinity_at_pressure(pressure,samples[y][i]);
                if (value > -9998) {
                    valid = 1;
                }
                break;
            }
            case MEASUREMENT_OXYGEN: {
                value = argo::oxygen_at_pressure(pressure,samples[y][i]);
                if (value > -9998) {
                    valid = 1;
                }
                break;
            }
            }
            if (valid == 1) {
                valuesum += value;
                hits++;
            }
        }
    }
    if (hits > 0) {
        temp_value = valuesum / hits;
    }
    return temp_value;
}

/*
 * @brief returns an average measurement change value within a range of years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure The pressure (corresponding to depth)
 * @param measurement_type The type of measurement
 * @return average value
 */
float gridcell::get_average_measurement_years_diff(int start_year, int end_year,
                                                   float pressure,
                                                   int measurement_type)
{
    int reference_year = start_year;
    float ref[12];
    float value, valuesum = 0;
    int hits = 0, ref_hits[12];
    if (temp_value > -9998) return temp_value;

    if (end_year==start_year) end_year++;

    memset((void*)ref_hits,'\0',12*sizeof(int));
    memset((void*)ref,'\0',12*sizeof(float));

    /* calculate reference monthly values */
    for (unsigned int y = (unsigned int)reference_year; y <=
             (unsigned int)(reference_year + 1); y++) {
        for (unsigned int i = 0; i < samples[y].size(); i++) {
            int month = samples[y][i].month - 1;
            int valid = 0;
            value = -9999.0f;
            switch(measurement_type) {
            case MEASUREMENT_TEMPERATURE: {
                value = argo::temperature_at_pressure(pressure,samples[y][i]);
                if ((value > 0.1f) && (value < 40.0f)) {
                    valid = 1;
                }
                break;
            }
            case MEASUREMENT_SALINITY: {
                value = argo::salinity_at_pressure(pressure,samples[y][i]);
                if ((value > 20) && (value < 50)) {
                    valid = 1;
                }
                break;
            }
            case MEASUREMENT_OXYGEN: {
                value = argo::oxygen_at_pressure(pressure,samples[y][i]);
                if ((value > 0) && (value < 500)) {
                    valid = 1;
                }
                break;
            }
            }
            if (valid == 1) {
                ref[month] += value;
                ref_hits[month]++;
            }
        }
    }

    /* average the values for each month within the reference year */
    for (unsigned int i = 0; i < 12; i++) {
        if (ref_hits[i] > 0) {
            ref[i] /= (float)ref_hits[i];
        }
    }

    /* get the sum of differences from the reference values */
    for (unsigned int y = (unsigned int)start_year; y <
             (unsigned int)end_year; y++) {
        for (unsigned int i = 0; i < samples[y].size(); i++) {
            int month = samples[y][i].month - 1;
            if (ref_hits[month] == 0) continue;
            if (ref[month] == 0) continue;
            int valid = 0;
            value = -9999.0f;
            switch(measurement_type) {
            case MEASUREMENT_TEMPERATURE: {
                value = argo::temperature_at_pressure(pressure,samples[y][i]);
                if ((value > 0.1f) && (value < 40.0f) &&
                    (ref[month] > 0.1f) && (ref[month] < 40.0f)) {
                    valid = 1;
                }
                break;
            }
            case MEASUREMENT_SALINITY: {
                value = argo::salinity_at_pressure(pressure,samples[y][i]);
                if ((value > 20) && (value < 50)) {
                    valid = 1;
                }
                break;
            }
            case MEASUREMENT_OXYGEN: {
                value = argo::oxygen_at_pressure(pressure,samples[y][i]);
                if ((value > 0) && (value < 500)) {
                    valid = 1;
                }
                break;
            }
            }
            if (valid == 1) {
                valuesum += (value - ref[month]);
                hits++;
            }
        }
    }
    if (hits > 0) temp_value = valuesum / hits;
    return temp_value;
}


/*
 * @brief Return the average temperature between the given years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure Desired pressure (depth)
 * @return average temperature
 */
float gridcell::get_average_temperature_years(int start_year, int end_year,
                                              float pressure)
{
    return get_average_measurement_years(start_year, end_year,
                                         pressure,
                                         MEASUREMENT_TEMPERATURE);
}

/*
 * @brief Return the average temperature change between the given years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure Desired pressure (depth)
 * @return average temperature
 */
float gridcell::get_average_temperature_years_diff(int start_year, int end_year,
                                                   float pressure)
{
    return get_average_measurement_years_diff(start_year, end_year,
                                              pressure,
                                              MEASUREMENT_TEMPERATURE);
}

/*
 * @brief Return the average salinity between the given years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure Desired pressure (depth)
 * @return salinity value
 */
float gridcell::get_average_salinity_years(int start_year, int end_year,
                                           float pressure)
{
    return get_average_measurement_years(start_year, end_year,
                                         pressure,
                                         MEASUREMENT_SALINITY);
}

/*
 * @brief Return the average salinity change between the given years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure Desired pressure (depth)
 * @return salinity value
 */
float gridcell::get_average_salinity_years_diff(int start_year, int end_year,
                                                float pressure)
{
    return get_average_measurement_years_diff(start_year, end_year,
                                              pressure,
                                              MEASUREMENT_SALINITY);
}

/*
 * @brief Return the average oxygen between the given years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure Desired pressure (depth)
 * @return Oxygen value
 */
float gridcell::get_average_oxygen_years(int start_year, int end_year,
                                         float pressure)
{
    return get_average_measurement_years(start_year, end_year,
                                         pressure,
                                         MEASUREMENT_OXYGEN);
}

/*
 * @brief Return the average oxygen change between the given years
 * @param start_year The start year
 * @param end_year The end year
 * @param pressure Desired pressure (depth)
 * @return Oxygen value
 */
float gridcell::get_average_oxygen_years_diff(int start_year, int end_year,
                                              float pressure)
{
    return get_average_measurement_years_diff(start_year, end_year,
                                              pressure,
                                              MEASUREMENT_OXYGEN);
}

/*
 * @brief Returns average temperature for a given year at a given depth
 * @param year Year
 * @param pressure Pressure value (corresponding to depth)
 * @return temperature value
 */
float gridcell::get_average_temperature(int year, float pressure)
{
    return argo::temperature_at_pressure(pressure,samples[year]);
}

/*
 * @brief Returns the average salinity for a given year at a given depth
 * @param year Year
 * @param pressure Pressure value (corresponding to depth)
 * @return Salinity value
 */
float gridcell::get_average_salinity(int year, float pressure)
{
    return argo::salinity_at_pressure(pressure,samples[year]);
}

/*
 * @brief Returns the average oxygen for a given year at a given depth
 * @param year Year
 * @param pressure Pressure value (corresponding to depth)
 * @return Oxygen value
 */
float gridcell::get_average_oxygen(int year, float pressure)
{
    return argo::oxygen_at_pressure(pressure,samples[year]);
}

/*
 * @brief Returns the average measurement
 * @param year Year
 * @param month Month
 * @param pressure Pressure value (corresponding to depth)
 * @param measurement_type
 * @return average measurement value
 */
float gridcell::get_average_measurement(int year, int month,
                                        float pressure,
                                        int measurement_type)
{
    float valuesum = 0;
    int hits = 0;

    for (int i = 0; i < (int)samples[year].size(); i++) {
        if (samples[year][i].month == month) {
            float value = -9999.0f;
            switch(measurement_type) {
            case MEASUREMENT_TEMPERATURE: {
                value = argo::temperature_at_pressure(pressure,samples[year][i]);
                break;
            }
            case MEASUREMENT_SALINITY: {
                value = argo::salinity_at_pressure(pressure,samples[year][i]);
                break;
            }
            case MEASUREMENT_OXYGEN: {
                value = argo::oxygen_at_pressure(pressure,samples[year][i]);
                break;
            }
            }
            if (value > -9998.0f) {
                valuesum += value;
                hits++;
            }

        }
    }
    if (hits > 0) {
        return valuesum / hits;
    }
    return -9999;
}


/*
 * @brief Returns the average temperature for the given year, month and depth
 * @param year Year
 * @param month Month
 * @param pressure Pressure (corresponds to depth)
 * @return average temperature
 */
float gridcell::get_average_temperature(int year, int month, float pressure)
{
    return get_average_measurement(year, month,
                                   pressure,
                                   MEASUREMENT_TEMPERATURE);
}

float gridcell::get_average_temperature_diff(int year, float pressure, int reference_year)
{
    float diff = 0, av;
    int hits = 0;
    for (int m = 0; m < 12; m++) {
        av = get_average_measurement(reference_year, m,
                                     pressure,
                                     MEASUREMENT_TEMPERATURE);
        if (av > -9998) {
            diff +=
                (get_average_measurement(year-1, m,
                                         pressure,
                                         MEASUREMENT_TEMPERATURE) - av);
            hits++;
        }
    }
    if (hits > 0) diff /= hits;
    return diff;
}

float gridcell::get_average_salinity(int year, int month, float pressure)
{
    return get_average_measurement(year, month,
                                   pressure,
                                   MEASUREMENT_SALINITY);
}

float gridcell::get_average_salinity_diff(int year, float pressure, int reference_year)
{
    float diff = 0, av;
    int hits = 0;
    for (int m = 0; m < 12; m++) {
        av = get_average_measurement(reference_year, m,
                                     pressure,
                                     MEASUREMENT_SALINITY);
        if (av > -9998) {
            diff +=
                (get_average_measurement(year-1, m,
                                         pressure,
                                         MEASUREMENT_SALINITY) - av);
            hits++;
        }
    }
    if (hits > 0) diff /= hits;
    return diff;
}

float gridcell::get_average_oxygen(int year, int month, float pressure)
{
    return get_average_measurement(year, month,
                                   pressure,
                                   MEASUREMENT_OXYGEN);
}

float gridcell::get_average_oxygen_diff(int year, float pressure, int reference_year)
{
    float diff = 0, av;
    int hits  = 0;
    for (int m = 0; m < 12; m++) {
        av = get_average_measurement(reference_year, m,
                                     pressure,
                                     MEASUREMENT_OXYGEN);
        if (av > -9998) {
            diff +=
                (get_average_measurement(year-1, m,
                                         pressure,
                                         MEASUREMENT_OXYGEN) - av);
            hits++;
        }
    }
    if (hits > 0) diff /= hits;
    return diff;
}

float gridcell::get_average_measurement(int start_year, int end_year,
                                        int month, float pressure,
                                        int measurement_type)
{
    vector<argodata> month_samples;
    for (int y = start_year; y <= end_year; y++) {
        for (int i = 0; i < (int)samples[y].size(); i++) {
            if (samples[y][i].month==month) {
                month_samples.push_back(samples[y][i]);
            }
        }
    }
    switch(measurement_type) {
    case MEASUREMENT_TEMPERATURE: {
        return argo::temperature_at_pressure(pressure,month_samples);
        break;
    }
    case MEASUREMENT_SALINITY: {
        return argo::salinity_at_pressure(pressure,month_samples);
        break;
    }
    case MEASUREMENT_OXYGEN: {
        return argo::oxygen_at_pressure(pressure,month_samples);
        break;
    }
    }
    return -9999;
}

float gridcell::get_average_temperature(int start_year, int end_year,
                                        int month, float pressure)
{
    return get_average_measurement(start_year, end_year,
                                   month, pressure,
                                   MEASUREMENT_TEMPERATURE);
}

float gridcell::get_average_salinity(int start_year, int end_year,
                                     int month, float pressure)
{
    return get_average_measurement(start_year, end_year,
                                   month, pressure,
                                   MEASUREMENT_SALINITY);
}

float gridcell::get_average_oxygen(int start_year, int end_year,
                                   int month, float pressure)
{
    return get_average_measurement(start_year, end_year,
                                   month, pressure,
                                   MEASUREMENT_OXYGEN);
}

void gridcell::update(int reference_start_year, int reference_end_year,
                      float pressure, int graph_type)
{
    switch (graph_type) {
    case GRAPH_TEMPERATURE:
        for (int m = 0; m < 12; m++) {
            reference_model[m] =
                get_average_temperature(reference_start_year,
                                        reference_end_year, m, pressure);
        }
        break;
    case GRAPH_SALINITY:
        for (int m = 0; m < 12; m++) {
            reference_model[m] =
                get_average_salinity(reference_start_year,
                                     reference_end_year, m, pressure);
        }
        break;
    case GRAPH_DENSITY:
        for (int m = 0; m < 12; m++) {
            float temperature =
                get_average_temperature(reference_start_year,
                                        reference_end_year, m, pressure);
            float salinity =
                get_average_salinity(reference_start_year,
                                     reference_end_year, m, pressure);
            if ((temperature>-9999.0f) &&
                (salinity>-9999.0f)) {
                reference_model[m] = argo::density(temperature,salinity);
            }
            else {
                reference_model[m] = -9999.0f;
            }
        }
        break;
    }
}

bool gridcell::update_histogram(int year, float pressure)
{
    for (int i = 0; i < 200; i++) histogram[i]=0;

    int tot=0;
    for (int i = 0; i < (int)samples[year].size(); i++) {
        float temp = argo::temperature_at_pressure(pressure,samples[year][i]);
        if (temp>-9999) {
            int temp_index = (int)(temp+0.5f)+100;
            if ((temp_index>=0) &&
                (temp_index<200)) {
                histogram[temp_index]+=1.0f;
                tot++;
            }
        }
    }

    // Normalise
    if (tot>0) {
        for (int i = 0; i < 200; i++) {
            histogram[i] = histogram[i]*100.0f/tot;
        }
        return true;
    }
    return false;
}

int gridcell::update_diurnal(int year, int month, float pressure, int graph_type)
{
    float v, temperature, salinity;
    int hits[DIURNAL_SAMPLES], total_hits = 0;

    for (int i = 0; i < DIURNAL_SAMPLES; i++) {
        diurnal[i]=0;
        hits[i] = 0;
    }

    for (int i = 0; i < (int)samples[year].size(); i++) {
        int time_index =
            ((samples[year][i].hour * 60) + samples[year][i].min) * DIURNAL_SAMPLES / (60*24);

        if ((month < 0) || (samples[year][i].month == month)) {
            switch(graph_type) {
            case GRAPH_TEMPERATURE:
                {
                    if (month >= 0) {
                        v = get_average_temperature(year, month, pressure);
                    }
                    else {
                        v = get_average_temperature(year, pressure);
                    }
                    if (v > -9999.0f) {
                        diurnal[time_index] += v;
                        hits[time_index]++;
                    }
                    break;
                }
            case GRAPH_SALINITY:
                {
                    if (month >= 0) {
                        v = get_average_salinity(year, month, pressure);
                    }
                    else {
                        v = get_average_salinity(year, pressure);
                    }
                    if (v > -9999.0f) {
                        diurnal[time_index] += v;
                        hits[time_index]++;
                    }
                    break;
                }
            case GRAPH_DENSITY:
                {
                    if (month >= 0) {
                        temperature = get_average_temperature(year, month, pressure);
                        salinity = get_average_salinity(year, month, pressure);
                    }
                    else {
                        temperature = get_average_temperature(year, pressure);
                        salinity = get_average_salinity(year, pressure);
                    }
                    if ((temperature > -9999.0f) &&
                        (salinity > -9999.0f)) {
                        diurnal[time_index] += argo::density(temperature,salinity);
                        hits[time_index]++;
                    }
                }
            }
        }
    }


    for (int i = 0; i < DIURNAL_SAMPLES; i++) {
        if (hits[i] > 0) {
            diurnal[i] /= hits[i];
            total_hits += hits[i];
        }
        else {
            diurnal[i] = -9999;
        }
    }
    return total_hits;
}

float gridcell::get_anomaly(int year, int month,
                            int reference_start_year, int reference_end_year,
                            float pressure, int graph_type)
{
    if (reference_model[month]>-9999.0f) {
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
        {
            float temperature = get_average_temperature(year, month, pressure);
            if (temperature > -9999.0f) {
                return temperature - reference_model[month];
            }
            break;
        }
        case GRAPH_SALINITY:
        {
            float salinity = get_average_salinity(year, month, pressure);
            if (salinity > -9999.0f) {
                return salinity - reference_model[month];
            }
            break;
        }
        case GRAPH_DENSITY:
        {
            float temperature = get_average_temperature(year, month, pressure);
            float salinity = get_average_salinity(year, month, pressure);
            if ((temperature > -9999.0f) &&
                (salinity > -9999.0f)) {
                return argo::density(temperature,salinity) -
                    reference_model[month];
            }
        }
        }
    }
    return -9999.0f;
}

float gridcell::get_anomaly(int year,
                            int reference_start_year, int reference_end_year,
                            float pressure, int graph_type)
{
    int hits=0;
    float average=0;

    for (int m = 0; m < 12; m++) {
        float anomaly = get_anomaly(year,m,reference_start_year,
                                    reference_end_year,
                                    pressure, graph_type);
        if (anomaly>-9999.0f) {
            average += anomaly;
            hits++;
        }
    }
    if (hits>0) {
        return average/hits;
    }
    return -9999.0f;
}

void gridcell::get_variance(int year, float &min, float &max,
                            int reference_start_year, int reference_end_year,
                            float pressure, int graph_type)
{
    for (int m = 0; m < 12; m++) {
        float anomaly = get_anomaly(year,m,reference_start_year,
                                    reference_end_year,
                                    pressure, graph_type);
        if (anomaly > -9999.0f) {
            if (anomaly<min) min = anomaly;
            if (anomaly>max) max = anomaly;
        }
    }
}
