/*
  Grid cell representing an area of the globe
  Copyright (C) 2013-2015 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRIDCELL_H_
#define GRIDCELL_H_

#include <vector>
#include <stdio.h>
#include "argodata.h"
#include "argo.h"

using namespace std;

class gridcell {
public:
    // samples for each year
    vector<argodata> samples[3000];
    float histogram[200];
    float diurnal[DIURNAL_SAMPLES];

    // 3D coordinates of the cell centre on a unit sphere
    float x,y,z;

    // Latitude and longitude of centre point
    float latitude,longitude;

    float reference_model[12];

    // A temporary value used during plotting of maps
    float temp_value;

    void update(int reference_start_year, int reference_end_year,
                float pressure, int graph_type);
    bool update_histogram(int year, float pressure);
    int update_diurnal(int year, int month, float pressure, int graph_type);

    float get_average_measurement(int start_year, int end_year,
                                  int month, float pressure,
                                  int measurement_type);
    float get_average_measurement(int year, int month,
                                  float pressure,
                                  int measurement_type);
    float get_average_measurement_years(int start_year, int end_year,
                                        float pressure,
                                        int measurement_type);
    float get_average_measurement_years_diff(int start_year, int end_year,
                                             float pressure,
                                             int measurement_type);

    float get_average_temperature_years(int start_year, int end_year,
                                        float pressure);
    float get_average_temperature_years_diff(int start_year, int end_year,
                                             float pressure);
    float get_average_salinity_years(int start_year, int end_year,
                                     float pressure);
    float get_average_salinity_years_diff(int start_year, int end_year,
                                          float pressure);
    float get_average_oxygen_years(int start_year, int end_year,
                                   float pressure);
    float get_average_oxygen_years_diff(int start_year, int end_year,
                                        float pressure);

    float get_average_temperature(int year, float pressure);
    float get_average_temperature(int year, int month, float pressure);
    float get_average_temperature(int start_year, int end_year,
                                  int month, float pressure);

    float get_average_salinity(int year, float pressure);
    float get_average_salinity(int year, int month, float pressure);
    float get_average_salinity(int start_year, int end_year,
                               int month, float pressure);

    float get_average_oxygen(int year, float pressure);
    float get_average_oxygen(int year, int month, float pressure);
    float get_average_oxygen(int start_year, int end_year,
                             int month, float pressure);

    float get_average_temperature_diff(int year, float pressure, int reference_year);
    float get_average_salinity_diff(int year, float pressure, int reference_year);
    float get_average_oxygen_diff(int year, float pressure, int reference_year);

    float get_anomaly(int year, int month,
                      int reference_start_year, int reference_end_year,
                      float pressure, int graph_type);
    float get_anomaly(int year,
                      int reference_start_year, int reference_end_year,
                      float pressure, int graph_type);
    void get_variance(int year, float &min, float &max,
                      int reference_start_year, int reference_end_year,
                      float pressure, int graph_type);

    gridcell();
    virtual ~gridcell();
};

#endif /* GRIDCELL_H_ */
