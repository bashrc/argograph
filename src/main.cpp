/*
  argograph
  A command line utility for plotting ocean temperature data
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <math.h>
#include <time.h>

#include "utils.h"
#include "anyoption.h"
#include "argodata.h"
#include "argo.h"
#include "gridcell.h"
#include "globalgrid.h"
#include "gnuplot.h"

#define VERSION 1.20

using namespace std;

int main(int argc, char* argv[]) {
    AnyOption *opt = new AnyOption();

    // help
    opt->addUsage( "Example: " );
    opt->addUsage( "  argograph --start 1998 --end 2012 --data datadirectory" );
    opt->addUsage( " " );
    opt->addUsage( "Usage: " );
    opt->addUsage( "" );
    opt->addUsage( "     --start <yyyy>                  " \
                   "Starting year for the plot");
    opt->addUsage( "     --end <yyyy>                    " \
                   "Ending year for the plot");
    opt->addUsage( "     --year <yyyy>                   " \
                   "Year for the plot");
    opt->addUsage( "     --month <1-12>                  " \
                   "Month number for the plot");
    opt->addUsage( "     --data <directory>              " \
                   "Directory containing data");
    opt->addUsage( "     --load <filename>               " \
                   "Load data from an exported file");
    opt->addUsage( "     --export <filename>             " \
                   "Export loaded data to the given file");
    opt->addUsage( "     --grid <dimension>              " \
                   "Number of cells across the grid");
    opt->addUsage( "     --area <lat,long,lat,long>      " \
                   "Limit to a geographical area");
    opt->addUsage( "     --range <min,max>               " \
                   "Sets the range of values to show");
    opt->addUsage( "     --platform <number>             " \
                   "Set the float number or numbers");
    opt->addUsage( "     --latitudes <min,max>           " \
                   "Limit between latitudes");
    opt->addUsage( "     --view <lat,long>               " \
                   "Viewing coordinates for 3D");
    opt->addUsage( "     --refyears <min,max>            " \
                   "Reference years for anomaly calculation");
    opt->addUsage( "     --verbose                       " \
                   "Verbose mode");
    opt->addUsage( "     --title <text>                  " \
                   "Graph title");
    opt->addUsage( "     --subtitle <text>               " \
                   "Graph subtitle");
    opt->addUsage( "     --indent <value>                " \
                   "Left indentation of the subtitle (0.0-1.0)");
    opt->addUsage( "     --depth <metres>                " \
                   "Depth in in metres");
    opt->addUsage( "     --maxdepth <metres>             " \
                   "Maximum depth in in metres");
    opt->addUsage( "     --depthstep <metres>            " \
                   "Depth increment in in metres");
    opt->addUsage( "     --yearstep <years>              " \
                   "Time step in years");
    opt->addUsage( "     --minmax                        " \
                   "Show minimum and maximum values");
    opt->addUsage( "     --tempprofiles <filename>       " \
                   "Plot temperature profiles graph");
    opt->addUsage( "     --salprofiles <filename>        " \
                   "Plot salinity profiles graph");
    opt->addUsage( "     --ohcprofiles <filename>        " \
                   "Plot ocean heat content profiles graph");
    opt->addUsage( "     --denprofiles <filename>        " \
                   "Plot density profiles graph");
    opt->addUsage( "     --oxyprofiles <filename>        " \
                   "Plot oxygen profiles graph");
    opt->addUsage( "     --tempanomalies <filename>      " \
                   "Plot temperature anomalies graph");
    opt->addUsage( "     --salanomalies <filename>       " \
                   "Plot salinity anomalies graph");
    opt->addUsage( "     --denanomalies <filename>       " \
                   "Plot density anomalies graph");
    opt->addUsage( "     --ohcanomalies <filename>       " \
                   "Plot ocean heat content anomalies graph");
    opt->addUsage( "     --oxyanomalies <filename>       " \
                   "Plot oxygen anomalies graph");
    opt->addUsage( "     --fitline                       " \
                   "Show a best fit line");
    opt->addUsage( "     --averages <filename>           " \
                   "Plot averages graph");
    opt->addUsage( "     --distribution <filename>       " \
                   "Plot temperature distribution graph");
    opt->addUsage( "     --diurnal <filename>       " \
                   "Plot diurnal graph");
    opt->addUsage( "     --mapaverages <filename>        " \
                   "Plot map showing average temperatures");
    opt->addUsage( "     --mapaverages3d <filename>      " \
                   "Plot 3D map showing average temperatures");
    opt->addUsage( "     --mapsalaverages <filename>     " \
                   "Plot map showing average salinity");
    opt->addUsage( "     --mapsalaverages3d <filename>   " \
                   "Plot 3D map showing average salinity");
    opt->addUsage( "     --mapdenaverages <filename>     " \
                   "Plot map showing average density");
    opt->addUsage( "     --mapohcaverages <filename>     " \
                   "Plot map showing average oceanheat content");
    opt->addUsage( "     --mapdenaverages3d <filename>   " \
                   "Plot 3D map showing average density");
    opt->addUsage( "     --mapoxyaverages <filename>     " \
                   "Plot map showing average oxygen");
    opt->addUsage( "     --mapoxyaverages3d <filename>   " \
                   "Plot 3D map showing average oxygen");
    opt->addUsage( "     --mapplatforms <filename>       " \
                   "Plot map showing platform locations");
    opt->addUsage( "     --mapplatforms3d <filename>     " \
                   "Plot 3D map showing platform locations");
    opt->addUsage( "     --maptempanomalies <filename>   " \
                   "Plot map showing temperature anomalies");
    opt->addUsage( "     --maptempanomalies3d <filename> " \
                   "Plot 3D map showing temperature anomalies");
    opt->addUsage( "     --mapsalanomalies <filename>    " \
                   "Plot map showing salinity anomalies");
    opt->addUsage( "     --mapsalanomalies3d <filename>  " \
                   "Plot 3D map showing salinity anomalies");
    opt->addUsage( "     --mapdenanomalies <filename>    " \
                   "Plot map showing density anomalies");
    opt->addUsage( "     --mapohcanomalies <filename>    " \
                   "Plot map showing ocean heat content anomalies");
    opt->addUsage( "     --mapdenanomalies3d <filename>  " \
                   "Plot 3D map showing density anomalies");
    opt->addUsage( "     --mapohcanomalies3d <filename>  " \
                   "Plot 3D map showing ocean heat content anomalies");
    opt->addUsage( "     --mapoxyanomalies <filename>    " \
                   "Plot map showing oxygen anomalies");
    opt->addUsage( "     --mapoxyanomalies3d <filename>  " \
                   "Plot 3D map showing oxygen anomalies");
    opt->addUsage( "     --tempcycles <filename>         " \
                   "Plot temperature cycles for a single platform");
    opt->addUsage( "     --salcycles <filename>          " \
                   "Plot salinity cycles for a single platform");
    opt->addUsage( "     --dencycles <filename>          " \
                   "Plot density cycles for a single platform");
    opt->addUsage( "     --ohccycles <filename>          " \
                   "Plot ocean heat content cycles for a single platform");
    opt->addUsage( "     --oxycycles <filename>          " \
                   "Plot oxygen cycles for a single platform");
    opt->addUsage( "     --templatitudes <filename>      " \
                   "Plot temperatures for each latitude");
    opt->addUsage( "     --sallatitudes <filename>       " \
                   "Plot salinity for each latitude");
    opt->addUsage( "     --denlatitudes <filename>       " \
                   "Plot density for each latitude");
    opt->addUsage( "     --ohclatitudes <filename>       " \
                   "Plot ocean heat content for each latitude");
    opt->addUsage( "     --oxylatitudes <filename>       " \
                   "Plot oxygen for each latitude");
    opt->addUsage( "     --templatanomalies <filename>   " \
                   "Plot temperature anomalies for each latitude");
    opt->addUsage( "     --sallatanomalies <filename>    " \
                   "Plot salinity anomalies for each latitude");
    opt->addUsage( "     --denlatanomalies <filename>    " \
                   "Plot density anomalies for each latitude");
    opt->addUsage( "     --ohclatanomalies <filename>    " \
                   "Plot ocean heat content anomalies for each latitude");
    opt->addUsage( "     --oxylatanomalies <filename>    " \
                   "Plot oxygen anomalies for each latitude");
    opt->addUsage( "     --direction <filename>          " \
                   "Show direction of currents");
    opt->addUsage( "     --turbulence <filename>         " \
                   "Show turbulence of currents");
    opt->addUsage( "     --active <filename>             " \
                   "Plot number of active platforms");
    opt->addUsage( "     --datacentres <filename>        " \
                   "Plot data centres over time");
    opt->addUsage( "     --speed <filename>              " \
                   "Plot map showing average platform speeds");
    opt->addUsage( "     --runningaverage                " \
                   "Show running average");
    opt->addUsage( "     --width <pixels>                " \
                   "Width of the graph image");
    opt->addUsage( "     --height <pixels>               " \
                   "Height of the graph image");
    opt->addUsage( "     --change                        " \
                   "Plot changes");
    opt->addUsage( "  -V --version                       " \
                   "Show version number");
    opt->addUsage( "  -h --help                          " \
                   "Show help");
    opt->addUsage( "" );

    opt->setOption(  "start" );
    opt->setOption(  "end" );
    opt->setOption(  "platform" );
    opt->setOption(  "data" );
    opt->setOption(  "load" );
    opt->setOption(  "area" );
    opt->setOption(  "latitudes" );
    opt->setOption(  "refyears" );
    opt->setOption(  "title" );
    opt->setOption(  "subtitle" );
    opt->setOption(  "width" );
    opt->setOption(  "height" );
    opt->setOption(  "depth" );
    opt->setOption(  "maxdepth" );
    opt->setOption(  "depthstep" );
    opt->setOption(  "yearstep" );
    opt->setOption(  "tempprofiles" );
    opt->setOption(  "salprofiles" );
    opt->setOption(  "denprofiles" );
    opt->setOption(  "ohcprofiles" );
    opt->setOption(  "oxyprofiles" );
    opt->setOption(  "tempanomalies" );
    opt->setOption(  "salanomalies" );
    opt->setOption(  "denanomalies" );
    opt->setOption(  "ohcanomalies" );
    opt->setOption(  "oxyanomalies" );
    opt->setOption(  "averages" );
    opt->setOption(  "distribution" );
    opt->setOption(  "diurnal" );
    opt->setOption(  "mapaverages" );
    opt->setOption(  "mapaverages3d" );
    opt->setOption(  "mapsalaverages" );
    opt->setOption(  "mapsalaverages3d" );
    opt->setOption(  "mapdenaverages" );
    opt->setOption(  "mapdenaverages3d" );
    opt->setOption(  "mapohcaverages" );
    opt->setOption(  "mapohcaverages3d" );
    opt->setOption(  "mapoxyaverages" );
    opt->setOption(  "mapoxyaverages3d" );
    opt->setOption(  "mapplatforms" );
    opt->setOption(  "mapplatforms3d" );
    opt->setOption(  "maptempanomalies" );
    opt->setOption(  "maptempanomalies3d" );
    opt->setOption(  "mapsalanomalies" );
    opt->setOption(  "mapsalanomalies3d" );
    opt->setOption(  "mapdenanomalies" );
    opt->setOption(  "mapdenanomalies3d" );
    opt->setOption(  "mapohcanomalies" );
    opt->setOption(  "mapohcanomalies3d" );
    opt->setOption(  "mapoxyanomalies" );
    opt->setOption(  "mapoxyanomalies3d" );
    opt->setOption(  "year" );
    opt->setOption(  "month" );
    opt->setOption(  "tempcycles" );
    opt->setOption(  "salcycles" );
    opt->setOption(  "dencycles" );
    opt->setOption(  "ohccycles" );
    opt->setOption(  "oxycycles" );
    opt->setOption(  "templatitudes" );
    opt->setOption(  "sallatitudes" );
    opt->setOption(  "denlatitudes" );
    opt->setOption(  "ohclatitudes" );
    opt->setOption(  "oxylatitudes" );
    opt->setOption(  "templatanomalies" );
    opt->setOption(  "sallatanomalies" );
    opt->setOption(  "denlatanomalies" );
    opt->setOption(  "ohclatanomalies" );
    opt->setOption(  "oxylatanomalies" );
    opt->setOption(  "export" );
    opt->setOption(  "view" );
    opt->setOption(  "direction" );
    opt->setOption(  "turbulence" );
    opt->setOption(  "active" );
    opt->setOption(  "datacentres" );
    opt->setOption(  "speed" );
    opt->setOption(  "indent" );
    opt->setOption(  "grid" );
    opt->setOption(  "range" );
    opt->setFlag(  "change" );
    opt->setFlag(  "minmax" );
    opt->setFlag(  "runningaverage" );
    opt->setFlag(  "verbose" );
    opt->setFlag(  "fitline" );
    opt->setFlag(  "help", 'h' );
    opt->setFlag(  "version", 'V' );

    opt->processCommandArgs(argc, argv);

    // Show help
    if ((!opt->hasOptions()) || opt->getFlag( "help" )  ||
        opt->getFlag( 'h' )) {
        // print usage if no options
        opt->printUsage();
        delete opt;
        return(0);
    }

    bool year_step_set=false;
    bool use_reference_period=false;
    int month = -1;

    /* plot changes in temperature profiles */
    int plot_change = 0;

    // Verbose scanning of directories
    bool verbose=false;
    if (opt->getFlag("verbose")) {
        verbose = true;
    }

    // Show a best fit line
    bool show_best_fit_line = false;
    if (opt->getFlag("fitline")) {
        show_best_fit_line = true;
    }

    // Show minimum and maximum on the graph
    bool show_minmax = false;
    if (opt->getFlag("minmax")) {
        show_minmax = true;
    }


    /* plot changes to profiles */
    if (opt->getFlag("change")) {
        plot_change = 1;
        printf("Plot changes\n");
    }

    bool show_running_average = false;
    if (opt->getFlag("runningaverage")) {
        show_running_average = true;
    }

    // Version number
    if (opt->getFlag("version") || opt->getFlag('V')) {
        printf("Version %f\n", VERSION);
        delete opt;
        return(0);
    }

    // The directory to search for profile data
    string data_directory = "";
    if (opt->getValue("data") != NULL) {
        data_directory = opt->getValue("data");
    }

    // Load from a previously exported file
    // This is faster than loading many .nc profile files
    string load_filename="";
    if (opt->getValue("load") != NULL) {
        load_filename = opt->getValue("load");
    }

    // dimension of the grid (default is 72x36)
    int grid_dimension = 72;
    if (opt->getValue("grid") != NULL) {
        grid_dimension = atoi(opt->getValue("grid"));
        // ensure that it is divisible by 2
        grid_dimension = (grid_dimension/2)*2;
        printf("Grid resolution set to %dx%d "\
               "(%.2f degrees diameter)\n",
               grid_dimension,grid_dimension/2,360.0f/grid_dimension);
    }

    int image_width = 800;
    if (opt->getValue("width") != NULL) {
        image_width = atoi(opt->getValue("width"));
    }

    int image_height = 600;
    if (opt->getValue("height") != NULL) {
        image_height = atoi(opt->getValue("height"));
    }

    float depth = 100;
    if (opt->getValue("depth") != NULL) {
        depth = atof(opt->getValue("depth"));
    }
    float pressure = depth;

    float max_pressure = 1000;
    if (opt->getValue("maxdepth") != NULL) {
        max_pressure = atof(opt->getValue("maxdepth"));
    }

    float pressure_step = 250;
    if (opt->getValue("depthstep") != NULL) {
        pressure_step = atof(opt->getValue("depthstep"));
    }

    int year_step = 3;
    if (opt->getValue("yearstep") != NULL) {
        year_step = atoi(opt->getValue("yearstep"));
        year_step_set = true;
    }

    string title="";
    if (opt->getValue("title") != NULL) {
        title = opt->getValue("title");
    }

    string subtitle = "Source: http://data.nodc.noaa.gov/argo/data/";
    if (opt->getValue("subtitle") != NULL) {
        subtitle = opt->getValue("subtitle");
    }

    float subtitle_indent = 0.3f;
    if (opt->getValue("indent") != NULL) {
        subtitle_indent = atof(opt->getValue("indent"));
    }

    // One of more platform numbers
    vector<int> platforms;
    if (opt->getValue("platform") != NULL) {
        vector<string> platform_strings;
        string platforms_str = opt->getValue("platform");
        utils::parse_string(platforms_str, platform_strings);
        for (int i = 0; i < (int)platform_strings.size(); i++) {
            int number = atoi(platform_strings[i].c_str());
            platforms.push_back(number);
        }
    }

    // Within a given area
    vector<float> area;
    if (opt->getValue("area") != NULL) {
        string area_str = opt->getValue("area");
        utils::parse_string(area_str, area);
        if (((int)area.size() > 0) && ((int)area.size() < 4)) {
            printf("Only %d out of 4 area coordinates were specified." \
                   "  Did you miss out a comma?\n",
                   (int)area.size());
            delete opt;
            return(0);
        }
    }

    // Range of latitudes
    if (opt->getValue("latitudes") != NULL) {
        string latitude_str = opt->getValue("latitudes");
        float min_latitude=40, max_latitude=50;
        utils::parse_latitude(latitude_str, min_latitude, max_latitude);
        area.clear();
        area.push_back(min_latitude);
        area.push_back(0.0f);
        area.push_back(max_latitude);
        area.push_back(0.0f);
    }

    // Sets the viewing angle for teh 3D map
    float view_longitude=30, view_latitude=0;
    if (opt->getValue("view") != NULL) {
        string view_str = opt->getValue("view");
        utils::parse_view(view_str, view_longitude, view_latitude);
        printf("Viewing angle: %.1fW - %.1fN\n",
               view_longitude, view_latitude);
    }

    // Reference years used for anomaly calculation
    int reference_start_year=1998, reference_end_year=2006;
    if (opt->getValue("refyears") != NULL) {
        string refyears_str = opt->getValue("refyears");
        float y1=1998,y2=2004;
        utils::parse_range(refyears_str, y1,y2);
        reference_start_year = (int)y1;
        reference_end_year = (int)y2;
        use_reference_period=true;
        if (reference_end_year < reference_start_year) {
            int temp_year = reference_start_year;
            reference_start_year = reference_end_year;
            reference_end_year = temp_year;
        }
        printf("Reference period: %d - %d\n",
               reference_start_year, reference_end_year);
    }

    // Set the range of values to plot
    float range_min=0, range_max=0;
    if (opt->getValue("range") != NULL) {
        string range_str = opt->getValue("range");
        utils::parse_range(range_str, range_min,range_max);
        printf("Range set to: %.2f - %.2f\n", range_min, range_max);
    }

    // Filename for temperature anomalies graph
    string temperature_anomalies_filename = "";
    if (opt->getValue("tempanomalies") != NULL) {
        temperature_anomalies_filename = opt->getValue("tempanomalies");
        if (temperature_anomalies_filename=="") {
            temperature_anomalies_filename =
                "graph_temperature_anomalies.png";
        }
        use_reference_period=true;
    }
    // Filename for salinity anomalies graph
    string salinity_anomalies_filename = "";
    if (opt->getValue("salanomalies") != NULL) {
        salinity_anomalies_filename = opt->getValue("salanomalies");
        if (salinity_anomalies_filename=="") {
            salinity_anomalies_filename = "graph_salinity_anomalies.png";
        }
        use_reference_period=true;
    }
    // Filename for density anomalies graph
    string density_anomalies_filename = "";
    if (opt->getValue("denanomalies") != NULL) {
        density_anomalies_filename = opt->getValue("denanomalies");
        if (density_anomalies_filename=="") {
            density_anomalies_filename = "graph_density_anomalies.png";
        }
        use_reference_period=true;
    }
    // Filename for ocean heat content anomalies graph
    string ohc_anomalies_filename = "";
    if (opt->getValue("ohcanomalies") != NULL) {
        ohc_anomalies_filename = opt->getValue("ohcanomalies");
        if (ohc_anomalies_filename=="") {
            ohc_anomalies_filename = "graph_ohc_anomalies.png";
        }
        use_reference_period=true;
    }
    // Filename for oxygen anomalies graph
    string oxygen_anomalies_filename = "";
    if (opt->getValue("oxyanomalies") != NULL) {
        oxygen_anomalies_filename = opt->getValue("oxyanomalies");
        if (oxygen_anomalies_filename=="") {
            oxygen_anomalies_filename = "graph_oxygen_anomalies.png";
        }
        use_reference_period=true;
    }

    // Filename for averages graph
    string averages_filename = "";
    if (opt->getValue("averages") != NULL) {
        averages_filename = opt->getValue("averages");
        if (averages_filename=="") {
            averages_filename = "graph_averages.png";
        }
    }

    // Set the starting year
    int start_year = 1998;
    if (opt->getValue("start") != NULL) {
        start_year = atoi(opt->getValue("start"));
    }

    // Set the ending year
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);
    int end_year = aTime->tm_year + 1900 - 1;
    if (opt->getValue("end") != NULL) {
        end_year = atoi(opt->getValue("end"));
    }

    // Filename for distribution graph
    string distribution_filename = "";
    if (opt->getValue("distribution") != NULL) {
        distribution_filename = opt->getValue("distribution");
        if (distribution_filename=="") {
            distribution_filename = "graph_distribution.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }

    string diurnal_filename = "";
    if (opt->getValue("diurnal") != NULL) {
        diurnal_filename = opt->getValue("diurnal");
        if (diurnal_filename=="") {
            diurnal_filename = "graph_diurnal.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }

    // Filename for temperature profiles graph
    string temperature_profiles_filename = "";
    if (opt->getValue("tempprofiles") != NULL) {
        temperature_profiles_filename = opt->getValue("tempprofiles");
        if (temperature_profiles_filename=="") {
            temperature_profiles_filename =
                "graph_temperature_profiles.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }

    // Filename for salinity profiles graph
    string salinity_profiles_filename = "";
    if (opt->getValue("salprofiles") != NULL) {
        salinity_profiles_filename = opt->getValue("salprofiles");
        if (salinity_profiles_filename=="") {
            salinity_profiles_filename = "graph_salinity_profiles.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }

    // Filename for density profiles graph
    string density_profiles_filename = "";
    if (opt->getValue("denprofiles") != NULL) {
        density_profiles_filename = opt->getValue("denprofiles");
        if (density_profiles_filename=="") {
            density_profiles_filename = "graph_density_profiles.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }
    // Filename for ocean heat content profiles graph
    string ohc_profiles_filename = "";
    if (opt->getValue("ohcprofiles") != NULL) {
        ohc_profiles_filename = opt->getValue("ohcprofiles");
        if (ohc_profiles_filename=="") {
            ohc_profiles_filename = "graph_ohc_profiles.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }

    // Filename for oxygen profiles graph
    string oxygen_profiles_filename = "";
    if (opt->getValue("oxyprofiles") != NULL) {
        oxygen_profiles_filename = opt->getValue("oxyprofiles");
        if (oxygen_profiles_filename=="") {
            oxygen_profiles_filename = "graph_oxygen_profiles.png";
        }
        if (!year_step_set) year_step = end_year - start_year;
    }

    bool threed=false;
    string map_averages_filename = "";
    if (opt->getValue("mapaverages") != NULL) {
        map_averages_filename = opt->getValue("mapaverages");
        if (map_averages_filename=="") {
            map_averages_filename = "map_averages.png";
        }
    }
    if (opt->getValue("mapaverages3d") != NULL) {
        map_averages_filename = opt->getValue("mapaverages3d");
        if (map_averages_filename=="") {
            map_averages_filename = "map_averages3d.png";
        }
        threed=true;
    }

    string map_temperature_anomalies_filename = "";
    bool anomalies_threed = false;
    if (opt->getValue("maptempanomalies") != NULL) {
        map_temperature_anomalies_filename =
            opt->getValue("maptempanomalies");

        if (map_temperature_anomalies_filename=="") {
            map_temperature_anomalies_filename =
                "map_temperature_anomalies.png";
        }
        use_reference_period=true;
    }
    if (opt->getValue("maptempanomalies3d") != NULL) {
        map_temperature_anomalies_filename =
            opt->getValue("maptempanomalies3d");

        if (map_temperature_anomalies_filename=="") {
            map_temperature_anomalies_filename =
                "map_temperature_anomalies3d.png";
        }
        anomalies_threed=true;
        use_reference_period=true;
    }

    string map_salinity_anomalies_filename = "";
    if (opt->getValue("mapsalanomalies") != NULL) {
        map_salinity_anomalies_filename =
            opt->getValue("mapsalanomalies");
        if (map_salinity_anomalies_filename=="") {
            map_salinity_anomalies_filename =
                "map_salinity_anomalies.png";
        }
        anomalies_threed=false;
        use_reference_period=true;
    }
    if (opt->getValue("mapsalanomalies3d") != NULL) {
        map_salinity_anomalies_filename =
            opt->getValue("mapsalanomalies3d");
        if (map_salinity_anomalies_filename=="") {
            map_salinity_anomalies_filename =
                "map_salinity_anomalies3d.png";
        }
        anomalies_threed=true;
        use_reference_period=true;
    }

    string map_density_anomalies_filename = "";
    bool density_anomalies_threed=false;
    if (opt->getValue("mapdenanomalies") != NULL) {
        map_density_anomalies_filename =
            opt->getValue("mapdenanomalies");
        if (map_density_anomalies_filename=="") {
            map_density_anomalies_filename = "map_density_anomalies.png";
        }
        density_anomalies_threed=false;
        use_reference_period=true;
    }
    if (opt->getValue("mapdenanomalies3d") != NULL) {
        map_density_anomalies_filename =
            opt->getValue("mapdenanomalies3d");
        if (map_density_anomalies_filename=="") {
            map_density_anomalies_filename =
                "map_density_anomalies3d.png";
        }
        density_anomalies_threed=true;
        use_reference_period=true;
    }

    string map_ohc_anomalies_filename = "";
    if (opt->getValue("mapohcanomalies") != NULL) {
        map_ohc_anomalies_filename =
            opt->getValue("mapohcanomalies");
        if (map_ohc_anomalies_filename=="") {
            map_ohc_anomalies_filename = "map_ohc_anomalies.png";
        }
        density_anomalies_threed=false;
        use_reference_period=true;
    }
    if (opt->getValue("mapohcanomalies3d") != NULL) {
        map_ohc_anomalies_filename =
            opt->getValue("mapohcanomalies3d");
        if (map_ohc_anomalies_filename=="") {
            map_ohc_anomalies_filename =
                "map_ohc_anomalies3d.png";
        }
        density_anomalies_threed=true;
        use_reference_period=true;
    }

    string map_oxygen_anomalies_filename = "";
    //bool oxygen_anomalies_threed=false;
    if (opt->getValue("mapoxyanomalies") != NULL) {
        map_oxygen_anomalies_filename =
            opt->getValue("mapoxyanomalies");
        if (map_oxygen_anomalies_filename=="") {
            map_oxygen_anomalies_filename = "map_oxygen_anomalies.png";
        }
        //oxygen_anomalies_threed=false;
        use_reference_period=true;
    }
    if (opt->getValue("mapoxyanomalies3d") != NULL) {
        map_oxygen_anomalies_filename =
            opt->getValue("mapoxyanomalies3d");
        if (map_oxygen_anomalies_filename=="") {
            map_oxygen_anomalies_filename = "map_oxygen_anomalies3d.png";
        }
        //oxygen_anomalies_threed=true;
        use_reference_period=true;
    }

    string map_salinity_averages_filename = "";
    bool salinity_threed=false;
    if (opt->getValue("mapsalaverages") != NULL) {
        map_salinity_averages_filename = opt->getValue("mapsalaverages");
        if (map_salinity_averages_filename=="") {
            map_salinity_averages_filename = "map_salinity_averages.png";
        }
    }
    if (opt->getValue("mapsalaverages3d") != NULL) {
        map_salinity_averages_filename =
            opt->getValue("mapsalaverages3d");
        if (map_salinity_averages_filename=="") {
            map_salinity_averages_filename =
                "map_salinity_averages3d.png";
        }
        salinity_threed=true;
    }

    string map_density_averages_filename = "";
    bool density_threed=false;
    if (opt->getValue("mapdenaverages") != NULL) {
        map_density_averages_filename =
            opt->getValue("mapdenaverages");
        if (map_density_averages_filename=="") {
            map_density_averages_filename = "map_density_averages.png";
        }
    }
    if (opt->getValue("mapdenaverages3d") != NULL) {
        map_density_averages_filename =
            opt->getValue("mapdenaverages3d");
        if (map_density_averages_filename=="") {
            map_density_averages_filename = "map_density_averages3d.png";
        }
        density_threed=true;
    }

    string map_ohc_averages_filename = "";
    bool ohc_threed=false;
    if (opt->getValue("mapohcaverages") != NULL) {
        map_ohc_averages_filename =
            opt->getValue("mapohcaverages");
        if (map_ohc_averages_filename=="") {
            map_ohc_averages_filename = "map_ohc_averages.png";
        }
    }
    if (opt->getValue("mapohcaverages3d") != NULL) {
        map_ohc_averages_filename =
            opt->getValue("mapohcaverages3d");
        if (map_ohc_averages_filename=="") {
            map_ohc_averages_filename = "map_ohc_averages3d.png";
        }
        density_threed=true;
    }

    string map_oxygen_averages_filename = "";
    bool oxygen_threed=false;
    if (opt->getValue("mapoxyaverages") != NULL) {
        map_oxygen_averages_filename = opt->getValue("mapoxyaverages");
        if (map_oxygen_averages_filename=="") {
            map_oxygen_averages_filename = "map_oxygen_averages.png";
        }
    }
    if (opt->getValue("mapoxyaverages3d") != NULL) {
        map_oxygen_averages_filename = opt->getValue("mapoxyaverages3d");
        if (map_oxygen_averages_filename=="") {
            map_oxygen_averages_filename = "map_oxygen_averages3d.png";
        }
        oxygen_threed=true;
    }

    // Show the locations of platforms
    string map_platforms_filename = "";
    if (opt->getValue("mapplatforms") != NULL) {
        map_platforms_filename = opt->getValue("mapplatforms");
        if (map_platforms_filename=="") {
            map_platforms_filename = "graph_platforms.png";
        }
    }
    if (opt->getValue("mapplatforms3d") != NULL) {
        map_platforms_filename = opt->getValue("mapplatforms3d");
        if (map_platforms_filename=="") {
            map_platforms_filename = "graph_platforms3d.png";
        }
        threed = true;
    }

    // set a single year for the graph
    if (opt->getValue("year") != NULL) {
        start_year = atoi(opt->getValue("year"));
        end_year = start_year;
        year_step = 1;
    }

    if (opt->getValue("month") != NULL) {
        month = atoi(opt->getValue("month"));
        if (month < 1) month = 1;
        if (month > 12) month = 12;
    }

    // Plot temperature cycles for a single platform
    string temperature_cycles_filename="";
    if (opt->getValue("tempcycles") != NULL) {
        temperature_cycles_filename = opt->getValue("tempcycles");
        if (temperature_cycles_filename=="") {
            temperature_cycles_filename = "graph_temperature_cycles.png";
        }
    }

    // Plot salinity cycles for a single platform
    string salinity_cycles_filename="";
    if (opt->getValue("salcycles") != NULL) {
        salinity_cycles_filename = opt->getValue("salcycles");
        if (salinity_cycles_filename=="") {
            salinity_cycles_filename = "graph_salinity_cycles.png";
        }
    }

    // Plot density cycles for a single platform
    string density_cycles_filename="";
    if (opt->getValue("dencycles") != NULL) {
        density_cycles_filename = opt->getValue("dencycles");
        if (density_cycles_filename=="") {
            density_cycles_filename = "graph_density_cycles.png";
        }
    }

    // Plot ohc cycles for a single platform
    string ohc_cycles_filename="";
    if (opt->getValue("ohccycles") != NULL) {
        ohc_cycles_filename = opt->getValue("ohccycles");
        if (ohc_cycles_filename=="") {
            ohc_cycles_filename = "graph_ohc_cycles.png";
        }
    }

    // Plot oxygen cycles for a single platform
    string oxygen_cycles_filename="";
    if (opt->getValue("oxycycles") != NULL) {
        oxygen_cycles_filename = opt->getValue("oxycycles");
        if (oxygen_cycles_filename=="") {
            oxygen_cycles_filename = "graph_oxygen_cycles.png";
        }
    }

    // Plot temperatures at each latitude
    string temperature_latitudes_filename="";
    if (opt->getValue("templatitudes") != NULL) {
        temperature_latitudes_filename = opt->getValue("templatitudes");
        if (temperature_latitudes_filename=="") {
            temperature_latitudes_filename =
                "graph_temperature_latitudes.png";
        }
    }

    // Plot salinity at each latitude
    string salinity_latitudes_filename="";
    if (opt->getValue("sallatitudes") != NULL) {
        salinity_latitudes_filename = opt->getValue("sallatitudes");
        if (salinity_latitudes_filename=="") {
            salinity_latitudes_filename = "graph_salinity_latitudes.png";
        }
    }

    // Plot density at each latitude
    string density_latitudes_filename="";
    if (opt->getValue("denlatitudes") != NULL) {
        density_latitudes_filename = opt->getValue("denlatitudes");
        if (density_latitudes_filename=="") {
            density_latitudes_filename = "graph_density_latitudes.png";
        }
    }

    // Plot ohc at each latitude
    string ohc_latitudes_filename="";
    if (opt->getValue("ohclatitudes") != NULL) {
        ohc_latitudes_filename = opt->getValue("ohclatitudes");
        if (ohc_latitudes_filename=="") {
            ohc_latitudes_filename = "graph_ohc_latitudes.png";
        }
    }

    // Plot oxygen at each latitude
    string oxygen_latitudes_filename="";
    if (opt->getValue("oxylatitudes") != NULL) {
        oxygen_latitudes_filename = opt->getValue("oxylatitudes");
        if (oxygen_latitudes_filename=="") {
            oxygen_latitudes_filename = "graph_oxygen_latitudes.png";
        }
    }

    // Plot temperature anomalies for each latitude and depth
    string temperature_latitude_anomalies_filename = "";
    if (opt->getValue("templatanomalies") != NULL) {
        temperature_latitude_anomalies_filename =
            opt->getValue("templatanomalies");
        if (temperature_latitude_anomalies_filename=="") {
            temperature_latitude_anomalies_filename =
                "graph_temperature_latitude_anomalies.png";
        }
        use_reference_period=true;
    }
    // Plot salinity anomalies for each latitude and depth
    string salinity_latitude_anomalies_filename = "";
    if (opt->getValue("sallatanomalies") != NULL) {
        salinity_latitude_anomalies_filename =
            opt->getValue("sallatanomalies");
        if (salinity_latitude_anomalies_filename=="") {
            salinity_latitude_anomalies_filename =
                "graph_salinity_latitude_anomalies.png";
        }
        use_reference_period=true;
    }
    // Plot density anomalies for each latitude and depth
    string density_latitude_anomalies_filename = "";
    if (opt->getValue("denlatanomalies") != NULL) {
        density_latitude_anomalies_filename =
            opt->getValue("denlatanomalies");
        if (density_latitude_anomalies_filename=="") {
            density_latitude_anomalies_filename =
                "graph_density_latitude_anomalies.png";
        }
        use_reference_period=true;
    }
    // Plot ohc anomalies for each latitude and depth
    string ohc_latitude_anomalies_filename = "";
    if (opt->getValue("ohclatanomalies") != NULL) {
        ohc_latitude_anomalies_filename =
            opt->getValue("ohclatanomalies");
        if (ohc_latitude_anomalies_filename=="") {
            ohc_latitude_anomalies_filename =
                "graph_ohc_latitude_anomalies.png";
        }
        use_reference_period=true;
    }
    // Plot oxygen anomalies for each latitude and depth
    string oxygen_latitude_anomalies_filename = "";
    if (opt->getValue("oxylatanomalies") != NULL) {
        oxygen_latitude_anomalies_filename =
            opt->getValue("oxylatanomalies");
        if (oxygen_latitude_anomalies_filename=="") {
            oxygen_latitude_anomalies_filename =
                "graph_oxygen_latitude_anomalies.png";
        }
        use_reference_period=true;
    }

    // Direction of currents
    string map_currents_direction_filename="";
    if (opt->getValue("direction") != NULL) {
        map_currents_direction_filename = opt->getValue("direction");
        if (map_currents_direction_filename=="") {
            map_currents_direction_filename = "map_current_direction.png";
        }
    }

    // Speed of currents
    string map_speeds_filename="";
    if (opt->getValue("speed") != NULL) {
        map_speeds_filename = opt->getValue("speed");
        if (map_speeds_filename=="") {
            map_speeds_filename = "map_speeds.png";
        }
    }

    // Turbulence of currents
    string map_currents_turbulence_filename="";
    if (opt->getValue("turbulence") != NULL) {
        map_currents_turbulence_filename = opt->getValue("turbulence");
        if (map_currents_turbulence_filename=="") {
            map_currents_turbulence_filename =
                "map_current_turbulence.png";
        }
    }

    // export profile data to a file, which may make it quicker
    // to load subsequently
    string export_filename="";
    if (opt->getValue("export") != NULL) {
        export_filename = opt->getValue("export");
    }

    // plot number of active platforms over time
    string active_platforms_filename="";
    if (opt->getValue("active") != NULL) {
        active_platforms_filename = opt->getValue("active");
    }

    // plot data centres over time
    string data_centres_filename="";
    if (opt->getValue("datacentres") != NULL) {
        data_centres_filename = opt->getValue("datacentres");
    }

    int retval = 0;
    vector<argodata> data;
    string temporary_filename = "tempnetcdf.txt";

    // obtain data
    if ((data_directory!="") || (load_filename!="")) {
        int data_start_year = start_year;
        int data_end_year = end_year;

        if (use_reference_period) {
            // ensure that all the data for the reference
            // period and plotting period is loaded
            if (data_start_year > reference_start_year) {
                data_start_year = reference_start_year;
            }
            if (data_end_year < reference_end_year) {
                data_end_year = reference_end_year;
            }
        }

        if (data_directory!="") {
            retval = argo::load_netcdf_directory(data_directory, data,
                                                 verbose,
                                                 data_start_year,
                                                 data_end_year, month,
                                                 area, platforms,
                                                 temporary_filename,
                                                 export_filename);
        }
        if (load_filename!="") {
            retval = argo::load(load_filename, data,
                                data_start_year, data_end_year, month,
                                area, platforms);
        }

        if (retval!=0) {
            printf("Can't load file %d\n", retval);
            return -1;
        }
        if (export_filename!="") {
            printf("Profile data exported to %s\n",
                   export_filename.c_str());
            return 0;
        }
        if (data.size()==0) {
            printf("No profiles loaded\n");
            return -1;
        }
        printf("%d profiles loaded\n", (int)data.size());

        int min_year=0, min_month=0, max_year=0, max_month=0;
        int range_months = argo::date_range(data, min_year,
                                            min_month, max_year,
                                            max_month);
        printf("Date range: %d-%d -> %d-%d  (%d months)\n",
               min_year,min_month,max_year,max_month, range_months);

        // If plotting platforms on the map use an
        // approximately one degree resolution
        if (map_platforms_filename!="") {
            grid_dimension = 160;
        }

        // update global grid
        vector<gridcell> grid;
        if (globalgrid::load(data, grid, area, grid_dimension)==0) {
            printf("No samples loaded into grid\n");
            return -1;
        }

        if (temperature_anomalies_filename!="") {
            gnuplot::graph_anomalies(grid, temperature_anomalies_filename,
                                     title, subtitle, subtitle_indent,
                                     start_year, end_year, month,
                                     reference_start_year,
                                     reference_end_year,
                                     show_minmax,
                                     show_running_average,
                                     show_best_fit_line,
                                     image_width, image_height,
                                     pressure, area,
                                     GRAPH_TEMPERATURE);
        }
        if (salinity_anomalies_filename!="") {
            gnuplot::graph_anomalies(grid, salinity_anomalies_filename,
                                     title, subtitle, subtitle_indent,
                                     start_year, end_year, month,
                                     reference_start_year,
                                     reference_end_year,
                                     show_minmax,
                                     show_running_average,
                                     show_best_fit_line,
                                     image_width, image_height,
                                     pressure, area,
                                     GRAPH_SALINITY);
        }
        if (density_anomalies_filename!="") {
            gnuplot::graph_anomalies(grid, density_anomalies_filename,
                                     title, subtitle, subtitle_indent,
                                     start_year, end_year, month,
                                     reference_start_year,
                                     reference_end_year,
                                     show_minmax,
                                     show_running_average,
                                     show_best_fit_line,
                                     image_width, image_height,
                                     pressure, area,
                                     GRAPH_DENSITY);
        }
        if (oxygen_anomalies_filename!="") {
            gnuplot::graph_anomalies(grid, oxygen_anomalies_filename,
                                     title, subtitle, subtitle_indent,
                                     start_year, end_year, month,
                                     reference_start_year,
                                     reference_end_year,
                                     show_minmax,
                                     show_running_average,
                                     show_best_fit_line,
                                     image_width, image_height,
                                     pressure, area,
                                     GRAPH_OXYGEN);
        }
        if (ohc_anomalies_filename!="") {
            gnuplot::graph_anomalies(grid, ohc_anomalies_filename,
                                     title, subtitle, subtitle_indent,
                                     start_year, end_year, month,
                                     reference_start_year,
                                     reference_end_year,
                                     show_minmax,
                                     show_running_average,
                                     show_best_fit_line,
                                     image_width, image_height,
                                     pressure, area,
                                     GRAPH_OHC);
        }
        if (averages_filename!="") {
            gnuplot::graph_averages(grid, averages_filename,
                                    title, subtitle, subtitle_indent,
                                    start_year, end_year, month,
                                    show_running_average,
                                    image_width, image_height,
                                    pressure_step,
                                    max_pressure, area, plot_change);
        }

        if (temperature_profiles_filename!="") {
            gnuplot::graph_profiles(grid, temperature_profiles_filename,
                                    title, subtitle, subtitle_indent,
                                    start_year, end_year, month,
                                    image_width, image_height,
                                    year_step, GRAPH_TEMPERATURE,
                                    max_pressure, area, plot_change);
        }
        if (salinity_profiles_filename!="") {
            gnuplot::graph_profiles(grid, salinity_profiles_filename,
                                    title, subtitle, subtitle_indent,
                                    start_year, end_year, month,
                                    image_width, image_height,
                                    year_step, GRAPH_SALINITY,
                                    max_pressure, area, plot_change);
        }
        if (density_profiles_filename!="") {
            gnuplot::graph_profiles(grid, density_profiles_filename,
                                    title, subtitle, subtitle_indent,
                                    start_year, end_year, month,
                                    image_width, image_height,
                                    year_step, GRAPH_DENSITY,
                                    max_pressure, area, plot_change);
        }
        if (oxygen_profiles_filename!="") {
            gnuplot::graph_profiles(grid, oxygen_profiles_filename,
                                    title, subtitle, subtitle_indent,
                                    start_year, end_year, month,
                                    image_width, image_height,
                                    year_step, GRAPH_OXYGEN,
                                    max_pressure, area, plot_change);
        }
        if (ohc_profiles_filename!="") {
            gnuplot::graph_profiles(grid, ohc_profiles_filename,
                                    title, subtitle, subtitle_indent,
                                    start_year, end_year, month,
                                    image_width, image_height,
                                    year_step, GRAPH_OHC,
                                    max_pressure, area, plot_change);
        }
        if (distribution_filename!="") {
            gnuplot::graph_distribution(grid, distribution_filename,
                                        title, subtitle, subtitle_indent,
                                        start_year, end_year, month,
                                        image_width, image_height,
                                        pressure, year_step, area);
        }
        if (diurnal_filename!="") {
            gnuplot::graph_diurnal(grid, diurnal_filename,
                                   title, subtitle, subtitle_indent,
                                   start_year, end_year, month,
                                   image_width, image_height,
                                   pressure, year_step, area,
                                   GRAPH_TEMPERATURE);
        }
        if (map_averages_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_map_averages(grid, map_averages_filename,
                                            title, subtitle, subtitle_indent,
                                            start_year, end_year, threed, month,
                                            view_longitude, view_latitude,
                                            image_width, image_height,
                                            pressure, area,
                                            GRAPH_TEMPERATURE,
                                            range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_map_averages(grid,
                                                utils::append_number_to_filename(map_averages_filename,yr),
                                                title, subtitle,
                                                subtitle_indent,
                                                yr, yr+year_step,
                                                threed, month,
                                                view_longitude, view_latitude,
                                                image_width, image_height,
                                                pressure, area,
                                                GRAPH_TEMPERATURE,
                                                range_min, range_max);
                }
            }
        }
        if (map_salinity_averages_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_map_averages(grid, map_salinity_averages_filename,
                                            title, subtitle, subtitle_indent,
                                            start_year, end_year, salinity_threed,
                                            month,
                                            view_longitude, view_latitude,
                                            image_width, image_height,
                                            pressure, area,
                                            GRAPH_SALINITY,
                                            range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_map_averages(grid,
                                                utils::append_number_to_filename(map_salinity_averages_filename,yr),
                                                title, subtitle, subtitle_indent,
                                                yr, yr+year_step, salinity_threed,
                                                month,
                                                view_longitude, view_latitude,
                                                image_width, image_height,
                                                pressure, area,
                                                GRAPH_SALINITY,
                                                range_min, range_max);
                }
            }
        }
        if (map_density_averages_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_map_averages(grid, map_density_averages_filename,
                                            title, subtitle, subtitle_indent,
                                            start_year, end_year, density_threed,
                                            month,
                                            view_longitude, view_latitude,
                                            image_width, image_height,
                                            pressure, area,
                                            GRAPH_DENSITY,
                                            range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_map_averages(grid,
                                                utils::append_number_to_filename(map_density_averages_filename,yr),
                                                title, subtitle, subtitle_indent,
                                                yr, yr+year_step, density_threed,
                                                month,
                                                view_longitude, view_latitude,
                                                image_width, image_height,
                                                pressure, area,
                                                GRAPH_DENSITY,
                                                range_min, range_max);
                }
            }
        }
        if (map_oxygen_averages_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_map_averages(grid, map_oxygen_averages_filename,
                                            title, subtitle, subtitle_indent,
                                            start_year, end_year, oxygen_threed, month,
                                            view_longitude, view_latitude,
                                            image_width, image_height,
                                            pressure, area,
                                            GRAPH_OXYGEN,
                                            range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_map_averages(grid,
                                                utils::append_number_to_filename(map_oxygen_averages_filename,yr),
                                                title, subtitle, subtitle_indent,
                                                yr, yr+year_step, oxygen_threed, month,
                                                view_longitude, view_latitude,
                                                image_width, image_height,
                                                pressure, area,
                                                GRAPH_OXYGEN,
                                                range_min, range_max);
                }
            }
        }
        if (map_ohc_averages_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_map_averages(grid, map_ohc_averages_filename,
                                            title, subtitle, subtitle_indent,
                                            start_year, end_year, ohc_threed,
                                            month,
                                            view_longitude, view_latitude,
                                            image_width, image_height,
                                            pressure, area,
                                            GRAPH_OHC,
                                            range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_map_averages(grid,
                                                utils::append_number_to_filename(map_ohc_averages_filename,yr),
                                                title, subtitle, subtitle_indent,
                                                yr, yr+year_step, ohc_threed,
                                                month,
                                                view_longitude, view_latitude,
                                                image_width, image_height,
                                                pressure, area,
                                                GRAPH_OHC,
                                                range_min, range_max);
                }
            }
        }
        if (map_platforms_filename!="") {
            gnuplot::graph_platforms(platforms,grid, map_platforms_filename,
                                     title, subtitle, subtitle_indent,
                                     start_year, end_year, threed, month,
                                     view_longitude, view_latitude,
                                     image_width, image_height,
                                     area);
        }
        if (temperature_cycles_filename!="") {
            gnuplot::graph_cycles(data, temperature_cycles_filename,
                                  title, subtitle, subtitle_indent,
                                  platforms,
                                  image_width, image_height,
                                  area, max_pressure,
                                  GRAPH_TEMPERATURE);
        }
        if (salinity_cycles_filename!="") {
            gnuplot::graph_cycles(data, salinity_cycles_filename,
                                  title, subtitle, subtitle_indent,
                                  platforms,
                                  image_width, image_height,
                                  area, max_pressure,
                                  GRAPH_SALINITY);
        }
        if (density_cycles_filename!="") {
            gnuplot::graph_cycles(data, density_cycles_filename,
                                  title, subtitle, subtitle_indent,
                                  platforms,
                                  image_width, image_height,
                                  area, max_pressure,
                                  GRAPH_DENSITY);
        }
        if (oxygen_cycles_filename!="") {
            gnuplot::graph_cycles(data, oxygen_cycles_filename,
                                  title, subtitle, subtitle_indent,
                                  platforms,
                                  image_width, image_height,
                                  area, max_pressure,
                                  GRAPH_OXYGEN);
        }
        if (temperature_latitudes_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_latitudes(data, temperature_latitudes_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         area, max_pressure,
                                         GRAPH_TEMPERATURE,
                                         start_year, end_year, month,
                                         range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_latitudes(data,
                                             utils::append_number_to_filename(temperature_latitudes_filename,yr),
                                             title, subtitle, subtitle_indent,
                                             image_width, image_height,
                                             area, max_pressure,
                                             GRAPH_TEMPERATURE,
                                             yr, yr+year_step, month,
                                             range_min, range_max);
                }
            }
        }
        if (salinity_latitudes_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_latitudes(data, salinity_latitudes_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         area, max_pressure,
                                         GRAPH_SALINITY,
                                         start_year, end_year, month,
                                         range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_latitudes(data,
                                             utils::append_number_to_filename(salinity_latitudes_filename,yr),
                                             title, subtitle, subtitle_indent,
                                             image_width, image_height,
                                             area, max_pressure,
                                             GRAPH_SALINITY,
                                             yr, yr+year_step, month,
                                             range_min, range_max);
                }
            }
        }
        if (density_latitudes_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_latitudes(data, density_latitudes_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         area, max_pressure,
                                         GRAPH_DENSITY,
                                         start_year, end_year, month,
                                         range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_latitudes(data,
                                             utils::append_number_to_filename(density_latitudes_filename,yr),
                                             title, subtitle, subtitle_indent,
                                             image_width, image_height,
                                             area, max_pressure,
                                             GRAPH_DENSITY,
                                             yr, yr+year_step, month,
                                             range_min, range_max);
                }
            }
        }
        if (oxygen_latitudes_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_latitudes(data, oxygen_latitudes_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         area, max_pressure,
                                         GRAPH_OXYGEN,
                                         start_year, end_year, month,
                                         range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_latitudes(data,
                                             utils::append_number_to_filename(oxygen_latitudes_filename,yr),
                                             title, subtitle, subtitle_indent,
                                             image_width, image_height,
                                             area, max_pressure,
                                             GRAPH_OXYGEN,
                                             yr, yr+year_step, month,
                                             range_min, range_max);
                }
            }
        }
        if (ohc_latitudes_filename!="") {
            if (!year_step_set) {
                gnuplot::graph_latitudes(data, ohc_latitudes_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         area, max_pressure,
                                         GRAPH_OHC,
                                         start_year, end_year, month,
                                         range_min, range_max);
            }
            else {
                // Plot a series of images
                for (int yr = start_year;
                     yr <= end_year-year_step; yr+=year_step) {
                    gnuplot::graph_latitudes(data,
                                             utils::append_number_to_filename(ohc_latitudes_filename,yr),
                                             title, subtitle, subtitle_indent,
                                             image_width, image_height,
                                             area, max_pressure,
                                             GRAPH_OHC,
                                             yr, yr+year_step, month,
                                             range_min, range_max);
                }
            }
        }
        if (temperature_latitude_anomalies_filename!="") {
            gnuplot::graph_latitude_anomalies(data,
                                              temperature_latitude_anomalies_filename,
                                              title, subtitle, subtitle_indent,
                                              image_width, image_height,
                                              area, max_pressure,
                                              GRAPH_TEMPERATURE,
                                              start_year, end_year,
                                              reference_start_year,
                                              reference_end_year,
                                              range_min, range_max);
        }
        if (salinity_latitude_anomalies_filename!="") {
            gnuplot::graph_latitude_anomalies(data,
                                              salinity_latitude_anomalies_filename,
                                              title, subtitle, subtitle_indent,
                                              image_width, image_height,
                                              area, max_pressure,
                                              GRAPH_SALINITY,
                                              start_year, end_year,
                                              reference_start_year,
                                              reference_end_year,
                                              range_min, range_max);
        }
        if (density_latitude_anomalies_filename!="") {
            gnuplot::graph_latitude_anomalies(data,
                                              density_latitude_anomalies_filename,
                                              title, subtitle, subtitle_indent,
                                              image_width, image_height,
                                              area, max_pressure,
                                              GRAPH_DENSITY,
                                              start_year, end_year,
                                              reference_start_year,
                                              reference_end_year,
                                              range_min, range_max);
        }
        if (oxygen_latitude_anomalies_filename!="") {
            gnuplot::graph_latitude_anomalies(data,
                                              oxygen_latitude_anomalies_filename,
                                              title, subtitle, subtitle_indent,
                                              image_width, image_height,
                                              area, max_pressure,
                                              GRAPH_OXYGEN,
                                              start_year, end_year,
                                              reference_start_year,
                                              reference_end_year,
                                              range_min, range_max);
        }
        if (ohc_latitude_anomalies_filename!="") {
            gnuplot::graph_latitude_anomalies(data,
                                              ohc_latitude_anomalies_filename,
                                              title, subtitle, subtitle_indent,
                                              image_width, image_height,
                                              area, max_pressure,
                                              GRAPH_OHC,
                                              start_year, end_year,
                                              reference_start_year,
                                              reference_end_year,
                                              range_min, range_max);
        }
        if (map_temperature_anomalies_filename!="") {
            gnuplot::graph_map_anomalies(grid,map_temperature_anomalies_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         pressure,
                                         GRAPH_TEMPERATURE,
                                         start_year, anomalies_threed,
                                         view_longitude, view_latitude,
                                         reference_start_year,
                                         reference_end_year,
                                         range_min, range_max);
        }
        if (map_salinity_anomalies_filename!="") {
            gnuplot::graph_map_anomalies(grid,map_salinity_anomalies_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         pressure,
                                         GRAPH_SALINITY,
                                         start_year, anomalies_threed,
                                         view_longitude, view_latitude,
                                         reference_start_year,
                                         reference_end_year,
                                         range_min, range_max);
        }
        if (map_density_anomalies_filename!="") {
            gnuplot::graph_map_anomalies(grid,map_density_anomalies_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         pressure,
                                         GRAPH_DENSITY,
                                         start_year, density_anomalies_threed,
                                         view_longitude, view_latitude,
                                         reference_start_year,
                                         reference_end_year,
                                         range_min, range_max);
        }
        if (map_oxygen_anomalies_filename!="") {
            gnuplot::graph_map_anomalies(grid,map_oxygen_anomalies_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         pressure,
                                         GRAPH_OXYGEN,
                                         start_year, density_anomalies_threed,
                                         view_longitude, view_latitude,
                                         reference_start_year,
                                         reference_end_year,
                                         range_min, range_max);
        }
        if (map_ohc_anomalies_filename!="") {
            gnuplot::graph_map_anomalies(grid,map_ohc_anomalies_filename,
                                         title, subtitle, subtitle_indent,
                                         image_width, image_height,
                                         pressure,
                                         GRAPH_OHC,
                                         start_year, density_anomalies_threed,
                                         view_longitude, view_latitude,
                                         reference_start_year,
                                         reference_end_year,
                                         range_min, range_max);
        }
        if (map_currents_direction_filename!="") {
            gnuplot::graph_currents(data, map_currents_direction_filename,
                                    title, subtitle, subtitle_indent,
                                    threed,
                                    start_year, end_year,
                                    view_longitude, view_latitude,
                                    image_width, image_height,
                                    GRAPH_DIRECTION,
                                    range_min, range_max);
        }
        if (map_currents_turbulence_filename!="") {
            gnuplot::graph_currents(data, map_currents_turbulence_filename,
                                    title, subtitle, subtitle_indent,
                                    threed,
                                    start_year, end_year,
                                    view_longitude, view_latitude,
                                    image_width, image_height,
                                    GRAPH_TURBULENCE,
                                    range_min, range_max);
        }
        if (map_speeds_filename!="") {
            gnuplot::graph_speeds(data, map_speeds_filename,
                                  title, subtitle, subtitle_indent,
                                  threed,
                                  start_year, end_year, month,
                                  view_longitude, view_latitude,
                                  image_width, image_height,
                                  range_min, range_max);
        }
        if (active_platforms_filename!="") {
            gnuplot::graph_active_platforms(data, active_platforms_filename,
                                            title, subtitle, subtitle_indent,
                                            start_year, end_year,
                                            image_width, image_height,
                                            area);
        }
        if (data_centres_filename!="") {
            gnuplot::graph_data_centres(data, data_centres_filename,
                                        title, subtitle, subtitle_indent,
                                        start_year, end_year,
                                        image_width, image_height,
                                        area);
        }
    }

    printf("Exit success\n");

    return 0;
}
