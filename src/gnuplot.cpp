/*
  plot oceanographic data using gnuplot
  Copyright (C) 2013-2015 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gnuplot.h"

gnuplot::gnuplot() {


}

gnuplot::~gnuplot() {

}

int gnuplot::plot_data_centres(
                               string plot_script_filename,
                               string plot_data_filename,
                               string image_filename,
                               string title,
                               string subtitle,
                               float subtitle_indent,
                               vector<argodata> &data,
                               int start_year,
                               int end_year,
                               int image_width,
                               int image_height)
{
    float minimum=9999,maximum=-9999;

    printf("done\nPlotting data centres...");

    vector<string> names;
    vector<vector<int> > qty;
    argo::get_data_centres(data,
                           start_year, end_year,
                           names, qty);

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    for (int year = start_year; year <= end_year; year++) {
        for (int month = 1; month <= 12; month++) {
            plotfile << year << "/" << month << "/" << "1";
            for (unsigned int n = 0; n < names.size(); n++) {
                int value = qty[n][((year-start_year)*12)+month-1];
                plotfile << "  " << value;
                if (value < minimum) minimum=value;
                if (value > maximum) maximum=value;
            }
            plotfile << "\n";
        }
    }
    plotfile.close();

    if (minimum==maximum) {
        printf("No variation %f\n",minimum);
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent << ", screen 0.92\n";
    }
    plotfile << "set yrange [" << minimum << ":" << maximum << "]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set tmargin 3.4\n";
    plotfile << "set xlabel \"Year\"\n";
    plotfile << "set ylabel \"Number of Cycles\"\n";
    plotfile << "set timefmt \"%Y/%m/%d\"\n";
    plotfile << "set mxtics\n";
    plotfile << "set xtics border out scale 1,1 mirror rotate by -90  " \
        "offset character 0, 0, 0 autofreq\n";
    plotfile << "set xdata time\n";
    plotfile << "set grid\n";
    plotfile << "set timestamp\n";

    string file_type = "png";
    if (image_filename != "") {
        if ((image_filename.substr((int)image_filename.size()-3,3) == "jpg") ||
            (image_filename.substr((int)image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = image_filename.substr((int)image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        plotfile << "set output \"" << image_filename << "\"\n";

        plotfile << "plot ";
        for (unsigned int n = 0; n < names.size(); n++) {
            if (n>0) plotfile << ",";
            plotfile << "\"" << plot_data_filename << "\" using 1:" <<
                2+n << " title \"" << argo::get_data_centre(names[n]) <<
                "\" with lines";
        }
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_active_platforms(
                                   string plot_script_filename,
                                   string plot_data_filename,
                                   string image_filename,
                                   string title,
                                   string subtitle,
                                   float subtitle_indent,
                                   vector<argodata> &data,
                                   int start_year,
                                   int end_year,
                                   int image_width,
                                   int image_height)
{
    float minimum=9999,maximum=-9999;

    printf("done\nPlotting active profiles...");

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    int platforms[1000][12];
#pragma omp parallel for
    for (int year = start_year; year <= end_year; year++) {
        for (int month = 1; month <= 12; month++) {
            platforms[year-start_year][month-1] =
                argo::active_platforms(data,year,month);
        }
    }

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    for (int year = start_year; year <= end_year; year++) {
        for (int month = 1; month <= 12; month++) {
            plotfile << year << "/" << month << "/" <<
                "1    " << platforms[year-start_year][month-1] << "\n";
            if (platforms[year-start_year][month-1] < minimum)
                minimum=platforms[year-start_year][month-1];
            if (platforms[year-start_year][month-1] > maximum)
                maximum=platforms[year-start_year][month-1];
        }
    }
    plotfile.close();

    if (minimum == maximum) {
        printf("No variation %f\n",minimum);
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent << ", screen 0.92\n";
    }
    plotfile << "set yrange [" << minimum << ":" << maximum << "]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set tmargin 3.4\n";
    plotfile << "set xlabel \"Year\"\n";
    plotfile << "set ylabel \"Active Platforms\"\n";
    plotfile << "set timefmt \"%Y/%m/%d\"\n";
    plotfile << "set mxtics\n";
    plotfile << "set xtics border out scale 1,1 mirror rotate by -90  " \
        "offset character 0, 0, 0 autofreq\n";
    plotfile << "set xdata time\n";
    plotfile << "set grid\n";
    plotfile << "set timestamp\n";

    string file_type = "png";
    if (image_filename != "") {
        if ((image_filename.substr((int)image_filename.size()-3,3) == "jpg") ||
            (image_filename.substr((int)image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = image_filename.substr((int)image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        plotfile << "set output \"" << image_filename << "\"\n";

        plotfile << "plot \"" << plot_data_filename <<
            "\" using 1:2 notitle with lines\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_profile(
                           string plot_script_filename,
                           string plot_data_filename,
                           string image_filename,
                           string title,
                           string subtitle,
                           float subtitle_indent,
                           vector<gridcell> &grid,
                           int start_year,
                           int end_year,
                           int image_width,
                           int image_height,
                           float year_step,
                           int graph_type,
                           float max_pressure,
                           int plot_change)
{
    const float pressure_step = 50;
    float minimum_temp=9999,maximum_temp=-9999;
    int max_year_steps = (int)((end_year-start_year+1) / year_step);
    int max_pressure_steps = (int)(max_pressure / pressure_step);
    vector<float> * time_series = new vector<float>[max_year_steps+1];
    int ps,yr=0;
    const int minimum_pressure = 10;

    printf("done\nPlotting profiles...");

    for (int year = start_year; year <= end_year; year += year_step, yr++) {
        ps=0;
        for (float pressure = minimum_pressure;
             pressure < max_pressure; pressure += pressure_step, ps++) {
            float temp=0,av=0;
            int av_hits=0;
            globalgrid::clear_temp_values(grid);
            for (int i = 0; i < (int)grid.size(); i++) {
                switch (graph_type) {
                case GRAPH_TEMPERATURE:
                    if (plot_change == 0) {
                        temp =
                            grid[i].get_average_temperature_years(year,
                                                                  year+year_step,
                                                                  pressure);
                    }
                    else {
                        temp =
                            grid[i].get_average_temperature_years_diff(year,
                                                                       year+year_step,
                                                                       pressure);
                    }
                    if ((temp < 0.1f) || (temp > 40.0f)) temp = -9999.0f;
                    break;
                case GRAPH_SALINITY:
                    if (plot_change == 0) {
                        temp =
                            grid[i].get_average_salinity_years(year,
                                                               year+year_step,
                                                               pressure);
                    }
                    else {
                        temp =
                            grid[i].get_average_salinity_years_diff(year,
                                                                    year+year_step,
                                                                    pressure);
                    }
                    break;
                case GRAPH_DENSITY: {
                    float t,s;
                    if (plot_change == 0) {
                        t =
                            grid[i].get_average_temperature_years(year,
                                                                  year+year_step,
                                                                  pressure);
                        s =
                            grid[i].get_average_salinity_years(year,
                                                               year+year_step,
                                                               pressure);
                    }
                    else {
                        t =
                            grid[i].get_average_temperature_years_diff(year,
                                                                       year+year_step,
                                                                       pressure);
                        s =
                            grid[i].get_average_salinity_years_diff(year,
                                                                    year+year_step,
                                                                    pressure);
                    }
                    if ((t>-9998) && (s>-9998)) {
                        temp = argo::density(t,s);
                    }
                    else {
                        temp=-9999;
                    }
                    break;
                }
                case GRAPH_OXYGEN:
                    if (plot_change == 0) {
                        temp =
                            grid[i].get_average_oxygen_years(year,
                                                             year+year_step,
                                                             pressure);
                    }
                    else {
                        temp =
                            grid[i].get_average_oxygen_years_diff(year,
                                                                  year+year_step,
                                                                  pressure);
                    }
                    break;
                case GRAPH_OHC: {
                    float t,s;
                    if (plot_change == 0) {
                        t =
                            grid[i].get_average_temperature_years(year,
                                                                  year+year_step,
                                                                  pressure);
                        s =
                            grid[i].get_average_salinity_years(year,
                                                               year+year_step,
                                                               pressure);
                    }
                    else {
                        t =
                            grid[i].get_average_temperature_years_diff(year,
                                                                       year+year_step,
                                                                       pressure);
                        s =
                            grid[i].get_average_salinity_years_diff(year,
                                                                    year+year_step,
                                                                    pressure);
                    }
                    if ((t>9998) && (s>-9998)) {
                        temp = argo::ocean_heat_content(t,s,pressure);
                    }
                    else {
                        temp=-9999;
                    }
                    break;
                }
                }
                if (temp > -9998) {
                    av += temp;
                    av_hits++;
                }
            }
            if (av_hits > 0) {
                av /= av_hits;
                if (av < minimum_temp) minimum_temp=av;
                if (av > maximum_temp) maximum_temp=av;
                time_series[yr].push_back(av);
            }
            else {
                time_series[yr].push_back(-9999);
            }
        }
    }

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    for (ps = 0; ps < max_pressure_steps; ps++) {
        plotfile << (ps*pressure_step);

        yr=0;
        float prev = time_series[0][ps];
        for (int y = start_year; y <= end_year; y += year_step, yr++) {
            float curr = time_series[yr][ps];
            if (curr==-9999) {
                curr = prev;
            }
            plotfile << "    " << curr;
            prev = curr;
        }
        plotfile << "\n";

    }
    plotfile.close();

    if (minimum_temp==maximum_temp) {
        printf("No variation %f\n",minimum_temp);
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle <<
            "\" at screen " << subtitle_indent <<
            ", screen 0.92\n";
    }
    plotfile << "set xrange [" << minimum_temp << ":" << maximum_temp << "]\n";
    plotfile << "set yrange [" << max_pressure << ":0]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set ylabel \"Depth (metres)\"\n";
    switch(graph_type) {
    case GRAPH_TEMPERATURE:
        plotfile << "set xlabel \"Average Temperature (Celcius)\"\n";
        break;
    case GRAPH_SALINITY:
        plotfile << "set xlabel \"Average Salinity (Parts per Thousand)\"\n";
        break;
    case GRAPH_DENSITY:
        plotfile << "set xlabel \"Average Density (Gram per cm^3)\"\n";
        break;
    case GRAPH_OXYGEN:
        plotfile << "set xlabel \"Average Oxygen (Micromole/kg)\"\n";
        break;
    case GRAPH_OHC:
        plotfile << "set xlabel \"Ocean Heat Content (J)\"\n";
        break;
    }
    plotfile << "set grid\n";
    plotfile << "set key right bottom\n";

    string file_type = "png";
    if (image_filename != "") {
        if ((image_filename.substr((int)image_filename.size()-3,3) == "jpg") ||
            (image_filename.substr((int)image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = image_filename.substr((int)image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " << image_width <<
            "," << image_height << "\n";

        plotfile << "set output \"" << image_filename << "\"\n";

        plotfile << "plot ";
        for (yr = 0; yr < max_year_steps; yr++) {
            plotfile << "\"" << plot_data_filename << "\" using " <<
                (2+yr) << ":1";
            plotfile << " title \"" << (start_year + (yr * year_step)) <<
                "-" << (start_year + ((yr+1) * year_step)) << "\" with lines";
            if (yr<max_year_steps-1) plotfile << ", ";
        }
        plotfile << "\n";
    }

    plotfile.close();

    printf("done\n");

    delete [] time_series;

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_averages(
                           string plot_script_filename,
                           string plot_data_filename,
                           string image_filename,
                           string title,
                           string subtitle,
                           float subtitle_indent,
                           vector<gridcell> &grid,
                           int start_year,
                           int end_year,
                           bool show_running_average,
                           int image_width,
                           int image_height,
                           float pressure_step,
                           float max_pressure,
                           int plot_change)
{
    float minimum_temp=9999,maximum_temp=-9999;
    int steps = (int)(max_pressure / pressure_step);
    vector<float> * time_series = new vector<float>[steps];
    int ps=0;
    const int minimum_pressure = 10;
    int reference_year = start_year;

    printf("done\nPlotting averages...");

    for (float pressure = minimum_pressure; pressure < max_pressure;
         pressure += pressure_step, ps++) {
        float prev_av=0;
        for (int year=start_year; year<=end_year; year++) {

#pragma omp parallel for
            for (int i = 0; i < (int)grid.size(); i++) {
                if (plot_change == 0) {
                    grid[i].temp_value =
                        grid[i].get_average_temperature(year, pressure);
                }
                else {
                    grid[i].temp_value =
                        grid[i].get_average_temperature_diff(year, pressure, reference_year);
                }
            }

            float av=0;
            int av_hits=0;
            for (int i = 0; i < (int)grid.size(); i++) {
                float temp = grid[i].temp_value;
                if ((temp > 0.1f) && (temp < 40.0f)) {
                    av += temp;
                    av_hits++;
                }
            }
            if (av_hits > 0) {
                av /= av_hits;
                if (av < minimum_temp) minimum_temp=av;
                if (av > maximum_temp) maximum_temp=av;
                time_series[ps].push_back(av);
                prev_av = av;
            }
            else {
                time_series[ps].push_back(prev_av);
            }
        }
    }

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    float * running_average = new float[steps];
    for (ps = 0; ps < steps; ps++) {
        running_average[ps] = time_series[ps][0];
    }
    for (int y = start_year; y <= end_year; y++) {
        int idx = y - start_year;

        plotfile << y;

        for (ps = 0; ps < steps; ps++) {
            if (!show_running_average) {
                plotfile << "    " << time_series[ps][idx];
            }
            else {
                plotfile << "    " << running_average[ps];
                running_average[ps] =
                    (running_average[ps]*0.8f)+(time_series[ps][idx]*0.2f);
                if (running_average[ps]>maximum_temp) {
                    maximum_temp = running_average[ps];
                }
                if (running_average[ps]<minimum_temp) {
                    minimum_temp = running_average[ps];
                }
            }
        }
        plotfile << "\n";

    }
    plotfile.close();

    if (minimum_temp==maximum_temp) {
        printf("No temperature variation %f\n",minimum_temp);
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent << ", screen 0.92\n";
    }
    plotfile << "set yrange [" << minimum_temp << ":" << maximum_temp << "]\n";
    plotfile << "set xrange [" << start_year << ":" << end_year << "]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set xlabel \"Year\"\n";
    plotfile << "set ylabel \"Average Temperature (Celcius)\n";
    plotfile << "set grid\n";
    plotfile << "set key right bottom\n";

    string file_type = "png";
    if (image_filename != "") {
        if ((image_filename.substr((int)image_filename.size()-3,3) == "jpg") ||
            (image_filename.substr((int)image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = image_filename.substr((int)image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        plotfile << "set output \"" << image_filename << "\"\n";

        plotfile << "plot ";
        for (ps = 0; ps < steps; ps++) {
            plotfile << "\"" << plot_data_filename << "\" using 1:" << (2+ps);
            plotfile << " title \"" << (int)(ps * pressure_step) <<
                "m\" with lines";
            if (ps<steps-1) plotfile << ", ";
        }
        plotfile << "\n";
    }

    plotfile.close();

    printf("done\n");

    delete [] time_series;
    delete [] running_average;

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_anomalies(
                            string plot_script_filename,
                            string plot_data_filename,
                            string anomalies_image_filename,
                            string title,
                            string subtitle,
                            float subtitle_indent,
                            vector<gridcell> &grid,
                            int start_year,
                            int end_year,
                            int reference_start_year,
                            int reference_end_year,
                            bool show_minmax,
                            bool show_running_average,
                            bool best_fit_line,
                            int image_width,
                            int image_height,
                            float pressure,
                            int graph_type)
{
    float minimum_anomaly=9999,maximum_anomaly=-9999;
    vector<float> time_series;
    vector<float> time_series_min;
    vector<float> time_series_max;

    printf("Calculating model for reference period...");

#pragma omp parallel for
    for (int i = 0; i < (int)grid.size(); i++) {
        grid[i].update(reference_start_year,reference_end_year,
                       pressure, graph_type);
    }

    printf("done\nPlotting anomalies...");

    for (int year = start_year; year <= end_year; year++) {

#pragma omp parallel for
        for (int i = 0; i < (int)grid.size(); i++) {
            grid[i].temp_value =
                grid[i].get_anomaly(year,reference_start_year,
                                    reference_end_year, pressure, graph_type);
        }

        float anomaly=0,av_min=0,av_max=0;
        int anomaly_hits=0,av_hits=0;
        for (int i = 0; i < (int)grid.size(); i++) {
            float anom = grid[i].temp_value;
            if (anom>-9999.0f) {
                float min=9999,max=-9999;
                if (show_minmax) {
                    grid[i].get_variance(year,min,max,reference_start_year,
                                         reference_end_year, pressure,
                                         graph_type);
                    av_min += min;
                    av_max += max;
                    av_hits++;
                }
                anomaly += anom;
                anomaly_hits++;
            }
        }
        if (anomaly_hits>0) {
            anomaly /= anomaly_hits;
            if (anomaly < minimum_anomaly) minimum_anomaly = anomaly;
            if (anomaly > maximum_anomaly) maximum_anomaly = anomaly;
            if (show_minmax) {
                av_min = (float)(av_min / av_hits);
                av_max = (float)(av_max / av_hits);
                if (av_min < minimum_anomaly) minimum_anomaly = av_min;
                if (av_max > maximum_anomaly) maximum_anomaly = av_max;
            }
        }
        time_series.push_back(anomaly);
        time_series_min.push_back(av_min);
        time_series_max.push_back(av_max);
    }

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    float running_average = time_series[0];
    if ((show_running_average) && (!show_minmax)) {
        minimum_anomaly=9999;
        maximum_anomaly=-9999;
    }
    for (int y = start_year; y <= end_year; y++) {
        int idx = y - start_year;
        plotfile << y << "    " << time_series[idx];
        plotfile << "    " << time_series_min[idx];
        plotfile << "    " << time_series_max[idx];
        plotfile << "    " << running_average;
        plotfile << "\n";
        running_average = (running_average*0.8f)+(time_series[idx]*0.2f);

        if ((show_running_average) && (!show_minmax)) {
            if (running_average>maximum_anomaly) {
                maximum_anomaly = running_average;
            }
            if (running_average<minimum_anomaly) {
                minimum_anomaly = running_average;
            }
        }
    }
    plotfile.close();

    if (minimum_anomaly==maximum_anomaly) {
        printf("\nWARNING: No anomaly variation %f\n",minimum_anomaly);
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent << ", screen 0.92\n";
    }
    plotfile << "set yrange [" << minimum_anomaly << ":" <<
        maximum_anomaly << "]\n";
    plotfile << "set xrange [" << start_year << ":" << end_year << "]\n";
    plotfile << "set lmargin 10\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set xlabel \"Year\"\n";
    switch (graph_type) {
    case GRAPH_TEMPERATURE:
        plotfile << "set ylabel \"Average Anomaly (Celcius) relative to ";
        break;
    case GRAPH_SALINITY:
        plotfile << "set ylabel \"Average Anomaly (Parts per Thousand) " \
            "relative to ";
        break;
    case GRAPH_DENSITY:
        plotfile << "set ylabel \"Average Anomaly (Gram per cm^3) relative to ";
        break;
    case GRAPH_OXYGEN:
        plotfile << "set ylabel \"Average Anomaly (Micromole per Kg) " \
            "relative to ";
        break;
    case GRAPH_OHC:
        plotfile << "set ylabel \"Ocean Heat Content (J) relative to ";
        break;
    }
    plotfile << reference_start_year << " - " << reference_end_year << "\"\n";
    plotfile << "set grid\n";
    plotfile << "set key right bottom\n";

    string file_type = "png";
    if (anomalies_image_filename != "") {
        if ((anomalies_image_filename.substr((int)anomalies_image_filename.size()-3,3) == "jpg") ||
            (anomalies_image_filename.substr((int)anomalies_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type =
                anomalies_image_filename.substr((int)anomalies_image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        if (best_fit_line) {
            plotfile << "b = 0.0\n";
            plotfile << "m = 0.0\n";
            plotfile << "f(x) = m*x + b\n";
            plotfile << "fit f(x) \"" << plot_data_filename <<
                "\" using 1:2 via b, m\n";
        }
        plotfile << "set output \"" << anomalies_image_filename << "\"\n";
        if (show_running_average) {
            plotfile << "plot \"" << plot_data_filename << "\" using 1:5";
        }
        else {
            plotfile << "plot \"" << plot_data_filename << "\" using 1:2";
        }

        if (!show_minmax) {
            plotfile << " notitle with lines";
            if (best_fit_line) plotfile << ", f(x) title \"Best fit\"";
        }
        else {
            plotfile << " title \"Mean\" with lines,";
            plotfile << "\"" << plot_data_filename <<
                "\" using 1:3 title \"Min\" with lines";
            plotfile << ", \"" << plot_data_filename <<
                "\" using 1:4 title \"Max\" with lines";
            if (best_fit_line) {
                plotfile << ", f(x) title \"Best fit\"";
            }
        }


        plotfile << "\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}


int gnuplot::plot_distribution(
                               string plot_script_filename,
                               string plot_data_filename,
                               string distribution_image_filename,
                               string title,
                               string subtitle,
                               float subtitle_indent,
                               vector<gridcell> &grid,
                               int start_year,
                               int end_year,
                               int year_step,
                               int image_width,
                               int image_height,
                               float pressure)
{
    int intervals = 1;
    if (year_step>0) intervals = (end_year-start_year)/year_step;
    if (intervals<1) intervals=1;
    float ** histogram = new float*[intervals];
    int * hits = new int[intervals];
    int min_temp=9999,max_temp=-9999;

    printf("Plotting distribution...");

    for (int i = 0; i < intervals; i++) {
        histogram[i] = new float[200];
    }

    for (int p = 0; p < intervals; p++) {

        hits[p]=0;
        for (int s=0; s < 200; s++) histogram[p][s]=0;

        int interval_start_year =
            start_year + (p*(end_year-start_year)/intervals);
        int interval_end_year =
            start_year + ((p+1)*(end_year-start_year)/intervals);

        for (int year=interval_start_year; year<interval_end_year; year++) {

            globalgrid::clear_temp_values(grid);
#pragma omp parallel for
            for (int i = 0; i < (int)grid.size(); i++) {
                if (grid[i].update_histogram(year,pressure)) {
                    grid[i].temp_value = 1;
                }
            }
            for (int i = 0; i < (int)grid.size(); i++) {
                if (grid[i].temp_value == 1) {
                    for (int s = 0; s < 200; s++) {
                        histogram[p][s] += grid[i].histogram[s];
                    }
                    hits[p]++;
                }
            }
        }

        if (hits[p]>0) {
            for (int i = 0; i < 200; i++) {
                histogram[p][i] /= (float)hits[p];
                if (histogram[p][i]>0) {
                    if (i-100>max_temp) max_temp = i-100;
                    if (i-100<min_temp) min_temp = i-100;
                }
            }
        }
    }

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    float max=0;
    for (int s = 0; s < 200; s++) {
        plotfile << (s-100);
        for (int p = 0; p < intervals; p++) {
            plotfile << "    " << histogram[p][s];
            if (histogram[p][s]>max) max = histogram[p][s];
        }
        plotfile << "\n";
    }
    plotfile.close();

    if (max==0) {
        printf("No data\n");
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent << ", screen 0.92\n";
    }
    plotfile << "set yrange [0:" << max << "]\n";
    plotfile << "set xrange [" << min_temp << ":" << max_temp << "]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    plotfile << "set xlabel \"Temperature (Celcius)\"\n";
    plotfile << "set ylabel \"Normalised Samples\"\n";
    plotfile << "set grid\n";
    plotfile << "set key left top\n";

    string file_type = "png";
    if (distribution_image_filename != "") {
        if ((distribution_image_filename.substr((int)distribution_image_filename.size()-3,3) == "jpg") ||
            (distribution_image_filename.substr((int)distribution_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type =
                distribution_image_filename.substr((int)distribution_image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        plotfile << "set output \"" << distribution_image_filename << "\"\n";

        plotfile << "plot ";

        for (int p = 0; p < intervals; p++) {
            if (p>0) plotfile << ",";
            plotfile << "\"" << plot_data_filename << "\" using 1:" << (2+p);
            if (intervals>1) {
                plotfile << " title \"" << (start_year+(p*year_step)) <<
                    " - " << (start_year+((p+1)*year_step)) << "\"";
            }
            else {
                plotfile << " notitle";
            }
            plotfile << " with lines";
        }

        plotfile << "\n";
    }
    else {
        printf("\nERROR: No image filename specified\n");
    }

    plotfile.close();

    printf("done\n");

    delete [] hits;
    for (int i = 0; i < intervals; i++) {
        delete [] histogram[i];
    }
    delete [] histogram;

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_diurnal(string plot_script_filename,
                          string plot_data_filename,
                          string diurnal_image_filename,
                          string title,
                          string subtitle,
                          float subtitle_indent,
                          vector<gridcell> &grid,
                          int start_year,
                          int end_year,
                          int year_step,
                          int month,
                          int image_width,
                          int image_height,
                          float pressure,
                          int graph_type)
{
    int intervals = 1;
    if (year_step>0) intervals = (end_year-start_year)/year_step;
    if (intervals<1) intervals=1;
    float ** diurnal = new float*[intervals];
    int diurnal_hits[DIURNAL_SAMPLES];
    int min_temp=9999, max_temp=-9999;

    printf("Plotting diurnal...");

    for (int i = 0; i < intervals; i++) {
        diurnal[i] = new float[DIURNAL_SAMPLES];
    }

    for (int p = 0; p < intervals; p++) {
        for (int s=0; s < DIURNAL_SAMPLES; s++) diurnal[p][s]=0;
        for (int s=0; s < DIURNAL_SAMPLES; s++) diurnal_hits[s]=0;

        int interval_start_year =
            start_year + (p*(end_year-start_year)/intervals);
        int interval_end_year =
            start_year + ((p+1)*(end_year-start_year)/intervals);

        for (int year=interval_start_year; year<interval_end_year; year++) {
            globalgrid::clear_temp_values(grid);
#pragma omp parallel for
            for (int i = 0; i < (int)grid.size(); i++) {
                grid[i].update_diurnal(year,month,pressure,graph_type);
            }
            for (int i = 0; i < (int)grid.size(); i++) {
                for (int s=0; s < DIURNAL_SAMPLES; s++) {
                    if (grid[i].diurnal[s] > -9999) {
                        diurnal[p][s] += grid[i].diurnal[s];
                        diurnal_hits[s]++;
                    }
                }
            }
        }

        for (int s=0; s < DIURNAL_SAMPLES; s++) {
            if (diurnal_hits[s] > 0) {
                diurnal[p][s] /= diurnal_hits[s];
                if (diurnal[p][s] > max_temp) max_temp = diurnal[p][s];
                if (diurnal[p][s] < min_temp) min_temp = diurnal[p][s];
            }
        }
    }

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    ofstream plotfile;
    plotfile.open(plot_data_filename.c_str());
    float max=0;
    for (int s = 0; s < DIURNAL_SAMPLES; s++) {
        plotfile << s;
        for (int p = 0; p < intervals; p++) {
            plotfile << "    " << diurnal[p][s];
            if (diurnal[p][s]>max) max = diurnal[p][s];
        }
        plotfile << "\n";
    }
    plotfile.close();

    if (max==0) {
        printf("No data\n");
        return -1;
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent << ", screen 0.92\n";
    }
    plotfile << "set xrange [0:24]\n";
    plotfile << "set yrange [" << min_temp << ":" << max_temp << "]\n";
    plotfile << "set lmargin 9\n";
    plotfile << "set rmargin 2\n";
    switch(graph_type) {
    case GRAPH_TEMPERATURE:
        plotfile << "set ylabel \"Temperature (Celcius)\"\n";
        break;
    case GRAPH_SALINITY:
        plotfile << "set ylabel \"Average Salinity (Parts per Thousand)\"\n";
        break;
    case GRAPH_DENSITY:
        plotfile << "set ylabel \"Average Density (Gram per cm^3)\"\n";
        break;
    case GRAPH_OXYGEN:
        plotfile << "set ylabel \"Average Oxygen (Micromole/kg)\"\n";
        break;
    case GRAPH_OHC:
        plotfile << "set ylabel \"Ocean Heat Content (J)\"\n";
        break;
    }
    plotfile << "set xlabel \"Time of Day\"\n";
    plotfile << "set grid\n";
    plotfile << "set key left top\n";

    string file_type = "png";
    if (diurnal_image_filename != "") {
        if ((diurnal_image_filename.substr((int)diurnal_image_filename.size()-3,3) == "jpg") ||
            (diurnal_image_filename.substr((int)diurnal_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type =
                diurnal_image_filename.substr((int)diurnal_image_filename.size()-3,3);
        }

        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";

        plotfile << "set output \"" << diurnal_image_filename << "\"\n";

        plotfile << "plot ";

        for (int p = 0; p < intervals; p++) {
            if (p>0) plotfile << ",";
            plotfile << "\"" << plot_data_filename << "\" using 1:" << (2+p);
            if (intervals>1) {
                plotfile << " title \"" << (start_year+(p*year_step)) <<
                    " - " << (start_year+((p+1)*year_step)) << "\"";
            }
            else {
                plotfile << " notitle";
            }
            plotfile << " with lines";
        }

        plotfile << "\n";
    }
    else {
        printf("\nERROR: No image filename specified\n");
    }

    plotfile.close();

    printf("done\n");

    for (int i = 0; i < intervals; i++) {
        delete [] diurnal[i];
    }
    delete [] diurnal;

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}


void gnuplot::save_world_map(string filename)
{
    ofstream plotfile;

    std::remove(filename.c_str());

    plotfile.open(filename.c_str());

    plotfile << "#\n";
    plotfile << "# $Id: world.dat,v 1.2 2006/06/02 06:01:44 sfeam Exp $\n";
    plotfile << "#\n";
    plotfile << "#\n";
    plotfile << "-92.32  48.24\n";
    plotfile << "-88.13  48.92\n";
    plotfile << "-83.11  46.27\n";
    plotfile << "-81.66  44.76\n";
    plotfile << "-82.09  42.29\n";
    plotfile << "-77.10  44.00\n";
    plotfile << "-69.95  46.92\n";
    plotfile << "-65.92  45.32\n";
    plotfile << "-66.37  44.25\n";
    plotfile << "-61.22  45.43\n";
    plotfile << "-64.94  47.34\n";
    plotfile << "-64.12  48.52\n";
    plotfile << "-70.68  47.02\n";
    plotfile << "-67.24  49.33\n";
    plotfile << "-59.82  50.48\n";
    plotfile << "-56.14  52.46\n";
    plotfile << "-59.07  53.58\n";
    plotfile << "-58.26  54.21\n";
    plotfile << "-60.69  55.33\n";
    plotfile << "-61.97  57.41\n";
    plotfile << "-64.35  59.49\n";
    plotfile << "-67.29  58.15\n";
    plotfile << "-69.89  59.91\n";
    plotfile << "-71.31  61.45\n";
    plotfile << "-78.22  61.97\n";
    plotfile << "-77.28  59.53\n";
    plotfile << "-77.09  55.88\n";
    plotfile << "-79.06  51.68\n";
    plotfile << "-82.23  52.70\n";
    plotfile << "-86.75  55.72\n";
    plotfile << "-92.17  56.86\n";
    plotfile << "-95.61  58.82\n";
    plotfile << "-92.66  62.02\n";
    plotfile << "-90.65  63.24\n";
    plotfile << "-95.96  64.12\n";
    plotfile << "-89.88  63.98\n";
    plotfile << "-89.30  65.22\n";
    plotfile << "-86.86  66.12\n";
    plotfile << "-84.54  66.88\n";
    plotfile << "-82.30  67.76\n";
    plotfile << "-83.10  69.68\n";
    plotfile << "-86.05  67.98\n";
    plotfile << "-88.18  68.20\n";
    plotfile << "-91.00  68.82\n";
    plotfile << "-91.72  69.69\n";
    plotfile << "-93.15  71.09\n";
    plotfile << "-96.58  71.05\n";
    plotfile << "-93.35  69.52\n";
    plotfile << "-94.23  68.25\n";
    plotfile << "-95.96  66.73\n";
    plotfile << "-98.83  68.27\n";
    plotfile << "-102.45  67.69\n";
    plotfile << "-108.34  68.43\n";
    plotfile << "-105.83  68.05\n";
    plotfile << "-108.15  66.60\n";
    plotfile << "-111.15  67.63\n";
    plotfile << "-114.10  68.23\n";
    plotfile << "-120.92  69.44\n";
    plotfile << "-124.32  69.26\n";
    plotfile << "-128.76  70.50\n";
    plotfile << "-131.86  69.19\n";
    plotfile << "-131.15  69.79\n";
    plotfile << "-135.81  69.13\n";
    plotfile << "-140.19  69.37\n";
    plotfile << "-141.20  69.58 \n";
    plotfile << "-141.21  69.56 \n";
    plotfile << "-142.49  69.83\n";
    plotfile << "-148.09  70.26\n";
    plotfile << "-154.37  70.96\n";
    plotfile << "-159.53  70.38\n";
    plotfile << "-166.64  68.25\n";
    plotfile << "-161.56  66.55\n";
    plotfile << "-162.99  65.97\n";
    plotfile << "-168.23  65.49\n";
    plotfile << "-161.12  64.49\n";
    plotfile << "-165.29  62.57\n";
    plotfile << "-164.58  60.06\n";
    plotfile << "-162.06  58.36\n";
    plotfile << "-157.85  58.12\n";
    plotfile << "-162.34  55.06\n";
    plotfile << "-156.52  57.11\n";
    plotfile << "-153.53  59.32\n";
    plotfile << "-149.18  60.81\n";
    plotfile << "-149.90  59.50\n";
    plotfile << "-146.54  60.36\n";
    plotfile << "-139.98  59.73\n";
    plotfile << "-137.12  58.28\n";
    plotfile << "-136.01  59.12\n";
    plotfile << "-133.84  57.12\n";
    plotfile << "-131.46  55.98\n";
    plotfile << "-132.08  57.20\n";
    plotfile << "-140.37  60.25\n";
    plotfile << "-141.21  60.16\n";
    plotfile << "-133.38  58.93\n";
    plotfile << "-130.88  54.83\n";
    plotfile << "-128.86  53.90\n";
    plotfile << "-126.58  52.12\n";
    plotfile << "-127.08  50.80\n";
    plotfile << "-124.42  49.66\n";
    plotfile << "-122.56  48.91 \n";
    plotfile << "-122.44  48.92\n";
    plotfile << "-124.42  47.18\n";
    plotfile << "-124.52  42.48\n";
    plotfile << "-123.09  38.45\n";
    plotfile << "-121.73  36.62\n";
    plotfile << "-117.60  33.34\n";
    plotfile << "-117.28  32.64 \n";
    plotfile << "-117.29  32.48\n";
    plotfile << "-114.75  27.80\n";
    plotfile << "-112.53  24.80\n";
    plotfile << "-110.55  24.07\n";
    plotfile << "-114.23  29.59\n";
    plotfile << "-112.58  29.99\n";
    plotfile << "-109.57  25.94\n";
    plotfile << "-105.61  21.94\n";
    plotfile << "-102.09  17.87\n";
    plotfile << "-95.75  15.94\n";
    plotfile << "-92.21  14.97 \n";
    plotfile << "-92.22  14.71\n";
    plotfile << "-86.74  12.06\n";
    plotfile << "-83.03   8.65\n";
    plotfile << "-79.93   8.74\n";
    plotfile << "-77.00   7.82\n";
    plotfile << "-81.99   8.97\n";
    plotfile << "-83.92  12.70\n";
    plotfile << "-86.33  15.80\n";
    plotfile << "-88.40  15.92 \n";
    plotfile << "-88.45  17.42\n";
    plotfile << "-87.01  21.33\n";
    plotfile << "-91.65  18.72\n";
    plotfile << "-96.96  20.37\n";
    plotfile << "-97.65  25.67 \n";
    plotfile << "-97.62  25.82\n";
    plotfile << "-95.62  28.84\n";
    plotfile << "-90.77  29.03\n";
    plotfile << "-87.33  30.22\n";
    plotfile << "-82.69  28.15\n";
    plotfile << "-80.16  26.66\n";
    plotfile << "-80.74  32.31\n";
    plotfile << "-76.89  35.43\n";
    plotfile << "-76.47  38.21\n";
    plotfile << "-75.66  37.67\n";
    plotfile << "-71.31  41.76\n";
    plotfile << "-69.44  44.17\n";
    plotfile << "-67.69  47.03\n";
    plotfile << "-73.18  45.14\n";
    plotfile << "-79.26  43.28\n";
    plotfile << "-82.84  42.59\n";
    plotfile << "-83.49  45.32\n";
    plotfile << "-86.36  43.65\n";
    plotfile << "-87.75  43.42\n";
    plotfile << "-86.01  45.96\n";
    plotfile << "-87.00  46.59\n";
    plotfile << "-91.39  46.79\n";
    plotfile << "-90.05  47.96 \n";
    plotfile << "\n";
    plotfile << "-152.62  58.41\n";
    plotfile << "-152.60  58.40 \n";
    plotfile << "\n";
    plotfile << "-153.30  57.80\n";
    plotfile << "-152.40  57.48\n";
    plotfile << "-153.32  57.79 \n";
    plotfile << "\n";
    plotfile << "-166.96  53.96\n";
    plotfile << "-167.01  53.95 \n";
    plotfile << "\n";
    plotfile << "-168.36  53.50\n";
    plotfile << "-168.19  53.36 \n";
    plotfile << "\n";
    plotfile << "-170.73  52.68\n";
    plotfile << "-170.60  52.55 \n";
    plotfile << "\n";
    plotfile << "-174.47  51.94\n";
    plotfile << "-174.47  51.92 \n";
    plotfile << "\n";
    plotfile << "-176.58  51.71\n";
    plotfile << "-176.64  51.73 \n";
    plotfile << "\n";
    plotfile << "-177.55  51.76\n";
    plotfile << "-177.41  51.63 \n";
    plotfile << "\n";
    plotfile << "-178.27  51.75 \n";
    plotfile << "\n";
    plotfile << "177.35  51.80\n";
    plotfile << "177.33  51.76 \n";
    plotfile << "\n";
    plotfile << "172.44  53.00\n";
    plotfile << "172.55  53.03 \n";
    plotfile << "\n";
    plotfile << "-123.40  48.33\n";
    plotfile << "-128.00  50.84\n";
    plotfile << "-123.50  48.34 \n";
    plotfile << "\n";
    plotfile << "-132.49  52.88\n";
    plotfile << "-132.44  52.91 \n";
    plotfile << "\n";
    plotfile << "-132.64  53.02\n";
    plotfile << "-131.97  53.71\n";
    plotfile << "-132.63  53.02 \n";
    plotfile << "\n";
    plotfile << "-55.36  51.56\n";
    plotfile << "-54.66  49.52\n";
    plotfile << "-53.65  47.48\n";
    plotfile << "-52.98  46.31\n";
    plotfile << "-56.12  46.84\n";
    plotfile << "-58.47  47.57\n";
    plotfile << "-57.61  50.38\n";
    plotfile << "-55.39  51.53 \n";
    plotfile << "\n";
    plotfile << "-61.37  49.01\n";
    plotfile << "-61.80  49.29\n";
    plotfile << "-61.38  49.03 \n";
    plotfile << "\n";
    plotfile << "-63.01  46.71\n";
    plotfile << "-64.42  46.61\n";
    plotfile << "-63.04  46.68 \n";
    plotfile << "\n";
    plotfile << "-60.14  46.48\n";
    plotfile << "-60.14  46.50 \n";
    plotfile << "\n";
    plotfile << "-71.97  41.11\n";
    plotfile << "-71.97  41.15 \n";
    plotfile << "\n";
    plotfile << "-80.79  27.03\n";
    plotfile << "-81.01  26.99 \n";
    plotfile << "\n";
    plotfile << "-113.01  42.09\n";
    plotfile << "-113.10  42.01 \n";
    plotfile << "\n";
    plotfile << "-155.74  20.02\n";
    plotfile << "-155.73  19.98 \n";
    plotfile << "\n";
    plotfile << "-156.51  20.78\n";
    plotfile << "-156.51  20.78 \n";
    plotfile << "\n";
    plotfile << "-157.12  21.21\n";
    plotfile << "-157.08  20.95 \n";
    plotfile << "\n";
    plotfile << "-157.87  21.42 \n";
    plotfile << "\n";
    plotfile << "-159.53  22.07 \n";
    plotfile << "\n";
    plotfile << "-117.44  66.46\n";
    plotfile << "-119.59  65.24\n";
    plotfile << "-123.95  65.03\n";
    plotfile << "-123.69  66.44\n";
    plotfile << "-119.21  66.22\n";
    plotfile << "-117.44  66.44 \n";
    plotfile << "\n";
    plotfile << "-120.71  64.03\n";
    plotfile << "-114.91  62.30\n";
    plotfile << "-109.07  62.72\n";
    plotfile << "-112.62  61.19\n";
    plotfile << "-118.68  61.19\n";
    plotfile << "-117.01  61.17\n";
    plotfile << "-115.97  62.56\n";
    plotfile << "-119.46  64.00\n";
    plotfile << "-120.59  63.94 \n";
    plotfile << "\n";
    plotfile << "-112.31  58.46\n";
    plotfile << "-108.90  59.44\n";
    plotfile << "-104.14  58.90\n";
    plotfile << "-102.56  56.72\n";
    plotfile << "-101.82  58.73\n";
    plotfile << "-104.65  58.91\n";
    plotfile << "-111.00  58.51\n";
    plotfile << "-112.35  58.62 \n";
    plotfile << "\n";
    plotfile << "-98.74  50.09\n";
    plotfile << "-99.75  52.24\n";
    plotfile << "-99.62  51.47\n";
    plotfile << "-98.82  50.39 \n";
    plotfile << "\n";
    plotfile << "-97.02  50.21\n";
    plotfile << "-97.50  54.02\n";
    plotfile << "-98.69  52.93\n";
    plotfile << "-97.19  51.09\n";
    plotfile << "-96.98  50.20 \n";
    plotfile << "\n";
    plotfile << "-95.34  49.04\n";
    plotfile << "-92.32  50.34\n";
    plotfile << "-94.14  49.47\n";
    plotfile << "-95.36  48.82 \n";
    plotfile << "\n";
    plotfile << "-80.39  56.16\n";
    plotfile << "-79.22  55.94\n";
    plotfile << "-80.34  56.08 \n";
    plotfile << "\n";
    plotfile << "-103.56  58.60\n";
    plotfile << "-103.60  58.58 \n";
    plotfile << "\n";
    plotfile << "-101.82  58.03\n";
    plotfile << "-102.33  58.10\n";
    plotfile << "-101.77  58.06 \n";
    plotfile << "\n";
    plotfile << "-101.88  55.79\n";
    plotfile << "-97.92  57.15\n";
    plotfile << "-101.22  55.85\n";
    plotfile << "-101.88  55.74 \n";
    plotfile << "\n";
    plotfile << "-77.61   6.80\n";
    plotfile << "-78.70   0.97\n";
    plotfile << "-80.75  -4.47\n";
    plotfile << "-76.19 -14.57\n";
    plotfile << "-70.44 -18.75\n";
    plotfile << "-70.68 -26.15\n";
    plotfile << "-71.44 -32.03\n";
    plotfile << "-73.38 -37.27\n";
    plotfile << "-73.06 -42.11\n";
    plotfile << "-73.17 -46.09\n";
    plotfile << "-73.52 -48.05\n";
    plotfile << "-73.67 -51.56\n";
    plotfile << "-71.06 -53.88\n";
    plotfile << "-69.14 -50.77\n";
    plotfile << "-67.51 -46.59\n";
    plotfile << "-63.49 -42.80\n";
    plotfile << "-62.14 -40.16\n";
    plotfile << "-57.12 -36.71\n";
    plotfile << "-53.17 -34.15\n";
    plotfile << "-51.26 -32.02\n";
    plotfile << "-48.16 -25.48\n";
    plotfile << "-40.73 -22.32\n";
    plotfile << "-38.88 -15.24\n";
    plotfile << "-34.60  -7.81\n";
    plotfile << "-41.95  -3.42\n";
    plotfile << "-48.02  -1.84\n";
    plotfile << "-48.44  -1.57\n";
    plotfile << "-50.81   0.00\n";
    plotfile << "-54.47   5.39\n";
    plotfile << "-60.59   8.32\n";
    plotfile << "-64.19   9.88\n";
    plotfile << "-70.78  10.64\n";
    plotfile << "-70.97  11.89\n";
    plotfile << "-76.26   8.76\n";
    plotfile << "-77.61   6.80 \n";
    plotfile << "\n";
    plotfile << "-69.14 -52.79\n";
    plotfile << "-66.16 -55.08\n";
    plotfile << "-70.01 -54.88\n";
    plotfile << "-70.55 -53.85\n";
    plotfile << "-69.31 -52.81 \n";
    plotfile << "\n";
    plotfile << "-59.29 -51.58\n";
    plotfile << "-59.35 -51.54 \n";
    plotfile << "\n";
    plotfile << "-58.65 -51.55\n";
    plotfile << "-58.55 -51.56 \n";
    plotfile << "\n";
    plotfile << "-84.39  21.44\n";
    plotfile << "-73.90  19.73\n";
    plotfile << "-79.27  21.18\n";
    plotfile << "-83.74  21.80\n";
    plotfile << "-84.32  21.42 \n";
    plotfile << "\n";
    plotfile << "-66.96  17.95\n";
    plotfile << "-67.05  17.89 \n";
    plotfile << "\n";
    plotfile << "-77.88  17.22\n";
    plotfile << "-78.06  16.98 \n";
    plotfile << "\n";
    plotfile << "-74.47  18.08\n";
    plotfile << "-69.88  18.99\n";
    plotfile << "-71.10  17.76\n";
    plotfile << "-74.45  17.86 \n";
    plotfile << "\n";
    plotfile << "-85.28  73.74\n";
    plotfile << "-85.79  70.96\n";
    plotfile << "-85.13  71.94\n";
    plotfile << "-84.74  72.96\n";
    plotfile << "-80.61  73.10\n";
    plotfile << "-78.45  72.20\n";
    plotfile << "-75.44  72.55\n";
    plotfile << "-73.89  71.98\n";
    plotfile << "-72.56  71.04\n";
    plotfile << "-71.49  70.57\n";
    plotfile << "-69.78  70.29\n";
    plotfile << "-68.12  69.71\n";
    plotfile << "-65.91  69.19\n";
    plotfile << "-66.92  68.39\n";
    plotfile << "-64.08  67.68\n";
    plotfile << "-62.50  66.68\n";
    plotfile << "-63.07  65.33\n";
    plotfile << "-66.11  66.08\n";
    plotfile << "-67.48  65.41\n";
    plotfile << "-64.05  63.15\n";
    plotfile << "-66.58  63.26\n";
    plotfile << "-69.04  62.33\n";
    plotfile << "-72.22  63.77\n";
    plotfile << "-76.88  64.17\n";
    plotfile << "-73.25  65.54\n";
    plotfile << "-70.09  66.64\n";
    plotfile << "-72.05  67.44\n";
    plotfile << "-76.32  68.36\n";
    plotfile << "-78.34  70.17\n";
    plotfile << "-82.12  69.71\n";
    plotfile << "-87.64  70.12\n";
    plotfile << "-89.68  71.43\n";
    plotfile << "-85.28  73.74 \n";
    plotfile << "\n";
    plotfile << "-80.90  76.10\n";
    plotfile << "-84.21  76.28\n";
    plotfile << "-88.94  76.38\n";
    plotfile << "-85.47  77.40\n";
    plotfile << "-85.43  77.93\n";
    plotfile << "-87.01  78.54\n";
    plotfile << "-83.17  78.94\n";
    plotfile << "-84.87  79.93\n";
    plotfile << "-81.33  79.82\n";
    plotfile << "-76.27  80.92\n";
    plotfile << "-82.88  80.62\n";
    plotfile << "-82.58  81.16\n";
    plotfile << "-86.51  81.05\n";
    plotfile << "-89.36  81.21\n";
    plotfile << "-90.45  81.38\n";
    plotfile << "-89.28  81.86\n";
    plotfile << "-87.21  82.30\n";
    plotfile << "-80.51  82.05\n";
    plotfile << "-80.16  82.55\n";
    plotfile << "-77.83  82.86\n";
    plotfile << "-75.51  83.05\n";
    plotfile << "-71.18  82.90\n";
    plotfile << "-65.10  82.78\n";
    plotfile << "-63.34  81.80\n";
    plotfile << "-68.26  81.26\n";
    plotfile << "-69.46  80.34\n";
    plotfile << "-71.05  79.82\n";
    plotfile << "-74.40  79.46\n";
    plotfile << "-75.42  79.03\n";
    plotfile << "-75.48  78.92\n";
    plotfile << "-76.01  78.20\n";
    plotfile << "-80.66  77.28\n";
    plotfile << "-78.07  76.98\n";
    plotfile << "-80.90  76.13 \n";
    plotfile << "\n";
    plotfile << "-92.86  74.13\n";
    plotfile << "-92.50  72.70\n";
    plotfile << "-94.89  73.16\n";
    plotfile << "-92.96  74.14 \n";
    plotfile << "\n";
    plotfile << "-94.80  76.95\n";
    plotfile << "-89.68  76.04\n";
    plotfile << "-88.52  75.40\n";
    plotfile << "-82.36  75.67\n";
    plotfile << "-79.39  74.65\n";
    plotfile << "-86.15  74.22\n";
    plotfile << "-91.70  74.94\n";
    plotfile << "-95.60  76.91\n";
    plotfile << "-94.87  76.96 \n";
    plotfile << "\n";
    plotfile << "-99.96  73.74\n";
    plotfile << "-97.89  72.90\n";
    plotfile << "-98.28  71.13\n";
    plotfile << "-102.04  72.92\n";
    plotfile << "-101.34  73.14\n";
    plotfile << "-99.69  73.59 \n";
    plotfile << "\n";
    plotfile << "-107.58  73.25\n";
    plotfile << "-104.59  71.02\n";
    plotfile << "-101.71  69.56\n";
    plotfile << "-104.07  68.62\n";
    plotfile << "-106.61  69.12\n";
    plotfile << "-114.09  69.05\n";
    plotfile << "-113.89  70.12\n";
    plotfile << "-115.88  70.32\n";
    plotfile << "-116.10  71.32\n";
    plotfile << "-117.45  72.48\n";
    plotfile << "-113.53  72.44\n";
    plotfile << "-109.84  72.24\n";
    plotfile << "-106.62  71.71\n";
    plotfile << "-107.43  73.04 \n";
    plotfile << "\n";
    plotfile << "-120.96  74.29\n";
    plotfile << "-118.37  72.53\n";
    plotfile << "-123.06  71.18\n";
    plotfile << "-123.40  73.77\n";
    plotfile << "-120.93  74.27 \n";
    plotfile << "\n";
    plotfile << "-108.83  76.74\n";
    plotfile << "-106.25  75.54\n";
    plotfile << "-107.08  74.78\n";
    plotfile << "-112.99  74.16\n";
    plotfile << "-112.28  74.99\n";
    plotfile << "-116.04  75.33\n";
    plotfile << "-115.27  76.20\n";
    plotfile << "-110.95  75.56\n";
    plotfile << "-109.77  76.31\n";
    plotfile << "-108.82  76.70 \n";
    plotfile << "\n";
    plotfile << "-115.70  77.46\n";
    plotfile << "-118.10  76.30\n";
    plotfile << "-121.13  76.37\n";
    plotfile << "-116.04  77.28 \n";
    plotfile << "\n";
    plotfile << "-110.01  77.86\n";
    plotfile << "-112.36  77.68\n";
    plotfile << "-109.96  77.86 \n";
    plotfile << "\n";
    plotfile << "-109.60  78.48\n";
    plotfile << "-112.20  78.01\n";
    plotfile << "-109.60  78.48 \n";
    plotfile << "\n";
    plotfile << "-97.87  76.61\n";
    plotfile << "-99.21  75.31\n";
    plotfile << "-100.86  75.60\n";
    plotfile << "-99.40  76.26\n";
    plotfile << "-97.79  76.60 \n";
    plotfile << "\n";
    plotfile << "-94.72  75.53\n";
    plotfile << "-94.66  75.52 \n";
    plotfile << "\n";
    plotfile << "-104.10  79.01\n";
    plotfile << "-99.19  77.54\n";
    plotfile << "-103.22  78.08\n";
    plotfile << "-104.30  78.95 \n";
    plotfile << "\n";
    plotfile << "-93.74  77.52\n";
    plotfile << "-93.74  77.52 \n";
    plotfile << "\n";
    plotfile << "-96.88  78.50\n";
    plotfile << "-96.91  77.77\n";
    plotfile << "-96.94  78.48 \n";
    plotfile << "\n";
    plotfile << "-84.69  65.84\n";
    plotfile << "-81.58  63.87\n";
    plotfile << "-85.00  62.96\n";
    plotfile << "-84.63  65.71 \n";
    plotfile << "\n";
    plotfile << "-81.84  62.75\n";
    plotfile << "-82.01  62.63 \n";
    plotfile << "\n";
    plotfile << "-79.88  62.12\n";
    plotfile << "-79.88  62.12 \n";
    plotfile << "\n";
    plotfile << "-43.53  59.89\n";
    plotfile << "-45.29  60.67\n";
    plotfile << "-47.91  60.83\n";
    plotfile << "-49.90  62.41\n";
    plotfile << "-50.71  64.42\n";
    plotfile << "-51.39  64.94\n";
    plotfile << "-52.96  66.09\n";
    plotfile << "-53.62  67.19\n";
    plotfile << "-53.51  67.51\n";
    plotfile << "-51.84  68.65\n";
    plotfile << "-52.19  70.00\n";
    plotfile << "-51.85  71.03\n";
    plotfile << "-55.41  71.41\n";
    plotfile << "-54.63  72.97\n";
    plotfile << "-56.98  74.70\n";
    plotfile << "-61.95  76.09\n";
    plotfile << "-66.38  75.83\n";
    plotfile << "-71.13  77.00\n";
    plotfile << "-66.81  77.60\n";
    plotfile << "-70.78  77.78\n";
    plotfile << "-64.96  79.70\n";
    plotfile << "-63.38  81.16\n";
    plotfile << "-56.89  82.17\n";
    plotfile << "-48.18  82.15\n";
    plotfile << "-42.08  82.74\n";
    plotfile << "-38.02  83.54\n";
    plotfile << "-23.96  82.94\n";
    plotfile << "-25.97  81.97\n";
    plotfile << "-25.99  80.64\n";
    plotfile << "-13.57  80.97\n";
    plotfile << "-16.60  80.16\n";
    plotfile << "-19.82  78.82\n";
    plotfile << "-18.80  77.54\n";
    plotfile << "-21.98  76.46\n";
    plotfile << "-20.69  75.12\n";
    plotfile << "-21.78  74.40\n";
    plotfile << "-24.10  73.69\n";
    plotfile << "-26.54  73.08\n";
    plotfile << "-24.63  72.69\n";
    plotfile << "-21.84  71.69\n";
    plotfile << "-24.62  71.24\n";
    plotfile << "-27.16  70.89\n";
    plotfile << "-27.21  70.00\n";
    plotfile << "-24.10  69.35\n";
    plotfile << "-28.35  68.43\n";
    plotfile << "-32.48  68.56\n";
    plotfile << "-35.26  66.26\n";
    plotfile << "-37.90  65.90\n";
    plotfile << "-40.04  65.00\n";
    plotfile << "-40.49  64.04\n";
    plotfile << "-42.01  63.14\n";
    plotfile << "-42.88  61.15\n";
    plotfile << "-43.09  60.07\n";
    plotfile << "-43.56  59.90 \n";
    plotfile << "\n";
    plotfile << "-16.26  66.41\n";
    plotfile << "-15.32  64.29\n";
    plotfile << "-20.14  63.47\n";
    plotfile << "-21.76  64.21\n";
    plotfile << "-21.33  64.97\n";
    plotfile << "-23.04  65.62\n";
    plotfile << "-21.76  66.26\n";
    plotfile << "-18.77  66.12\n";
    plotfile << "-16.23  66.35 \n";
    plotfile << "\n";
    plotfile << "  0.56  51.47\n";
    plotfile << " -1.71  54.94\n";
    plotfile << " -3.41  57.52\n";
    plotfile << " -5.42  58.14\n";
    plotfile << " -5.77  55.59\n";
    plotfile << " -3.48  54.82\n";
    plotfile << " -4.68  52.88\n";
    plotfile << " -2.68  51.58\n";
    plotfile << " -3.80  50.08\n";
    plotfile << "  1.26  51.14\n";
    plotfile << "  0.65  51.41 \n";
    plotfile << "\n";
    plotfile << " -7.17  54.91\n";
    plotfile << " -9.97  53.47\n";
    plotfile << " -8.52  51.76\n";
    plotfile << " -5.69  54.79\n";
    plotfile << " -7.34  55.25 \n";
    plotfile << "\n";
    plotfile << " -1.33  60.66\n";
    plotfile << " -1.17  60.38 \n";
    plotfile << "\n";
    plotfile << " -6.18  58.44\n";
    plotfile << " -6.09  58.36 \n";
    plotfile << "\n";
    plotfile << " -6.47  57.58\n";
    plotfile << " -6.33  57.54 \n";
    plotfile << "\n";
    plotfile << " -7.30  57.54 \n";
    plotfile << "\n";
    plotfile << " -7.46  57.05 \n";
    plotfile << "\n";
    plotfile << " -6.54  56.94 \n";
    plotfile << "\n";
    plotfile << " -6.00  55.94 \n";
    plotfile << "\n";
    plotfile << " -5.09  55.55 \n";
    plotfile << "\n";
    plotfile << " -4.44  54.38\n";
    plotfile << " -4.30  54.19 \n";
    plotfile << "\n";
    plotfile << " -8.08  71.02\n";
    plotfile << " -8.21  70.86 \n";
    plotfile << "\n";
    plotfile << " 16.92  79.52\n";
    plotfile << " 22.26  78.46\n";
    plotfile << " 16.86  76.41\n";
    plotfile << " 16.00  77.39\n";
    plotfile << " 16.03  77.92\n";
    plotfile << " 16.81  79.50 \n";
    plotfile << "\n";
    plotfile << " 14.71  79.40\n";
    plotfile << " 16.05  79.12\n";
    plotfile << " 14.02  77.80\n";
    plotfile << " 13.56  78.46\n";
    plotfile << " 12.63  79.26\n";
    plotfile << " 14.68  79.40 \n";
    plotfile << "\n";
    plotfile << " 22.01  78.24\n";
    plotfile << " 21.86  78.23 \n";
    plotfile << "\n";
    plotfile << " 21.54  77.75\n";
    plotfile << " 23.88  77.26\n";
    plotfile << " 21.53  77.67\n";
    plotfile << " 22.79  77.79 \n";
    plotfile << "\n";
    plotfile << " 23.50  79.97\n";
    plotfile << " 28.24  79.54\n";
    plotfile << " 20.85  78.94\n";
    plotfile << " 19.00  79.34\n";
    plotfile << " 21.05  79.88\n";
    plotfile << " 23.41  79.96 \n";
    plotfile << "\n";
    plotfile << " 46.98  80.23\n";
    plotfile << " 43.13  79.97\n";
    plotfile << " 47.18  80.22\n";
    plotfile << "\n";
    plotfile << " 50.43  80.19\n";
    plotfile << " 50.55  79.88\n";
    plotfile << " 47.77  79.86\n";
    plotfile << " 50.45  80.14\n";
    plotfile << "\n";
    plotfile << " 61.79  80.18\n";
    plotfile << " 61.79  80.18\n";
    plotfile << "\n";
    plotfile << " 65.08  80.69\n";
    plotfile << " 64.27  80.59\n";
    plotfile << " 65.13  80.68\n";
    plotfile << "\n";
    plotfile << " -5.13  35.66\n";
    plotfile << "  4.06  36.63\n";
    plotfile << " 10.40  37.12\n";
    plotfile << " 11.36  33.61\n";
    plotfile << " 20.10  30.10\n";
    plotfile << " 23.49  32.17\n";
    plotfile << " 31.65  30.80\n";
    plotfile << " 35.76  23.74\n";
    plotfile << " 39.75  14.82\n";
    plotfile << " 42.93  11.34\n";
    plotfile << " 51.52  11.45\n";
    plotfile << " 49.82   6.99\n";
    plotfile << " 43.13  -0.62\n";
    plotfile << " 39.15  -7.58\n";
    plotfile << " 40.37 -13.20\n";
    plotfile << " 37.74 -18.17\n";
    plotfile << " 35.33 -22.71\n";
    plotfile << " 32.84 -28.15\n";
    plotfile << " 26.50 -34.39\n";
    plotfile << " 19.55 -35.51\n";
    plotfile << " 17.50 -30.88\n";
    plotfile << " 12.24 -18.75\n";
    plotfile << " 13.89 -12.81\n";
    plotfile << " 12.05  -5.55\n";
    plotfile << "  9.67   0.14\n";
    plotfile << "  7.19   3.79\n";
    plotfile << "  1.74   5.39\n";
    plotfile << " -4.77   4.59\n";
    plotfile << "-12.00   6.75\n";
    plotfile << "-15.54  10.98\n";
    plotfile << "-16.33  15.50\n";
    plotfile << "-16.10  22.29\n";
    plotfile << "-12.90  27.12\n";
    plotfile << " -9.52  31.09\n";
    plotfile << " -5.41  35.58\n";
    plotfile << "\n";
    plotfile << " 33.71   0.00\n";
    plotfile << " 33.48  -3.42\n";
    plotfile << " 33.34  -0.20\n";
    plotfile << " 33.71   0.00\n";
    plotfile << "\n";
    plotfile << " 49.30 -12.50\n";
    plotfile << " 49.28 -18.79\n";
    plotfile << " 43.95 -25.50\n";
    plotfile << " 44.37 -20.08\n";
    plotfile << " 46.34 -16.31\n";
    plotfile << " 47.91 -14.08\n";
    plotfile << " 49.30 -12.50\n";
    plotfile << "\n";
    plotfile << "178.88  69.10\n";
    plotfile << "181.20  68.42\n";
    plotfile << "183.52  67.78\n";
    plotfile << "188.87  66.38\n";
    plotfile << "186.54  64.74\n";
    plotfile << "182.87  65.63\n";
    plotfile << "180.13  65.14\n";
    plotfile << "179.48  64.88\n";
    plotfile << "178.20  64.29\n";
    plotfile << "177.46  62.62\n";
    plotfile << "170.42  60.17\n";
    plotfile << "164.48  59.89\n";
    plotfile << "162.92  57.34\n";
    plotfile << "161.82  54.88\n";
    plotfile << "156.42  51.09\n";
    plotfile << "156.40  57.76\n";
    plotfile << "163.79  61.73\n";
    plotfile << "159.90  60.73\n";
    plotfile << "156.81  61.68\n";
    plotfile << "153.83  59.10\n";
    plotfile << "148.57  59.46\n";
    plotfile << "140.77  58.39\n";
    plotfile << "137.10  54.07\n";
    plotfile << "140.72  52.43\n";
    plotfile << "138.77  47.30\n";
    plotfile << "129.92  42.04\n";
    plotfile << "128.33  38.46\n";
    plotfile << "126.15  35.18\n";
    plotfile << "125.12  39.08\n";
    plotfile << "121.62  40.15\n";
    plotfile << "117.58  38.21\n";
    plotfile << "121.77  36.90\n";
    plotfile << "120.73  32.65\n";
    plotfile << "121.28  30.25\n";
    plotfile << "118.83  24.93\n";
    plotfile << "112.69  21.81\n";
    plotfile << "108.53  21.73\n";
    plotfile << "107.55  16.34\n";
    plotfile << "107.32  10.45\n";
    plotfile << "104.39  10.37\n";
    plotfile << "100.01  13.52\n";
    plotfile << "100.26   8.30\n";
    plotfile << "103.22   1.56\n";
    plotfile << " 98.21   9.17\n";
    plotfile << " 97.66  15.36\n";
    plotfile << " 94.21  17.79\n";
    plotfile << " 90.05  21.74\n";
    plotfile << " 90.06  21.03\n";
    plotfile << " 82.06  15.95\n";
    plotfile << " 80.05  11.72\n";
    plotfile << " 76.41   8.60\n";
    plotfile << " 72.79  17.43\n";
    plotfile << " 72.02  20.00\n";
    plotfile << " 68.98  21.99\n";
    plotfile << " 64.62  24.41\n";
    plotfile << " 57.83  24.77\n";
    plotfile << " 53.11  26.20\n";
    plotfile << " 49.67  29.41\n";
    plotfile << " 50.96  25.15\n";
    plotfile << " 54.33  23.44\n";
    plotfile << " 59.03  22.57\n";
    plotfile << " 57.87  18.86\n";
    plotfile << " 52.95  15.74\n";
    plotfile << " 47.26  12.96\n";
    plotfile << " 42.75  14.68\n";
    plotfile << " 39.93  19.61\n";
    plotfile << " 36.92  25.78\n";
    plotfile << " 33.30  28.46\n";
    plotfile << " 32.60  30.63\n";
    plotfile << " 32.18  30.58\n";
    plotfile << " 36.08  35.03\n";
    plotfile << " 32.53  36.17\n";
    plotfile << " 27.77  36.94\n";
    plotfile << " 26.51  39.18\n";
    plotfile << " 31.54  40.82\n";
    plotfile << " 38.53  40.48\n";
    plotfile << " 40.35  43.17\n";
    plotfile << " 39.88  46.45\n";
    plotfile << " 35.18  44.99\n";
    plotfile << " 33.50  44.96\n";
    plotfile << " 30.24  45.14\n";
    plotfile << " 28.70  41.48\n";
    plotfile << " 26.55  39.84\n";
    plotfile << " 23.62  39.67\n";
    plotfile << " 23.80  37.34\n";
    plotfile << " 21.90  36.92\n";
    plotfile << " 18.79  42.02\n";
    plotfile << " 14.52  44.31\n";
    plotfile << " 14.58  42.25\n";
    plotfile << " 18.32  39.57\n";
    plotfile << " 16.05  39.35\n";
    plotfile << " 11.52  42.36\n";
    plotfile << "  6.87  43.08\n";
    plotfile << "  2.80  41.09\n";
    plotfile << " -1.11  37.14\n";
    plotfile << " -6.24  36.70\n";
    plotfile << " -8.67  39.57\n";
    plotfile << " -6.51  43.13\n";
    plotfile << " -0.84  45.55\n";
    plotfile << " -3.93  48.40\n";
    plotfile << "  0.48  49.09\n";
    plotfile << "  4.20  51.29\n";
    plotfile << "  6.44  52.92\n";
    plotfile << "  8.42  55.94\n";
    plotfile << " 11.72  55.49\n";
    plotfile << " 11.73  53.66\n";
    plotfile << " 16.78  54.14\n";
    plotfile << " 21.40  56.32\n";
    plotfile << " 24.67  57.20\n";
    plotfile << " 28.94  59.18\n";
    plotfile << " 24.16  59.52\n";
    plotfile << " 22.07  62.66\n";
    plotfile << " 23.76  65.35\n";
    plotfile << " 18.70  62.54\n";
    plotfile << " 19.11  59.67\n";
    plotfile << " 18.40  58.54\n";
    plotfile << " 15.34  55.73\n";
    plotfile << " 11.74  58.08\n";
    plotfile << "  8.37  57.68\n";
    plotfile << "  5.80  59.20\n";
    plotfile << "  7.38  60.86\n";
    plotfile << "  7.51  61.86\n";
    plotfile << "  9.62  62.99\n";
    plotfile << " 13.37  65.46\n";
    plotfile << " 15.46  67.12\n";
    plotfile << " 18.54  68.62\n";
    plotfile << " 22.32  69.64\n";
    plotfile << " 24.77  70.17\n";
    plotfile << " 25.93  69.79\n";
    plotfile << " 28.56  70.46\n";
    plotfile << " 29.75  69.76\n";
    plotfile << " 33.83  69.11\n";
    plotfile << " 41.90  66.85\n";
    plotfile << " 35.14  66.25\n";
    plotfile << " 33.30  66.07\n";
    plotfile << " 35.46  64.15\n";
    plotfile << " 37.68  64.03\n";
    plotfile << " 41.71  64.09\n";
    plotfile << " 44.80  65.58\n";
    plotfile << " 44.87  68.16\n";
    plotfile << " 45.92  66.83\n";
    plotfile << " 51.79  67.85\n";
    plotfile << " 53.70  67.89\n";
    plotfile << " 59.68  68.09\n";
    plotfile << " 65.07  69.08\n";
    plotfile << " 68.56  69.19\n";
    plotfile << " 68.38  70.97\n";
    plotfile << " 73.03  71.62\n";
    plotfile << " 73.80  68.29\n";
    plotfile << " 69.42  66.45\n";
    plotfile << " 73.43  66.36\n";
    plotfile << " 77.51  68.36\n";
    plotfile << " 80.74  66.74\n";
    plotfile << " 75.27  68.67\n";
    plotfile << " 75.11  71.80\n";
    plotfile << " 78.62  70.56\n";
    plotfile << " 78.43  71.90\n";
    plotfile << " 82.72  71.23\n";
    plotfile << " 84.25  70.03\n";
    plotfile << " 81.40  72.76\n";
    plotfile << " 86.50  74.01\n";
    plotfile << " 87.68  74.78\n";
    plotfile << " 90.25  75.23\n";
    plotfile << " 89.68  75.57\n";
    plotfile << " 95.12  75.95\n";
    plotfile << " 99.69  76.09\n";
    plotfile << "104.10  77.52\n";
    plotfile << "106.34  76.40\n";
    plotfile << "112.99  75.60\n";
    plotfile << "107.88  73.72\n";
    plotfile << "110.43  73.71\n";
    plotfile << "113.34  73.37\n";
    plotfile << "123.10  73.28\n";
    plotfile << "128.94  73.02\n";
    plotfile << "126.10  72.24\n";
    plotfile << "130.53  70.86\n";
    plotfile << "135.49  71.51\n";
    plotfile << "139.60  72.23\n";
    plotfile << "146.04  72.39\n";
    plotfile << "146.92  72.21\n";
    plotfile << "150.77  71.28\n";
    plotfile << "159.92  70.14\n";
    plotfile << "167.68  69.63\n";
    plotfile << "170.20  69.99\n";
    plotfile << "178.88  69.10\n";
    plotfile << "\n";
    plotfile << " 68.33  76.71\n";
    plotfile << " 66.03  75.62\n";
    plotfile << " 59.10  74.11\n";
    plotfile << " 54.92  73.03\n";
    plotfile << " 56.67  74.10\n";
    plotfile << " 58.56  75.09\n";
    plotfile << " 63.86  75.87\n";
    plotfile << " 68.19  76.70\n";
    plotfile << "\n";
    plotfile << " 53.04  72.57\n";
    plotfile << " 58.29  70.39\n";
    plotfile << " 55.03  70.78\n";
    plotfile << " 53.44  72.26\n";
    plotfile << " 53.63  72.61\n";
    plotfile << "\n";
    plotfile << " 52.22  46.50\n";
    plotfile << " 51.73  44.73\n";
    plotfile << " 52.56  41.80\n";
    plotfile << " 53.43  40.40\n";
    plotfile << " 54.22  37.86\n";
    plotfile << " 49.04  38.45\n";
    plotfile << " 48.17  42.76\n";
    plotfile << " 49.33  45.64\n";
    plotfile << " 52.22  46.50\n";
    plotfile << "\n";
    plotfile << " 62.32  46.32\n";
    plotfile << " 60.32  43.06\n";
    plotfile << " 59.57  45.58\n";
    plotfile << " 61.94  46.33\n";
    plotfile << "\n";
    plotfile << " 79.55  46.12\n";
    plotfile << " 74.30  44.44\n";
    plotfile << " 78.62  45.79\n";
    plotfile << " 79.66  46.07\n";
    plotfile << "\n";
    plotfile << " 76.81  41.96\n";
    plotfile << " 76.73  41.86\n";
    plotfile << "\n";
    plotfile << " 35.15  35.15\n";
    plotfile << " 34.61  34.84\n";
    plotfile << " 35.18  35.17\n";
    plotfile << "\n";
    plotfile << " 23.84  35.33\n";
    plotfile << " 24.30  34.91\n";
    plotfile << " 24.09  35.39\n";
    plotfile << "\n";
    plotfile << " 15.54  37.89\n";
    plotfile << " 13.47  37.89\n";
    plotfile << " 15.54  37.89\n";
    plotfile << "\n";
    plotfile << "  9.56  40.95\n";
    plotfile << "  8.46  39.99\n";
    plotfile << "  9.12  40.69\n";
    plotfile << "\n";
    plotfile << "  9.72  42.60\n";
    plotfile << "  9.54  42.35\n";
    plotfile << "\n";
    plotfile << " 80.60   8.95\n";
    plotfile << " 79.73   5.96\n";
    plotfile << " 80.10   8.30\n";
    plotfile << "\n";
    plotfile << " 11.04  57.44\n";
    plotfile << " 10.67  57.25\n";
    plotfile << "\n";
    plotfile << "-77.92  24.67\n";
    plotfile << "-77.98  24.22\n";
    plotfile << "\n";
    plotfile << "-77.61  23.62\n";
    plotfile << "-77.18  23.64\n";
    plotfile << "\n";
    plotfile << "-75.55  24.13\n";
    plotfile << "-75.41  24.31\n";
    plotfile << "\n";
    plotfile << "-91.40  -0.17\n";
    plotfile << "-91.52  -0.26\n";
    plotfile << "\n";
    plotfile << "-60.25  46.68\n";
    plotfile << "-60.71  46.33\n";
    plotfile << "\n";
    plotfile << "-63.89  49.47\n";
    plotfile << "-63.45  49.43\n";
    plotfile << "\n";
    plotfile << "142.53 -10.60\n";
    plotfile << "145.62 -16.34\n";
    plotfile << "149.79 -22.09\n";
    plotfile << "153.21 -26.82\n";
    plotfile << "150.52 -35.19\n";
    plotfile << "145.60 -38.53\n";
    plotfile << "140.13 -37.69\n";
    plotfile << "137.34 -34.77\n";
    plotfile << "135.76 -34.56\n";
    plotfile << "131.50 -31.34\n";
    plotfile << "121.72 -33.65\n";
    plotfile << "115.62 -33.25\n";
    plotfile << "114.09 -26.01\n";
    plotfile << "114.88 -21.27\n";
    plotfile << "122.34 -18.13\n";
    plotfile << "125.32 -14.53\n";
    plotfile << "128.39 -14.90\n";
    plotfile << "132.35 -11.42\n";
    plotfile << "136.16 -12.43\n";
    plotfile << "138.07 -16.45\n";
    plotfile << "142.25 -10.78\n";
    plotfile << "\n";
    plotfile << "144.72 -40.68\n";
    plotfile << "148.32 -42.14\n";
    plotfile << "145.57 -42.77\n";
    plotfile << "146.47 -41.19\n";
    plotfile << "\n";
    plotfile << "172.86 -34.23\n";
    plotfile << "176.10 -37.52\n";
    plotfile << "177.06 -39.49\n";
    plotfile << "174.77 -38.03\n";
    plotfile << "172.83 -34.27\n";
    plotfile << "\n";
    plotfile << "172.36 -40.53\n";
    plotfile << "172.92 -43.81\n";
    plotfile << "168.41 -46.13\n";
    plotfile << "170.26 -43.21\n";
    plotfile << "173.69 -40.94\n";
    plotfile << "\n";
    plotfile << "150.74 -10.18\n";
    plotfile << "143.04  -8.26\n";
    plotfile << "138.48  -6.97\n";
    plotfile << "131.95  -2.94\n";
    plotfile << "130.91  -1.35\n";
    plotfile << "134.38  -2.64\n";
    plotfile << "141.24  -2.62\n";
    plotfile << "148.19  -8.15\n";
    plotfile << "150.75 -10.27\n";
    plotfile << "\n";
    plotfile << "117.24   7.01\n";
    plotfile << "117.90   0.76\n";
    plotfile << "113.89  -3.50\n";
    plotfile << "109.44  -0.82\n";
    plotfile << "113.13   3.38\n";
    plotfile << "117.24   7.01\n";
    plotfile << "\n";
    plotfile << " 95.31   5.75\n";
    plotfile << "102.32   1.40\n";
    plotfile << "106.03  -2.98\n";
    plotfile << "101.46  -2.81\n";
    plotfile << " 95.20   5.73\n";
    plotfile << "\n";
    plotfile << "140.91  41.53\n";
    plotfile << "140.79  35.75\n";
    plotfile << "136.82  34.56\n";
    plotfile << "133.56  34.72\n";
    plotfile << "132.49  35.41\n";
    plotfile << "136.73  37.20\n";
    plotfile << "139.82  40.00\n";
    plotfile << "140.68  41.43\n";
    plotfile << "\n";
    plotfile << "133.71  34.30\n";
    plotfile << "131.41  31.58\n";
    plotfile << "129.38  33.10\n";
    plotfile << "133.90  34.37\n";
    plotfile << "\n";
    plotfile << "141.89  45.50\n";
    plotfile << "144.12  42.92\n";
    plotfile << "140.30  41.64\n";
    plotfile << "141.53  45.30\n";
    plotfile << "141.89  45.53\n";
    plotfile << "\n";
    plotfile << "142.57  54.36\n";
    plotfile << "143.64  49.19\n";
    plotfile << "141.99  45.88\n";
    plotfile << "141.92  50.85\n";
    plotfile << "142.60  54.34\n";
    plotfile << "\n";
    plotfile << "121.92  25.48\n";
    plotfile << "120.53  24.70\n";
    plotfile << "121.70  25.51\n";
    plotfile << "\n";
    plotfile << "110.81  20.07\n";
    plotfile << "109.20  19.66\n";
    plotfile << "110.81  20.07\n";
    plotfile << "\n";
    plotfile << "106.51  -6.16\n";
    plotfile << "114.15  -7.72\n";
    plotfile << "108.71  -7.89\n";
    plotfile << "106.51  -6.16\n";
    plotfile << "\n";
    plotfile << "164.27 -20.01\n";
    plotfile << "164.16 -20.27\n";
    plotfile << "\n";
    plotfile << "178.61 -17.04\n";
    plotfile << "178.61 -17.04\n";
    plotfile << "\n";
    plotfile << "179.45 -16.43\n";
    plotfile << "179.35 -16.43\n";
    plotfile << "\n";
    plotfile << "-172.55 -13.39\n";
    plotfile << "-172.61 -13.78\n";
    plotfile << "\n";
    plotfile << "122.26  18.67\n";
    plotfile << "123.05  13.86\n";
    plotfile << "120.73  13.80\n";
    plotfile << "120.43  16.43\n";
    plotfile << "121.72  18.40\n";
    plotfile << "\n";
    plotfile << "125.34   9.79\n";
    plotfile << "125.56   6.28\n";
    plotfile << "122.38   7.00\n";
    plotfile << "125.10   9.38\n";
    plotfile << "\n";
    plotfile << "119.64  11.35\n";
    plotfile << "118.81  10.16\n";
    plotfile << "119.59  10.86\n";
    plotfile << "119.64  11.35\n";
    plotfile << "\n";
    plotfile << "-179.87  65.14\n";
    plotfile << "-177.13  65.63\n";
    plotfile << "-173.46  64.74\n";
    plotfile << "-171.13  66.38\n";
    plotfile << "-176.48  67.78\n";
    plotfile << "-178.80  68.42\n";
    plotfile << "\n";
    plotfile << "101.96  79.08\n";
    plotfile << "101.31  77.86\n";
    plotfile << "101.22  79.04\n";
    plotfile << "\n";
    plotfile << " 94.29  79.29\n";
    plotfile << " 95.31  78.68\n";
    plotfile << "100.02  79.43\n";
    plotfile << " 97.26  79.62\n";
    plotfile << " 95.44  79.65\n";
    plotfile << "\n";
    plotfile << " 95.46  80.62\n";
    plotfile << " 92.39  79.66\n";
    plotfile << " 95.07  80.54\n";
    plotfile << "\n";
    plotfile << "138.54  76.05\n";
    plotfile << "144.93  75.45\n";
    plotfile << "140.30  74.99\n";
    plotfile << "137.27  75.44\n";
    plotfile << "138.29  75.98\n";
    plotfile << "\n";
    plotfile << "146.08  75.29\n";
    plotfile << "147.75  74.73\n";
    plotfile << "145.85  75.06\n";
    plotfile << "\n";
    plotfile << "141.44  73.88\n";
    plotfile << "141.48  73.84\n";
    plotfile << "\n";
    plotfile << "  0.01 -71.68\n";
    plotfile << "  6.57 -70.57\n";
    plotfile << " 15.04 -70.44\n";
    plotfile << " 25.10 -70.75\n";
    plotfile << " 33.37 -69.10\n";
    plotfile << " 38.46 -69.77\n";
    plotfile << " 42.85 -68.16\n";
    plotfile << " 46.59 -67.23\n";
    plotfile << " 49.35 -66.96\n";
    plotfile << " 52.90 -65.97\n";
    plotfile << " 58.46 -67.20\n";
    plotfile << " 63.60 -67.58\n";
    plotfile << " 70.63 -68.41\n";
    plotfile << " 69.24 -70.36\n";
    plotfile << " 76.20 -69.44\n";
    plotfile << " 88.08 -66.64\n";
    plotfile << " 94.98 -66.52\n";
    plotfile << "101.53 -66.09\n";
    plotfile << "111.31 -65.91\n";
    plotfile << "118.64 -66.87\n";
    plotfile << "126.24 -66.24\n";
    plotfile << "133.09 -66.18\n";
    plotfile << "139.85 -66.72\n";
    plotfile << "146.86 -67.96\n";
    plotfile << "153.65 -68.82\n";
    plotfile << "159.94 -69.57\n";
    plotfile << "164.10 -70.67\n";
    plotfile << "170.19 -71.94\n";
    plotfile << "165.68 -74.64\n";
    plotfile << "163.82 -77.60\n";
    plotfile << "162.10 -78.95\n";
    plotfile << "166.72 -82.84\n";
    plotfile << "175.58 -83.86\n";
    plotfile << "\n";
    plotfile << "-178.56 -84.37\n";
    plotfile << "-147.96 -85.40\n";
    plotfile << "-152.96 -81.12\n";
    plotfile << "-153.95 -79.50\n";
    plotfile << "-151.24 -77.48\n";
    plotfile << "-146.74 -76.44\n";
    plotfile << "-137.68 -75.16\n";
    plotfile << "-131.63 -74.63\n";
    plotfile << "-123.05 -74.41\n";
    plotfile << "-114.76 -73.97\n";
    plotfile << "-111.91 -75.41\n";
    plotfile << "-105.05 -74.77\n";
    plotfile << "-100.90 -74.21\n";
    plotfile << "-101.04 -73.18\n";
    plotfile << "-100.28 -73.06\n";
    plotfile << "-93.06 -73.33\n";
    plotfile << "-85.40 -73.18\n";
    plotfile << "-79.82 -73.04\n";
    plotfile << "-78.21 -72.52\n";
    plotfile << "-71.90 -73.41\n";
    plotfile << "-67.51 -71.10\n";
    plotfile << "-67.57 -68.92\n";
    plotfile << "-66.65 -66.83\n";
    plotfile << "-64.30 -65.28\n";
    plotfile << "-59.14 -63.74\n";
    plotfile << "-59.58 -64.37\n";
    plotfile << "-62.50 -65.94\n";
    plotfile << "-62.48 -66.66\n";
    plotfile << "-65.64 -68.02\n";
    plotfile << "-63.85 -69.07\n";
    plotfile << "-61.69 -70.87\n";
    plotfile << "-60.89 -72.71\n";
    plotfile << "-61.07 -74.30\n";
    plotfile << "-63.33 -75.88\n";
    plotfile << "-76.05 -77.06\n";
    plotfile << "-83.04 -77.12\n";
    plotfile << "-74.30 -80.83\n";
    plotfile << "-56.40 -82.14\n";
    plotfile << "-42.46 -81.65\n";
    plotfile << "-31.60 -80.17\n";
    plotfile << "-34.01 -79.20\n";
    plotfile << "-32.48 -77.28\n";
    plotfile << "-26.28 -76.18\n";
    plotfile << "-17.18 -73.45\n";
    plotfile << "-11.20 -72.01\n";
    plotfile << " -8.67 -71.98\n";
    plotfile << " -5.45 -71.45\n";
    plotfile << " -0.82 -71.74\n";
    plotfile << "  0.07 -71.68\n";
    plotfile << "\n";
    plotfile << "164.65 -77.89\n";
    plotfile << "170.95 -77.37\n";
    plotfile << "179.67 -78.25\n";
    plotfile << "\n";
    plotfile << "-178.74 -78.24\n";
    plotfile << "-165.76 -78.47\n";
    plotfile << "-158.42 -77.73\n";
    plotfile << "\n";
    plotfile << "-58.98 -64.63\n";
    plotfile << "-60.99 -68.62\n";
    plotfile << "-61.02 -71.70\n";
    plotfile << "\n";
    plotfile << "-62.01 -74.94\n";
    plotfile << "-52.00 -77.07\n";
    plotfile << "-42.23 -77.80\n";
    plotfile << "-36.22 -78.03\n";
    plotfile << "\n";
    plotfile << "-35.03 -77.81\n";
    plotfile << "-26.13 -75.54\n";
    plotfile << "-19.35 -73.04\n";
    plotfile << "-12.16 -71.86\n";
    plotfile << " -6.15 -70.65\n";
    plotfile << " -0.57 -69.14\n";
    plotfile << "  4.93 -70.25\n";
    plotfile << " 10.91 -69.99\n";
    plotfile << " 16.52 -69.87\n";
    plotfile << " 25.41 -70.22\n";
    plotfile << " 32.13 -69.29\n";
    plotfile << " 33.62 -69.58\n";
    plotfile << "\n";
    plotfile << " 70.56 -68.53\n";
    plotfile << " 73.91 -69.51\n";
    plotfile << "\n";
    plotfile << " 81.42 -67.87\n";
    plotfile << " 84.67 -66.41\n";
    plotfile << " 89.07 -66.73\n";
    plotfile << "\n";
    plotfile << "-135.79 -74.67\n";
    plotfile << "-124.34 -73.22\n";
    plotfile << "-116.65 -74.08\n";
    plotfile << "-109.93 -74.64\n";
    plotfile << "-105.36 -74.56\n";
    plotfile << "-105.83 -74.77\n";
    plotfile << "\n";
    plotfile << "-69.30 -70.06\n";
    plotfile << "-71.33 -72.68\n";
    plotfile << "-71.42 -71.85\n";
    plotfile << "-75.10 -71.46\n";
    plotfile << "-71.79 -70.55\n";
    plotfile << "-70.34 -69.26\n";
    plotfile << "-69.34 -70.13\n";
    plotfile << "\n";
    plotfile << "-49.20 -77.83\n";
    plotfile << "-44.59 -78.79\n";
    plotfile << "-44.14 -80.13\n";
    plotfile << "-59.04 -79.95\n";
    plotfile << "-49.28 -77.84\n";
    plotfile << "-48.24 -77.81\n";
    plotfile << "\n";
    plotfile << "-58.13 -80.12\n";
    plotfile << "-63.25 -80.20\n";
    plotfile << "-58.32 -80.12\n";
    plotfile << "\n";
    plotfile << "-163.64 -78.74\n";
    plotfile << "-161.20 -79.93\n";
    plotfile << "-163.62 -78.74\n";
    plotfile << "\n";
    plotfile << " 66.82  66.82\n";
    plotfile << " 66.82  66.82\n";
    plotfile.close();
}

int gnuplot::plot_anomalies_world_map(
                                      string plot_script_filename,
                                      string plot_data_filename,
                                      string world_data_filename,
                                      string world_map_image_filename,
                                      string title,
                                      string subtitle,
                                      float subtitle_indent,
                                      vector<gridcell> &grid,
                                      int year,
                                      int reference_start_year,
                                      int reference_end_year,
                                      bool threed,
                                      float view_longitude, float view_latitude,
                                      bool show_cell_centres,
                                      int image_width,
                                      int image_height,
                                      float pressure,
                                      int graph_type,
                                      float range_min, float range_max)
{
    ofstream plotfile;

    printf("Calculating reference model...");

    for (int i = 0; i < (int)grid.size(); i++) {
        grid[i].update(reference_start_year,reference_end_year,
                       pressure,graph_type);
    }

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    if (!argo::file_exists(world_data_filename)) {
        printf("done\nSaving world map data...");
        save_world_map(world_data_filename);
        printf("done");
    }

    printf("\nPlotting anomalies...");

    float min_anomaly=9999,max_anomaly=-9999;
    plotfile.open(plot_data_filename.c_str());

    // calculate the anomalies for each grid cell
#pragma omp parallel for
    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {
            int grid_cell_index =
                globalgrid::get_closest_grid_cell((float)-longitude,
                                                  (float)latitude,grid);

            grid[grid_cell_index].temp_value =
                grid[grid_cell_index].get_anomaly(year,reference_start_year,
                                                  reference_end_year,pressure,
                                                  graph_type);
        }
    }

    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {

            // retrieve the anomaly for the grid cell
            int grid_cell_index =
                globalgrid::get_closest_grid_cell((float)-longitude,
                                                  (float)latitude,grid);
            float anomaly = grid[grid_cell_index].temp_value;

            // 9999 indicates no data
            if (anomaly==-9999) {
                anomaly=9999;
            }
            else {
                if (anomaly < min_anomaly) min_anomaly = anomaly;
                if (anomaly > max_anomaly) max_anomaly = anomaly;
            }

            if (show_cell_centres) {
                anomaly=0;
                for (int i = 0; i < (int)grid.size(); i++) {
                    if (((int)(grid[i].latitude)==latitude) &&
                        ((int)(grid[i].longitude)==longitude)) {
                        anomaly = 3;
                        break;
                    }
                }
            }

            plotfile << latitude << "  " << longitude << "      " <<
                anomaly << "\n";
        }
        plotfile << "\n";
    }
    plotfile.close();


    string file_type = "png";
    if (world_map_image_filename != "") {
        if ((world_map_image_filename.substr((int)world_map_image_filename.size()-3,3) == "jpg") ||
            (world_map_image_filename.substr((int)world_map_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type =
                world_map_image_filename.substr((int)world_map_image_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << world_map_image_filename << "\"\n";

    if (range_max > range_min) {
        plotfile << "set cbrange [" << range_min << ":" << range_max << "]\n";
    }
    else {
        switch (graph_type) {
        case GRAPH_TEMPERATURE:
            plotfile << "set cbrange [-5:5]\n";
            break;
        case GRAPH_SALINITY:
            plotfile << "set cbrange [" << min_anomaly << ":" <<
                max_anomaly << "]\n";
            break;
        case GRAPH_DENSITY:
            plotfile << "set cbrange [" << min_anomaly << ":" <<
                max_anomaly << "]\n";
            break;
        case GRAPH_OXYGEN:
            plotfile << "set cbrange [" << min_anomaly << ":" <<
                max_anomaly << "]\n";
            break;
        case GRAPH_OHC:
            plotfile << "set cbrange [" << min_anomaly << ":" <<
                max_anomaly << "]\n";
            break;
        }
    }

    if (!threed) {
        // 2D map
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

        plotfile << "set rmargin screen 0.85\n";

        plotfile << "unset key\n";
        plotfile << "set tics scale 0.5\n";
        plotfile << "unset xtics\n";
        plotfile << "unset ytics\n";
        plotfile << "set xrange[-179:179]\n"; // longitude range
        plotfile << "set yrange[-89:89]\n"; // latitude range
        plotfile << "set format '%g'\n";
        plotfile << "set palette defined (0 \"blue\",17 \"#00ffff\",33 " \
            "\"white\",50 \"yellow\",66 \"red\", 100 \"#990000\",101 " \
            "\"grey\")\n";
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";
        plotfile << "set title \"" << title << "\"\n";
        if (subtitle != "") {
            plotfile << "set label \"" << subtitle <<
                "\" at screen " << subtitle_indent-(0.28-0.17) <<
                ", screen 0.92\n";
        }
        plotfile << "plot \"" << plot_data_filename <<
            "\" u 2:1:3 w image, \"" << world_data_filename <<
            "\" with lines linestyle 1\n";
    }
    else {
        // 3D map
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_width << "\n";

        plotfile << "# color definitions\n";
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb '#000000' lt 1 lw 2\n";
        plotfile << "set style line 2 lc rgb '#c0c0c0' lt 2 lw 1\n";
        plotfile << "unset key; unset border\n";
        plotfile << "set tics scale 0\n";
        plotfile << "set lmargin screen 0\n";
        plotfile << "set bmargin screen 0\n";
        plotfile << "set rmargin screen 1\n";
        plotfile << "set tmargin screen 1\n";
        plotfile << "set format ''\n";
        plotfile << "set mapping spherical\n";
        plotfile << "set angles degrees\n";
        plotfile << "set hidden3d front\n";
        plotfile << "# Set xy-plane to intersect z axis at -1 to " \
            "avoid an offset between the lowest z\n";
        plotfile << "# value and the plane\n";
        plotfile << "set xyplane at -1\n";
        float lng = 90-view_longitude;
        if (lng<=0) lng += 360;
        if (lng>=360) lng -= 360;
        float lat = 90-view_latitude;
        if (lat<0) lat += 180;
        if (lat>=180) lat -= 180;
        plotfile << "set view " << lat << "," << lng << "\n";
        plotfile << "set parametric\n";
        plotfile << "set isosamples 25\n";
        plotfile << "set urange[0:360]\n";
        plotfile << "set vrange[-90:90]\n";
        plotfile << "set xrange[-1:1]\n";
        plotfile << "set yrange[-1:1]\n";
        plotfile << "set zrange[-1:1]\n";
        plotfile << "set palette defined (0 \"blue\",17 \"#00ffff\"," \
            "33 \"white\",50 \"yellow\",66 \"red\",100 \"#990000\"," \
            "101 \"grey\")\n";
        plotfile << "r = 0.99\n";
        plotfile << "set hidden3d front\n";
        plotfile << "splot \"" << plot_data_filename <<
            "\" u 2:1:(1):3 w pm3d, r*cos(v)*cos(u),r*cos(v)*sin(u),r*sin(v) " \
            "w l ls 2, \"" << world_data_filename << "\" u 1:2:(1) w l ls 1\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_averages_world_map(
                                     string plot_script_filename,
                                     string plot_data_filename,
                                     string world_data_filename,
                                     string world_map_image_filename,
                                     string title,
                                     string subtitle,
                                     float subtitle_indent,
                                     vector<gridcell> &grid,
                                     int start_year,int end_year,
                                     bool threed,
                                     float view_longitude, float view_latitude,
                                     int image_width,
                                     int image_height,
                                     float pressure,
                                     int graph_type,
                                     float range_min, float range_max)
{
    ofstream plotfile;

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    if (!argo::file_exists(world_data_filename)) {
        printf("done\nSaving world map data...");
        save_world_map(world_data_filename);
        printf("done");
    }

    printf("\nPlotting average values on map...");

    plotfile.open(plot_data_filename.c_str());
    float min_value=9999,max_value=-9999;
    globalgrid::clear_temp_values(grid);
    float degreegrid[180][360];
#pragma omp parallel for
    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {
            float value = 0;
            int grid_cell_index =
                globalgrid::get_closest_grid_cell((float)-longitude,
                                                  (float)latitude,grid);

            switch(graph_type) {
            case GRAPH_TEMPERATURE:
                value = grid[grid_cell_index].get_average_temperature_years(start_year,end_year,pressure);
                break;
            case GRAPH_SALINITY:
                value = grid[grid_cell_index].get_average_salinity_years(start_year,end_year,pressure);
                break;
            case GRAPH_DENSITY: {
                float t = grid[grid_cell_index].get_average_temperature_years(start_year,end_year,pressure);
                float s = grid[grid_cell_index].get_average_salinity_years(start_year,end_year,pressure);
                if ((t>-9999.0f) && (s>-9999.0f)) {
                    value = argo::density(t,s);
                }
                else {
                    value = -9999.0f;
                }
                break;
            }
            case GRAPH_OXYGEN:
                value = grid[grid_cell_index].get_average_oxygen_years(start_year,end_year,pressure);
                break;
            case GRAPH_OHC: {
                float t = grid[grid_cell_index].get_average_temperature_years(start_year,end_year,pressure);
                float s = grid[grid_cell_index].get_average_salinity_years(start_year,end_year,pressure);
                if ((t>-9999.0f) && (s>-9999.0f)) {
                    value = argo::ocean_heat_content(t,s,pressure);
                }
                else {
                    value = -9999.0f;
                }
                break;
            }
            }

            // 9999 indicates no data
            if (value==-9999) {
                value=9999;
            }

            degreegrid[latitude+89][longitude+179] = value;
        }
    }

    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {
            plotfile << latitude << "  " << longitude << "      " <<
                degreegrid[latitude+89][longitude+179] << "\n";
            if (degreegrid[latitude+89][longitude+179] != 9999) {
                if (degreegrid[latitude+89][longitude+179] < min_value) {
                    min_value = degreegrid[latitude+89][longitude+179];
                }
                if (degreegrid[latitude+89][longitude+179] > max_value) {
                    max_value = degreegrid[latitude+89][longitude+179];
                }
            }
        }
        plotfile << "\n";
    }
    plotfile.close();


    string file_type = "png";
    if (world_map_image_filename != "") {
        if ((world_map_image_filename.substr((int)world_map_image_filename.size()-3,3) == "jpg") ||
            (world_map_image_filename.substr((int)world_map_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = world_map_image_filename.substr((int)world_map_image_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << world_map_image_filename << "\"\n";
    switch (graph_type) {
    case GRAPH_TEMPERATURE:
        min_value = 0;
        max_value = 40;
        break;
    }

    if (range_max > range_min) {
        plotfile << "set cbrange [" << range_min << ":" << range_max << "]\n";
    }
    else {
        plotfile << "set cbrange [" << min_value << ":" << max_value << "]\n";
    }

    if (!threed) {
        // 2D map
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

        plotfile << "set rmargin screen 0.85\n";

        plotfile << "unset key\n";
        plotfile << "set tics scale 0.5\n";
        plotfile << "unset xtics\n";
        plotfile << "unset ytics\n";
        plotfile << "set xrange[-179:179]\n"; // longitude range
        plotfile << "set yrange[-89:89]\n"; // latitude range
        plotfile << "set format '%g'\n";
        if (graph_type != GRAPH_DENSITY) {
            plotfile << "set palette defined (-1 \"grey\",0 \"blue\",17 " \
                "\"#00ffff\",33 \"white\",50 \"yellow\",66 \"red\", 100 " \
                "\"#990000\",101 \"grey\")\n";
        }
        else {
            plotfile << "set palette defined (-1 \"grey\",0 \"red\",17 " \
                "\"yellow\",33 \"white\",50 \"#00ffff\",100 \"blue\", " \
                "101 \"grey\")\n";
        }
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";
        plotfile << "set title \"" << title << "\"\n";
        if (subtitle != "") {
            plotfile << "set label \"" << subtitle << "\" at screen " <<
                subtitle_indent-(0.28-0.17) << ", screen 0.92\n";
        }
        plotfile << "plot \"" << plot_data_filename <<
            "\" u 2:1:3 w image, \"" << world_data_filename <<
            "\" with lines linestyle 1\n";
    }
    else {
        // 3D map
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_width << "\n";

        plotfile << "# color definitions\n";
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb '#000000' lt 1 lw 2\n";
        plotfile << "set style line 2 lc rgb '#c0c0c0' lt 2 lw 1\n";
        plotfile << "unset key; unset border\n";
        plotfile << "set tics scale 0\n";
        plotfile << "set lmargin screen 0\n";
        plotfile << "set bmargin screen 0\n";
        plotfile << "set rmargin screen 1\n";
        plotfile << "set tmargin screen 1\n";
        plotfile << "set format ''\n";
        plotfile << "set mapping spherical\n";
        plotfile << "set angles degrees\n";
        plotfile << "set hidden3d front\n";
        plotfile << "# Set xy-plane to intersect z axis at -1 to avoid " \
            "an offset between the lowest z\n";
        plotfile << "# value and the plane\n";
        plotfile << "set xyplane at -1\n";
        float lng = 90-view_longitude;
        if (lng<=0) lng += 360;
        if (lng>=360) lng -= 360;
        float lat = 90-view_latitude;
        if (lat<0) lat += 180;
        if (lat>=180) lat -= 180;
        plotfile << "set view " << lat << "," << lng << "\n";
        plotfile << "set parametric\n";
        plotfile << "set isosamples 25\n";
        plotfile << "set urange[0:360]\n";
        plotfile << "set vrange[-90:90]\n";
        plotfile << "set xrange[-1:1]\n";
        plotfile << "set yrange[-1:1]\n";
        plotfile << "set zrange[-1:1]\n";
        if (graph_type != GRAPH_DENSITY) {
            plotfile << "set palette defined (-1 \"grey\",0 \"blue\"," \
                "17 \"#00ffff\",33 \"white\",50 \"yellow\",66 \"red\"," \
                "100 \"#990000\",101 \"grey\")\n";
        }
        else {
            plotfile << "set palette defined (-1 \"grey\",0 \"red\",17 " \
                "\"yellow\",33 \"white\",50 \"#00ffff\",100 \"blue\", " \
                "101 \"grey\")\n";
        }
        plotfile << "r = 0.99\n";
        plotfile << "set hidden3d front\n";
        plotfile << "splot \"" << plot_data_filename <<
            "\" u 2:1:(1):3 w pm3d, r*cos(v)*cos(u),r*cos(v)*sin(u),r*sin(v)" \
            " w l ls 2, \"" << world_data_filename <<
            "\" u 1:2:(1) w l ls 1\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_platforms(
                            string plot_script_filename,
                            string plot_data_filename,
                            string world_data_filename,
                            string world_map_image_filename,
                            string title,
                            string subtitle,
                            float subtitle_indent,
                            vector<gridcell> &grid,
                            int start_year,int end_year,
                            bool threed,
                            float view_longitude, float view_latitude,
                            int image_width,
                            int image_height)
{
    ofstream plotfile;

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    if (!argo::file_exists(world_data_filename)) {
        printf("done\nSaving world map data...");
        save_world_map(world_data_filename);
        printf("done");
    }

    printf("\nPlotting platforms on map...");

#pragma omp parallel for
    for (unsigned int i = 0; i < grid.size(); i++) {
        grid[i].temp_value = 9999;

        for (int yr = start_year; yr <= end_year; yr++) {
            if (grid[i].samples[yr].size() > 0) {
                grid[i].temp_value =
                    3 + (grid[i].samples[yr][0].platform_number%30);
                break;
            }
        }
    }

    plotfile.open(plot_data_filename.c_str());
    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {

            int grid_cell_index =
                globalgrid::get_closest_grid_cell((float)-longitude,
                                                  (float)latitude,grid);

            plotfile << latitude << "  " << longitude << "      " <<
                grid[grid_cell_index].temp_value << "\n";
        }
        plotfile << "\n";
    }
    plotfile.close();


    string file_type = "png";
    if (world_map_image_filename != "") {
        if ((world_map_image_filename.substr((int)world_map_image_filename.size()-3,3) == "jpg") ||
            (world_map_image_filename.substr((int)world_map_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = world_map_image_filename.substr((int)world_map_image_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << world_map_image_filename << "\"\n";
    plotfile << "set cbrange [0:40]\n"; // Temperature range

    if (!threed) {
        // 2D map
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

        plotfile << "set rmargin screen 0.85\n";

        plotfile << "unset key\n";
        plotfile << "set tics scale 0.5\n";
        plotfile << "unset xtics\n";
        plotfile << "unset ytics\n";
        plotfile << "set xrange[-179:179]\n"; // longitude range
        plotfile << "set yrange[-89:89]\n"; // latitude range
        //plotfile << "set format '%g'\n";
        plotfile << "set format ''\n";
        plotfile << "set palette defined (0 \"blue\",17 \"#00ffff\",33 " \
            "\"white\",50 \"yellow\",66 \"red\", 100 \"#990000\"," \
            "101 \"grey\")\n";
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";
        plotfile << "set title \"" << title << "\"\n";
        if (subtitle != "") {
            plotfile << "set label \"" << subtitle <<
                "\" at screen " << subtitle_indent-(0.28-0.17) <<
                ", screen 0.92\n";
        }
        plotfile << "plot \"" << plot_data_filename <<
            "\" u 2:1:3 w image, \"" << world_data_filename <<
            "\" with lines linestyle 1\n";
    }
    else {
        // 3D map
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_width << "\n";

        plotfile << "# color definitions\n";
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb '#000000' lt 1 lw 2\n";
        plotfile << "set style line 2 lc rgb '#c0c0c0' lt 2 lw 1\n";
        plotfile << "unset key; unset border\n";
        plotfile << "set tics scale 0\n";
        plotfile << "set lmargin screen 0\n";
        plotfile << "set bmargin screen 0\n";
        plotfile << "set rmargin screen 1\n";
        plotfile << "set tmargin screen 1\n";
        plotfile << "set format ''\n";
        plotfile << "set mapping spherical\n";
        plotfile << "set angles degrees\n";
        plotfile << "set hidden3d front\n";
        plotfile << "# Set xy-plane to intersect z axis at -1 to avoid " \
            "an offset between the lowest z\n";
        plotfile << "# value and the plane\n";
        plotfile << "set xyplane at -1\n";
        float lng = 90-view_longitude;
        if (lng<=0) lng += 360;
        if (lng>=360) lng -= 360;
        float lat = 90-view_latitude;
        if (lat<0) lat += 180;
        if (lat>=180) lat -= 180;
        plotfile << "set view " << lat << "," << lng << "\n";
        plotfile << "set parametric\n";
        plotfile << "set isosamples 25\n";
        plotfile << "set urange[0:360]\n";
        plotfile << "set vrange[-90:90]\n";
        plotfile << "set xrange[-1:1]\n";
        plotfile << "set yrange[-1:1]\n";
        plotfile << "set zrange[-1:1]\n";
        plotfile << "set palette defined (0 \"blue\",17 \"#00ffff\"," \
            "33 \"white\",50 \"yellow\",66 \"red\",100 \"#990000\"," \
            "101 \"grey\")\n";
        plotfile << "r = 0.99\n";
        plotfile << "set hidden3d front\n";
        plotfile << "splot \"" << plot_data_filename <<
            "\" u 2:1:(1):3 w pm3d, r*cos(v)*cos(u),r*cos(v)*sin(u),r*sin(v) " \
            "w l ls 2, \"" << world_data_filename << "\" u 1:2:(1) w l ls 1\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_cycles(
                         string plot_script_filename,
                         string plot_data_filename,
                         string cycles_filename,
                         string title,
                         string subtitle,
                         float subtitle_indent,
                         vector<argodata> &data,
                         int image_width,
                         int image_height,
                         float max_pressure,
                         int graph_type)
{
    unsigned int pressure_steps=400;
    ofstream plotfile;

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    int min_cycle_number=0,max_cycle_number=0;
    if (argo::cycle_range(data,min_cycle_number,
                          max_cycle_number)==-1) {
        return -1;
    }

    printf("\nPlotting cycles...");

    plotfile.open(plot_data_filename.c_str());

    unsigned int i;
    float pressure, value=0;
    float min_value = 9999;
    float max_value = -9999;
    for (unsigned int cycle=min_cycle_number;
         cycle <= (unsigned int)max_cycle_number; cycle++) {
        for (i = 0; i < data.size(); i++) {
            if ((unsigned int)data[i].cycle_number==cycle) {
                break;
            }
        }
        if (i<data.size()) {
            for (unsigned int p = 0; p < pressure_steps; p++) {
                pressure = p * max_pressure / pressure_steps;
                switch (graph_type) {
                case GRAPH_TEMPERATURE:
                    value = argo::temperature_at_pressure(pressure, data[i]);
                    break;
                case GRAPH_SALINITY:
                    value = argo::salinity_at_pressure(pressure, data[i]);
                    break;
                case GRAPH_DENSITY: {
                    float t = argo::temperature_at_pressure(pressure, data[i]);
                    float s = argo::salinity_at_pressure(pressure, data[i]);
                    if ((t>-9999.0f) && (s>-9999.0f)) {
                        value = argo::density(t,s);
                    }
                    else {
                        value = -9999.0f;
                    }
                    break;
                }
                case GRAPH_OXYGEN:
                    value = argo::oxygen_at_pressure(pressure, data[i]);
                    break;
                case GRAPH_OHC: {
                    float t = argo::temperature_at_pressure(pressure, data[i]);
                    float s = argo::salinity_at_pressure(pressure, data[i]);
                    if ((t>-9999.0f) && (s>-9999.0f)) {
                        value = argo::ocean_heat_content(t,s,pressure);
                    }
                    else {
                        value = -9999.0f;
                    }
                    break;
                }
                }
                if (value>-9999) {
                    if (value < min_value) min_value=value;
                    if (value > max_value) max_value=value;
                }
                else {
                    value = 9999;
                }
                plotfile << cycle << "  " << pressure << "  " << value << "\n";
            }
        }
    }

    plotfile.close();


    string file_type = "png";
    if (cycles_filename != "") {
        if ((cycles_filename.substr((int)cycles_filename.size()-3,3) == "jpg") ||
            (cycles_filename.substr((int)cycles_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = cycles_filename.substr((int)cycles_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << cycles_filename << "\"\n";
    plotfile << "set cbrange ["<< min_value << ":" << max_value << "]\n";

    plotfile << "set border lw 1.5\n";
    plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

    plotfile << "set rmargin screen 0.85\n";

    plotfile << "unset key\n";
    plotfile << "set tics scale 0.5\n";
    plotfile << "set xrange[" << min_cycle_number << ":" <<
        max_cycle_number << "]\n";
    plotfile << "set yrange[" << max_pressure << ":0]\n";
    plotfile << "set xlabel \"Cycles\"\n";
    plotfile << "set ylabel \"Depth (metres)\"\n";
    plotfile << "set format '%g'\n";
    if (graph_type != GRAPH_DENSITY) {
        plotfile << "set palette defined (0 \"blue\",17 \"#00ffff\"," \
            "33 \"white\",50 \"yellow\",66 \"red\", 100 \"#990000\"," \
            "101 \"grey\")\n";
    }
    else {
        plotfile << "set palette defined (0 \"red\",17 \"yellow\"," \
            "33 \"white\",50 \"#00ffff\",100 \"blue\", 101 \"grey\")\n";
    }
    plotfile << "set terminal " << file_type << " size " << image_width <<
        "," << image_height << "\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle <<
            "\" at screen " << subtitle_indent-0.04 << ", screen 0.92\n";
    }
    plotfile << "plot \"" << plot_data_filename <<
        "\" using 1:2:3 with image\n";

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_latitudes(
                            string plot_script_filename,
                            string plot_data_filename,
                            string latitudes_filename,
                            string title,
                            string subtitle,
                            float subtitle_indent,
                            vector<argodata> &data,
                            vector<float> &area,
                            int image_width,
                            int image_height,
                            float max_pressure,
                            int graph_type,
                            float range_min, float range_max,
                            int start_year, int end_year, int month)
{
    const unsigned int pressure_steps=400;
    vector<vector<vector<float> > > latitudedata;
    ofstream plotfile;

    // update the array
    argo::update_latitudes(data, latitudedata, pressure_steps, max_pressure,
                           start_year, end_year, month);

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    printf("\nPlotting latitudes...");

    plotfile.open(plot_data_filename.c_str());

    //unsigned int i;
    //float pressure, value;
    float min_value = 9999;
    float max_value = -9999;
    int min_lat = -90;
    int max_lat = 90;

    if (area.size()==4) {
        if (area[0] < area[2]) {
            min_lat = (int)area[0];
            max_lat = (int)area[2];
        }
        else {
            max_lat = (int)area[0];
            min_lat = (int)area[2];
        }
    }

    for (int lat=min_lat+90; lat < max_lat+90; lat++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            float pressure = p*max_pressure/pressure_steps;
            if (latitudedata[lat][p][(graph_type*2)+1]>0) {
                float value = latitudedata[lat][p][graph_type*2];
                plotfile << lat-90 << "  " << pressure << "  " <<
                    value << "\n";
                if (value < min_value) {
                    min_value = value;
                }
                if (value > max_value) {
                    max_value = value;
                }
            }
            else {
                plotfile << lat-90 << "  " << pressure << "  9999\n";
            }
        }
    }

    plotfile.close();


    string file_type = "png";
    if (latitudes_filename != "") {
        if ((latitudes_filename.substr((int)latitudes_filename.size()-3,3) == "jpg") ||
            (latitudes_filename.substr((int)latitudes_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = latitudes_filename.substr((int)latitudes_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << latitudes_filename << "\"\n";

    // set the range
    if (range_max > range_min) {
        plotfile << "set cbrange [" << range_min << ":" << range_max << "]\n";
    }
    else {
        switch (graph_type) {
        case GRAPH_TEMPERATURE:
            plotfile << "set cbrange [0:40]\n";
            break;
        case GRAPH_SALINITY:
            if (min_value<30) min_value=30;
            plotfile << "set cbrange [" << min_value << ":" <<
                max_value << "]\n";
            break;
        case GRAPH_DENSITY:
            plotfile << "set cbrange [" << min_value << ":" <<
                max_value << "]\n";
            break;
        case GRAPH_OXYGEN:
            plotfile << "set cbrange [" << min_value << ":" <<
                max_value << "]\n";
            break;
        }
    }

    plotfile << "set border lw 1.5\n";
    plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

    plotfile << "set rmargin screen 0.85\n";

    plotfile << "unset key\n";
    plotfile << "set tics scale 0.5\n";
    plotfile << "set xrange[" << min_lat << ":" << max_lat << "]\n";
    plotfile << "set yrange[" << max_pressure << ":0]\n";
    plotfile << "set xlabel \"Latitude (degrees)\"\n";
    plotfile << "set ylabel \"Depth (metres)\"\n";
    plotfile << "set format '%g'\n";
    if (graph_type != GRAPH_DENSITY) {
        plotfile << "set palette defined (-1 \"grey\",0 \"blue\"," \
            "17 \"#00ffff\",33 \"white\",50 \"yellow\",66 \"red\", " \
            "100 \"#990000\",101 \"grey\")\n";
    }
    else {
        plotfile << "set palette defined (-1 \"grey\",0 \"red\"," \
            "17 \"yellow\",33 \"white\",50 \"#00ffff\"," \
            "100 \"blue\", 101 \"grey\")\n";
    }
    plotfile << "set terminal " << file_type << " size " <<
        image_width << "," << image_height << "\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent-0.04 << ", screen 0.92\n";
    }
    plotfile << "plot \"" << plot_data_filename <<
        "\" using 1:2:3 with image\n";

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_latitude_anomalies(
                                     string plot_script_filename,
                                     string plot_data_filename,
                                     string latitudes_filename,
                                     string title,
                                     string subtitle,
                                     float subtitle_indent,
                                     vector<argodata> &data,
                                     vector<float> &area,
                                     int image_width,
                                     int image_height,
                                     float max_pressure,
                                     int graph_type,
                                     int start_year,
                                     int end_year,
                                     int reference_start_year,
                                     int reference_end_year,
                                     float range_min, float range_max)
{
    const unsigned int pressure_steps=400;
    ofstream plotfile;
    vector<vector<vector<float> > > anomalies;

    argo::latitude_anomalies(data, anomalies,
                             pressure_steps,
                             max_pressure,
                             start_year, end_year,
                             reference_start_year,
                             reference_end_year);

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    printf("\nPlotting latitude anomalies...");

    plotfile.open(plot_data_filename.c_str());

    //unsigned int i;
    //float pressure, value;
    float min_value = 9999;
    float max_value = -9999;
    int min_lat = -90;
    int max_lat = 90;

    if (area.size()==4) {
        if (area[0] < area[2]) {
            min_lat = (int)area[0];
            max_lat = (int)area[2];
        }
        else {
            max_lat = (int)area[0];
            min_lat = (int)area[2];
        }
    }

    for (int lat=min_lat+90; lat < max_lat+90; lat++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            float pressure = p*max_pressure/pressure_steps;
            if (anomalies[lat][p][(graph_type*2)+1]>0) {
                float value = anomalies[lat][p][graph_type*2];
                plotfile << lat-90 << "  " << pressure << "  " <<
                    value << "\n";
                if (value < min_value) {
                    min_value = value;
                }
                if (value > max_value) {
                    max_value = value;
                }
            }
            else {
                plotfile << lat-90 << "  " << pressure << "  9999\n";
            }
        }
    }

    plotfile.close();


    string file_type = "png";
    if (latitudes_filename != "") {
        if ((latitudes_filename.substr((int)latitudes_filename.size()-3,3) == "jpg") ||
            (latitudes_filename.substr((int)latitudes_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = latitudes_filename.substr((int)latitudes_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << latitudes_filename << "\"\n";

    if (range_max > range_min) {
        plotfile << "set cbrange [" << range_min << ":" << range_max << "]\n";
    }
    else {
        plotfile << "set cbrange [" << min_value << ":" << max_value << "]\n";
    }

    plotfile << "set border lw 1.5\n";
    plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

    plotfile << "set rmargin screen 0.85\n";

    plotfile << "unset key\n";
    plotfile << "set tics scale 0.5\n";
    plotfile << "set xrange[" << min_lat << ":" << max_lat << "]\n";
    plotfile << "set yrange[" << max_pressure << ":0]\n";
    plotfile << "set xlabel \"Latitude (degrees)\"\n";
    plotfile << "set ylabel \"Depth (metres)\"\n";
    plotfile << "set format '%g'\n";
    plotfile << "set palette defined (-1 \"grey\",0 \"blue\"," \
        "17 \"#00ffff\",33 \"white\",50 \"yellow\",66 \"red\", " \
        "100 \"#990000\",101 \"grey\")\n";
    plotfile << "set terminal " << file_type << " size " <<
        image_width << "," << image_height << "\n";
    plotfile << "set title \"" << title << "\"\n";
    if (subtitle != "") {
        plotfile << "set label \"" << subtitle << "\" at screen " <<
            subtitle_indent-0.04 << ", screen 0.92\n";
    }
    plotfile << "plot \"" << plot_data_filename <<
        "\" using 1:2:3 with image\n";

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_currents(
                           string plot_script_filename,
                           string plot_data_filename,
                           string world_data_filename,
                           string world_map_image_filename,
                           string title,
                           string subtitle,
                           float subtitle_indent,
                           vector<argodata> &data,
                           bool threed,
                           float view_longitude, float view_latitude,
                           int image_width,
                           int image_height,
                           int graph_type,
                           float range_min, float range_max)
{
    ofstream plotfile;

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    if (!argo::file_exists(world_data_filename)) {
        printf("done\nSaving world map data...");
        save_world_map(world_data_filename);
        printf("done");
    }

    printf("Calculating currents\n");

    vector<vector<vector<int> > > currents;
    argo::calculate_currents(data, currents);

    printf("\nPlotting currents on map...");

    plotfile.open(plot_data_filename.c_str());
    float value, min_value=0, max_value=360;
    if (graph_type != GRAPH_DIRECTION) {
        min_value = 999999;
        max_value = -999999;
    }
    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {

            float av=0, turbulence=9999;
            int max=0, avhits=0, dir=-1;
            for (int d = 0; d < 45; d++) {
                int prev_d = d-1;
                if (prev_d<0) prev_d+=45;
                int next_d = d+1;
                if (next_d>=45) next_d-=45;
                int score = currents[longitude+179][latitude+89][d] +
                    currents[longitude+179][latitude+89][prev_d] +
                    currents[longitude+179][latitude+89][next_d];

                if (score>max) {
                    max = score;
                    dir = d*8;
                }
                av += score;
                avhits++;
            }
            if (avhits>0) {
                av /= avhits;
                turbulence = 1.0f/(1.0f + (max - av));
            }

            value = 9999;

            switch(graph_type) {
            case GRAPH_DIRECTION: {
                if ((dir>-1) && (turbulence!=9999)) {
                    value = dir;
                }
                else {
                    value = 9999;
                }
                break;
            }
            case GRAPH_TURBULENCE: {
                value = turbulence;
                if (value!=9999) {
                    if (value>max_value) max_value = value;
                    if (value<min_value) min_value = value;
                }
                break;
            }
            }

            plotfile << latitude << "  " << longitude << "      " <<
                value << "\n";
        }
        plotfile << "\n";
    }
    plotfile.close();


    string file_type = "png";
    if (world_map_image_filename != "") {
        if ((world_map_image_filename.substr((int)world_map_image_filename.size()-3,3) == "jpg") ||
            (world_map_image_filename.substr((int)world_map_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = world_map_image_filename.substr((int)world_map_image_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << world_map_image_filename << "\"\n";

    if (range_max > range_min) {
        plotfile << "set cbrange [" << range_min << ":" << range_max << "]\n";
    }
    else {
        plotfile << "set cbrange [" << min_value << ":" << max_value << "]\n";
    }

    if (!threed) {
        // 2D map
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

        plotfile << "set rmargin screen 0.85\n";

        plotfile << "unset key\n";
        plotfile << "set tics scale 0.5\n";
        plotfile << "unset xtics\n";
        plotfile << "unset ytics\n";
        plotfile << "set xrange[-179:179]\n"; // longitude range
        plotfile << "set yrange[-89:89]\n"; // latitude range
        plotfile << "set format '%g'\n";
        if (graph_type == GRAPH_DIRECTION) {
            plotfile << "set palette defined (-1 \"grey\",0 \"blue\"," \
                "25 \"#00ffff\",50 \"white\",75 \"yellow\"," \
                "100 \"red\",101 \"grey\")\n";
        }
        else {
            plotfile << "set palette defined (0 \"blue\"," \
                "17 \"#00ffff\",33 \"white\",50 \"yellow\"," \
                "66 \"red\", 100 \"#990000\",101 \"grey\")\n";
        }
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";
        plotfile << "set title \"" << title << "\"\n";
        if (subtitle != "") {
            plotfile << "set label \"" << subtitle <<
                "\" at screen " << subtitle_indent-(0.28-0.17) <<
                ", screen 0.92\n";
        }
        plotfile << "plot \"" << plot_data_filename <<
            "\" u 2:1:3 w image, \"" << world_data_filename <<
            "\" with lines linestyle 1\n";
    }
    else {
        // 3D map
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_width << "\n";

        plotfile << "# color definitions\n";
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb '#000000' lt 1 lw 2\n";
        plotfile << "set style line 2 lc rgb '#c0c0c0' lt 2 lw 1\n";
        plotfile << "unset key; unset border\n";
        plotfile << "set tics scale 0\n";
        plotfile << "set lmargin screen 0\n";
        plotfile << "set bmargin screen 0\n";
        plotfile << "set rmargin screen 1\n";
        plotfile << "set tmargin screen 1\n";
        plotfile << "set format ''\n";
        plotfile << "set mapping spherical\n";
        plotfile << "set angles degrees\n";
        plotfile << "set hidden3d front\n";
        plotfile << "# Set xy-plane to intersect z axis at -1 to " \
            "avoid an offset between the lowest z\n";
        plotfile << "# value and the plane\n";
        plotfile << "set xyplane at -1\n";
        float lng = 90-view_longitude;
        if (lng<=0) lng += 360;
        if (lng>=360) lng -= 360;
        float lat = 90-view_latitude;
        if (lat<0) lat += 180;
        if (lat>=180) lat -= 180;
        plotfile << "set view " << lat << "," << lng << "\n";
        plotfile << "set parametric\n";
        plotfile << "set isosamples 25\n";
        plotfile << "set urange[0:360]\n";
        plotfile << "set vrange[-90:90]\n";
        plotfile << "set xrange[-1:1]\n";
        plotfile << "set yrange[-1:1]\n";
        plotfile << "set zrange[-1:1]\n";
        if (graph_type == GRAPH_DIRECTION) {
            plotfile << "set palette defined (0 \"blue\"," \
                "25 \"#00ffff\",50 \"white\",75 \"yellow\"," \
                "100 \"red\",101 \"grey\")\n";
        }
        else {
            plotfile << "set palette defined (0 \"blue\"," \
                "17 \"#00ffff\",33 \"white\",50 \"yellow\"," \
                "66 \"red\", 100 \"#990000\",101 \"grey\")\n";
        }
        plotfile << "r = 0.99\n";
        plotfile << "set hidden3d front\n";
        plotfile << "splot \"" << plot_data_filename <<
            "\" u 2:1:(1):3 w pm3d, r*cos(v)*cos(u),r*cos(v)*sin(u),r*sin(v)" \
            " w l ls 2, \"" << world_data_filename <<
            "\" u 1:2:(1) w l ls 1\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}

int gnuplot::plot_speeds(
                         string plot_script_filename,
                         string plot_data_filename,
                         string world_data_filename,
                         string world_map_image_filename,
                         string title,
                         string subtitle,
                         float subtitle_indent,
                         vector<argodata> &data,
                         bool threed,
                         float view_longitude, float view_latitude,
                         int image_width,
                         int image_height,
                         float range_min, float range_max)
{
    ofstream plotfile;

    std::remove(plot_script_filename.c_str());
    std::remove(plot_data_filename.c_str());

    if (!argo::file_exists(world_data_filename)) {
        printf("done\nSaving world map data...");
        save_world_map(world_data_filename);
        printf("done");
    }

    printf("Calculating speeds\n");

    vector<vector<float> > speeds;
    argo::calculate_speeds(data, speeds);

    printf("\nPlotting speeds on map...");

    plotfile.open(plot_data_filename.c_str());
    float min_value=99999, max_value=-99999;
    for (int latitude=-89; latitude < 90; latitude++) {
        for (int longitude=-179; longitude < 180; longitude++) {
            float speed = speeds[longitude+179][latitude+89];
            if (speed==0) {
                speed=9999;
            }
            else {
                if (speed>max_value) max_value = speed;
                if (speed<min_value) min_value = speed;
            }
            plotfile << latitude << "  " << longitude << "      " <<
                speed << "\n";
        }
        plotfile << "\n";
    }
    plotfile.close();


    string file_type = "png";
    if (world_map_image_filename != "") {
        if ((world_map_image_filename.substr((int)world_map_image_filename.size()-3,3) == "jpg") ||
            (world_map_image_filename.substr((int)world_map_image_filename.size()-4,4) == "jpeg")) {
            file_type = "jpeg";
        }
        else {
            file_type = world_map_image_filename.substr((int)world_map_image_filename.size()-3,3);
        }
    }

    plotfile.open(plot_script_filename.c_str());
    plotfile << "reset\n";

    plotfile << "set output \"" << world_map_image_filename << "\"\n";

    if (range_max > range_min) {
        plotfile << "set cbrange [" << range_min << ":" << range_max << "]\n";
    }
    else {
        plotfile << "set cbrange [" << min_value << ":" << max_value << "]\n";
    }

    if (!threed) {
        // 2D map
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb 'black' lt 1 lw 2\n";

        plotfile << "set rmargin screen 0.85\n";

        plotfile << "unset key\n";
        plotfile << "set tics scale 0.5\n";
        plotfile << "unset xtics\n";
        plotfile << "unset ytics\n";
        plotfile << "set xrange[-179:179]\n"; // longitude range
        plotfile << "set yrange[-89:89]\n"; // latitude range
        plotfile << "set format '%g'\n";
        plotfile << "set palette defined (-1 \"grey\",0 \"blue\"," \
            "17 \"#00ffff\",33 \"white\",50 \"yellow\"," \
            "66 \"red\", 100 \"#990000\",101 \"grey\")\n";
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_height << "\n";
        plotfile << "set title \"" << title << "\"\n";
        if (subtitle != "") {
            plotfile << "set label \"" <<
                subtitle << "\" at screen " <<
                subtitle_indent-(0.28-0.17) << ", screen 0.92\n";
        }
        plotfile << "plot \"" << plot_data_filename <<
            "\" u 2:1:3 w image, \"" << world_data_filename <<
            "\" with lines linestyle 1\n";
    }
    else {
        // 3D map
        plotfile << "set terminal " << file_type << " size " <<
            image_width << "," << image_width << "\n";

        plotfile << "# color definitions\n";
        plotfile << "set border lw 1.5\n";
        plotfile << "set style line 1 lc rgb '#000000' lt 1 lw 2\n";
        plotfile << "set style line 2 lc rgb '#c0c0c0' lt 2 lw 1\n";
        plotfile << "unset key; unset border\n";
        plotfile << "set tics scale 0\n";
        plotfile << "set lmargin screen 0\n";
        plotfile << "set bmargin screen 0\n";
        plotfile << "set rmargin screen 1\n";
        plotfile << "set tmargin screen 1\n";
        plotfile << "set format ''\n";
        plotfile << "set mapping spherical\n";
        plotfile << "set angles degrees\n";
        plotfile << "set hidden3d front\n";
        plotfile << "# Set xy-plane to intersect z axis at -1 to avoid " \
            "an offset between the lowest z\n";
        plotfile << "# value and the plane\n";
        plotfile << "set xyplane at -1\n";
        float lng = 90-view_longitude;
        if (lng<=0) lng += 360;
        if (lng>=360) lng -= 360;
        float lat = 90-view_latitude;
        if (lat<0) lat += 180;
        if (lat>=180) lat -= 180;
        plotfile << "set view " << lat << "," << lng << "\n";
        plotfile << "set parametric\n";
        plotfile << "set isosamples 25\n";
        plotfile << "set urange[0:360]\n";
        plotfile << "set vrange[-90:90]\n";
        plotfile << "set xrange[-1:1]\n";
        plotfile << "set yrange[-1:1]\n";
        plotfile << "set zrange[-1:1]\n";
        plotfile << "set palette defined (-1 \"grey\",0 \"blue\"," \
            "17 \"#00ffff\",33 \"white\",50 \"yellow\",66 \"red\", " \
            "100 \"#990000\",101 \"grey\")\n";
        plotfile << "r = 0.99\n";
        plotfile << "set hidden3d front\n";
        plotfile << "splot \"" << plot_data_filename <<
            "\" u 2:1:(1):3 w pm3d, r*cos(v)*cos(u),r*cos(v)*sin(u),r*sin(v)" \
            " w l ls 2, \"" << world_data_filename <<
            "\" u 1:2:(1) w l ls 1\n";
    }

    plotfile.close();

    printf("done\n");

    string command_str = "gnuplot " + plot_script_filename;
    return system(command_str.c_str());
}


void gnuplot::graph_averages(vector<gridcell> &grid, string filename,
                             string title, string subtitle,
                             float subtitle_indent,
                             int start_year, int end_year, int month,
                             bool show_running_average,
                             int image_width, int image_height,
                             float pressure_step, float max_pressure,
                             vector<float> &area,
                             int plot_change)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        title = "Argo Annual Temperatures";
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
        title = utils::append_area_to_title(title, area);
    }

    gnuplot::plot_averages(
                           plot_script_filename,
                           plot_data_filename,
                           filename,
                           title,
                           subtitle, subtitle_indent,
                           grid,
                           start_year,
                           end_year,
                           show_running_average,
                           image_width,
                           image_height,
                           pressure_step,
                           max_pressure,
                           plot_change);
}

void gnuplot::graph_active_platforms(vector<argodata> &data,
                                     string filename,
                                     string title, string subtitle,
                                     float subtitle_indent,
                                     int start_year, int end_year,
                                     int image_width, int image_height,
                                     vector<float> &area)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        title = "Argo Active Platforms";
        title = utils::append_time_to_title(title, start_year,
                                            end_year, -1);
        title = utils::append_area_to_title(title, area);
    }

    gnuplot::plot_active_platforms(
                                   plot_script_filename,
                                   plot_data_filename,
                                   filename,
                                   title, subtitle, subtitle_indent,
                                   data,
                                   start_year, end_year,
                                   image_width,
                                   image_height);
}

void gnuplot::graph_data_centres(vector<argodata> &data, string filename,
                                 string title, string subtitle,
                                 float subtitle_indent,
                                 int start_year, int end_year,
                                 int image_width, int image_height,
                                 vector<float> &area)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        title = "Argo Data Centres";
        title = utils::append_time_to_title(title, start_year,
                                            end_year, -1);
        title = utils::append_area_to_title(title, area);
    }

    gnuplot::plot_data_centres(
                               plot_script_filename,
                               plot_data_filename,
                               filename,
                               title, subtitle, subtitle_indent,
                               data,
                               start_year, end_year,
                               image_width,
                               image_height);
}

void gnuplot::graph_profiles(vector<gridcell> &grid, string filename,
                             string title, string subtitle,
                             float subtitle_indent,
                             int start_year, int end_year, int month,
                             int image_width, int image_height,
                             float year_step, int graph_type,
                             float max_pressure,
                             vector<float> &area,
                             int plot_change)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            title = "Argo Temperature Profiles";
            break;
        case GRAPH_SALINITY:
            title = "Argo Salinity Profiles";
            break;
        case GRAPH_DENSITY:
            title = "Argo Density Profiles";
            break;
        case GRAPH_OXYGEN:
            title = "Argo Oxygen Profiles";
            break;
        case GRAPH_OHC:
            title = "Argo Ocean Heat Content Profiles";
            break;
        }
        if (plot_change != 0) {
            title += " relative to " + std::to_string(start_year);
        }
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
        title = utils::append_area_to_title(title, area);
    }

    gnuplot::plot_profile(
                          plot_script_filename,
                          plot_data_filename,
                          filename,
                          title,
                          subtitle, subtitle_indent,
                          grid,
                          start_year,
                          end_year,
                          image_width,
                          image_height,
                          year_step,
                          graph_type,
                          max_pressure,
                          plot_change);
}

void gnuplot::graph_anomalies(vector<gridcell> &grid, string filename,
                              string title, string subtitle,
                              float subtitle_indent,
                              int start_year, int end_year, int month,
                              int reference_start_year,
                              int reference_end_year,
                              bool show_minmax,
                              bool show_running_average,
                              bool best_fit_line,
                              int image_width, int image_height,
                              float pressure, vector<float> &area,
                              int graph_type)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        char titlestr[256];
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            sprintf(titlestr,"Argo Temperature Anomalies at %dm",
                    (int)pressure);
            break;
        case GRAPH_SALINITY:
            sprintf(titlestr,"Argo Salinity Anomalies at %dm",
                    (int)pressure);
            break;
        case GRAPH_DENSITY:
            sprintf(titlestr,"Argo Density Anomalies at %dm",
                    (int)pressure);
            break;
        case GRAPH_OXYGEN:
            sprintf(titlestr,"Argo Oxygen Anomalies at %dm",
                    (int)pressure);
            break;
        case GRAPH_OHC:
            sprintf(titlestr,"Argo Ocean Heat Content Anomalies at %dm",
                    (int)pressure);
            break;
        }
        title = string(titlestr);
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
        title = utils::append_area_to_title(title, area);
    }

    gnuplot::plot_anomalies(
                            plot_script_filename,
                            plot_data_filename,
                            filename,
                            title,
                            subtitle, subtitle_indent,
                            grid,
                            start_year,end_year,
                            reference_start_year,
                            reference_end_year,
                            show_minmax,
                            show_running_average,
                            best_fit_line,
                            image_width,
                            image_height,
                            pressure,
                            graph_type);
}

void gnuplot::graph_distribution(vector<gridcell> &grid, string filename,
                                 string title, string subtitle,
                                 float subtitle_indent,
                                 int start_year, int end_year, int month,
                                 int image_width, int image_height,
                                 float pressure, int year_step,
                                 vector<float> &area)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        title = "Argo Temperature Distribution";
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
        title = utils::append_area_to_title(title, area);
    }
    gnuplot::plot_distribution(
                               plot_script_filename,
                               plot_data_filename,
                               filename,
                               title,subtitle,subtitle_indent,
                               grid,
                               start_year,end_year,
                               year_step,
                               image_width,image_height,
                               pressure);
}

void gnuplot::graph_diurnal(vector<gridcell> &grid, string filename,
                            string title, string subtitle,
                            float subtitle_indent,
                            int start_year, int end_year, int month,
                            int image_width, int image_height,
                            float pressure, int year_step,
                            vector<float> &area, int graph_type)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        title = "Argo Diurnal";
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
        title = utils::append_area_to_title(title, area);
    }
    gnuplot::plot_diurnal(plot_script_filename,
                          plot_data_filename,
                          filename,
                          title,subtitle,subtitle_indent,
                          grid,
                          start_year,end_year,
                          year_step, month,
                          image_width,image_height,
                          pressure, graph_type);
}


void gnuplot::graph_map_averages(vector<gridcell> &grid, string filename,
                                 string title, string subtitle,
                                 float subtitle_indent,
                                 int start_year, int end_year,
                                 bool threed, int month,
                                 float view_longitude, float view_latitude,
                                 int image_width, int image_height,
                                 float pressure, vector<float> &area,
                                 int graph_type,
                                 float range_min, float range_max)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";
    string world_data_filename = "temp_world.txt";

    if (title=="") {
        char titlestr[256];
        char yearstr[100];
        string graph_type_str="";
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            graph_type_str="Temperatures";
            break;
        case GRAPH_SALINITY:
            graph_type_str="Salinity";
            break;
        case GRAPH_DENSITY:
            graph_type_str="Density";
            break;
        case GRAPH_OXYGEN:
            graph_type_str="Oxygen";
            break;
        case GRAPH_OHC:
            graph_type_str="Ocean Heat Content";
            break;
        }
        if (start_year==end_year) {
            sprintf(yearstr,"%d",start_year);
        }
        else {
            sprintf(yearstr,"%d-%d",start_year,end_year);
        }
        if (month>0) {
            sprintf(titlestr,"Argo Average %s for %d/%s at depth %dm",
                    graph_type_str.c_str(),month,yearstr,(int)pressure);
        }
        else {
            sprintf(titlestr,"Argo Average %s for %s at depth %dm",
                    graph_type_str.c_str(),yearstr,(int)pressure);
        }
        title = string(titlestr);
        title = utils::append_area_to_title(title, area);
    }
    gnuplot::plot_averages_world_map(
                                     plot_script_filename,
                                     plot_data_filename,
                                     world_data_filename,
                                     filename,
                                     title,
                                     subtitle,subtitle_indent,
                                     grid,
                                     start_year,end_year,
                                     threed,
                                     view_longitude, view_latitude,
                                     image_width,
                                     image_height,
                                     pressure,
                                     graph_type,
                                     range_min, range_max);
}


void gnuplot::graph_platforms(vector<int> &platform_numbers,
                              vector<gridcell> &grid, string filename,
                              string title, string subtitle,
                              float subtitle_indent,
                              int start_year, int end_year,
                              bool threed, int month,
                              float view_longitude, float view_latitude,
                              int image_width, int image_height,
                              vector<float> &area)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";
    string world_data_filename = "temp_world.txt";

    if (title=="") {
        char titlestr[256];
        char yearstr[100];
        char platformstr[20];
        string platform_str="Argo Platform";
        if (platform_numbers.size()>0) {
            platform_str += " ";
            for (unsigned int i = 0; i < platform_numbers.size(); i++) {
                sprintf(platformstr,"%d",platform_numbers[i]);
                platform_str += string(platformstr);
                if (i<platform_numbers.size()-1) {
                    platform_str += ", ";
                }
            }
        }
        if (start_year==end_year) {
            sprintf(yearstr,"%d",start_year);
        }
        else {
            sprintf(yearstr,"%d-%d",start_year,end_year);
        }
        if (month>0) {
            sprintf(titlestr,"%s Locations for %d/%s",
                    platform_str.c_str(),month,yearstr);
        }
        else {
            sprintf(titlestr,"%s Locations for %s",
                    platform_str.c_str(),yearstr);
        }
        title = string(titlestr);
        title = utils::append_area_to_title(title, area);
    }
    gnuplot::plot_platforms(
        plot_script_filename,
        plot_data_filename,
        world_data_filename,
        filename,
        title,subtitle,subtitle_indent,
        grid,
        start_year,end_year,
        threed,
        view_longitude, view_latitude,
        image_width,
        image_height);
}

void gnuplot::graph_cycles(vector<argodata> &data, string filename,
                           string title, string subtitle,
                           float subtitle_indent,
                           vector<int> &platforms,
                           int image_width, int image_height,
                           vector<float> &area,
                           float max_pressure,
                           int graph_type)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (platforms.size()>1) {
        printf("WARNING: You can only plot cycles for one " \
               "platform at a time\n");
        return;
    }
    if (platforms.size()==0) {
        printf("You must specify a platform number with the " \
               "--platform option to be able to plot cycles\n");
        return;
    }

    if (title=="") {
        char titlestr[256];
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            sprintf(titlestr,"Argo Platform %d Cycles (Temperature)",
                    platforms[0]);
            break;
        case GRAPH_SALINITY:
            sprintf(titlestr,"Argo Platform %d Cycles (Salinity)",
                    platforms[0]);
            break;
        case GRAPH_DENSITY:
            sprintf(titlestr,"Argo Platform %d Cycles (Density)",
                    platforms[0]);
            break;
        case GRAPH_OXYGEN:
            sprintf(titlestr,"Argo Platform %d Cycles (Oxygen)",
                    platforms[0]);
            break;
        case GRAPH_OHC:
            sprintf(titlestr,"Argo Platform %d Cycles (OHC)",
                    platforms[0]);
            break;
        }
        title = string(titlestr);
        title = utils::append_area_to_title(title, area);
    }
    gnuplot::plot_cycles(
                         plot_script_filename,
                         plot_data_filename,
                         filename,
                         title,
                         subtitle,subtitle_indent,
                         data,
                         image_width,
                         image_height,
                         max_pressure,
                         graph_type);
}

void gnuplot::graph_latitudes(vector<argodata> &data, string filename,
                              string title, string subtitle,
                              float subtitle_indent,
                              int image_width, int image_height,
                              vector<float> &area,
                              float max_pressure,
                              int graph_type,
                              int start_year, int end_year, int month,
                              float range_min, float range_max)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        char titlestr[256];
        string bylat = " by Latitude";
        if (area.size()==4) bylat="";
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            sprintf(titlestr,"Argo Average Temperatures%s",bylat.c_str());
            break;
        case GRAPH_SALINITY:
            sprintf(titlestr,"Argo Average Salinity%s",bylat.c_str());
            break;
        case GRAPH_DENSITY:
            sprintf(titlestr,"Argo Average Density%s",bylat.c_str());
            break;
        case GRAPH_OXYGEN:
            sprintf(titlestr,"Argo Average Oxygen%s",bylat.c_str());
            break;
        case GRAPH_OHC:
            sprintf(titlestr,"Argo Average Ocean Heat Content%s",bylat.c_str());
            break;
        }
        title = string(titlestr);
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
        title = utils::append_area_to_title(title, area);
    }
    gnuplot::plot_latitudes(
                            plot_script_filename,
                            plot_data_filename,
                            filename,
                            title,subtitle,subtitle_indent,
                            data, area,
                            image_width,image_height,
                            max_pressure, graph_type,
                            range_min, range_max,
                            start_year, end_year, month);
}

void gnuplot::graph_latitude_anomalies(vector<argodata> &data,
                                       string filename,
                                       string title, string subtitle,
                                       float subtitle_indent,
                                       int image_width, int image_height,
                                       vector<float> &area,
                                       float max_pressure,
                                       int graph_type,
                                       int start_year, int end_year,
                                       int reference_start_year,
                                       int reference_end_year,
                                       float range_min, float range_max)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";

    if (title=="") {
        char titlestr[256];
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            title = "Argo Temperature Anomalies";
            break;
        case GRAPH_SALINITY:
            title = "Argo Salinity Anomalies";
            break;
        case GRAPH_DENSITY:
            title = "Argo Density Anomalies";
            break;
        case GRAPH_OXYGEN:
            title = "Argo Oxygen Anomalies";
            break;
        case GRAPH_OHC:
            title = "Argo Ocean Heat Content Anomalies";
            break;
        }
        title = utils::append_time_to_title(title, start_year,
                                            end_year, -1);

        sprintf(titlestr," Relative to %d-%d",reference_start_year,
                reference_end_year);
        title += string(titlestr);
    }
    gnuplot::plot_latitude_anomalies(
                                     plot_script_filename,
                                     plot_data_filename,
                                     filename,
                                     title, subtitle, subtitle_indent,
                                     data, area,
                                     image_width, image_height,
                                     max_pressure,
                                     graph_type,
                                     start_year, end_year,
                                     reference_start_year,
                                     reference_end_year,
                                     range_min, range_max);
}

void gnuplot::graph_map_anomalies(vector<gridcell> &grid, string filename,
                                  string title, string subtitle,
                                  float subtitle_indent,
                                  int image_width, int image_height,
                                  float pressure,
                                  int graph_type,
                                  int year, bool threed,
                                  float view_longitude, float view_latitude,
                                  int reference_start_year,
                                  int reference_end_year,
                                  float range_min, float range_max)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";
    string world_data_filename = "temp_world.txt";

    if (title=="") {
        char titlestr[256];
        switch(graph_type) {
        case GRAPH_TEMPERATURE:
            title = "Argo Temperature Anomalies";
            break;
        case GRAPH_SALINITY:
            title = "Argo Salinity Anomalies";
            break;
        case GRAPH_DENSITY:
            title = "Argo Density Anomalies";
            break;
        case GRAPH_OXYGEN:
            title = "Argo Oxygen Anomalies";
            break;
        case GRAPH_OHC:
            title = "Argo Ocean Heat Content Anomalies";
            break;
        }
        title = utils::append_time_to_title(title, year, year, -1);

        sprintf(titlestr," at %dm Relative to %d-%d",
                (int)pressure,reference_start_year,reference_end_year);
        title += string(titlestr);
    }
    gnuplot::plot_anomalies_world_map(
                                      plot_script_filename,
                                      plot_data_filename,
                                      world_data_filename,
                                      filename,
                                      title,subtitle,subtitle_indent,
                                      grid, year,
                                      reference_start_year,
                                      reference_end_year,
                                      threed,
                                      view_longitude, view_latitude,
                                      false,
                                      image_width,image_height,
                                      pressure,
                                      graph_type,
                                      range_min, range_max);
}

void gnuplot::graph_currents(vector<argodata> &data, string filename,
                             string title, string subtitle,
                             float subtitle_indent,
                             bool threed,
                             int start_year, int end_year,
                             float view_longitude, float view_latitude,
                             int image_width, int image_height,
                             int graph_type,
                             float range_min, float range_max)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";
    string world_data_filename = "temp_world.txt";

    if (title=="") {
        switch(graph_type) {
        case GRAPH_DIRECTION:
            title = "Argo Direction";
            break;
        case GRAPH_TURBULENCE:
            title = "Argo Turbulence";
            break;
        }
        title = utils::append_time_to_title(title, start_year,
                                            end_year, -1);
    }
    gnuplot::plot_currents(
                           plot_script_filename,
                           plot_data_filename,
                           world_data_filename,
                           filename,
                           title,
                           subtitle,subtitle_indent,
                           data,
                           threed,
                           view_longitude, view_latitude,
                           image_width,
                           image_height,
                           graph_type,
                           range_min, range_max);
}

void gnuplot::graph_speeds(vector<argodata> &data, string filename,
                           string title, string subtitle,
                           float subtitle_indent,
                           bool threed,
                           int start_year, int end_year, int month,
                           float view_longitude, float view_latitude,
                           int image_width, int image_height,
                           float range_min, float range_max)
{
    string plot_script_filename = "temp_script.txt";
    string plot_data_filename = "temp_data.txt";
    string world_data_filename = "temp_world.txt";

    if (title=="") {
        title = "Argo Average Platform Speed (m/s)";
        title = utils::append_time_to_title(title, start_year,
                                            end_year, month);
    }
    gnuplot::plot_speeds(
                         plot_script_filename,
                         plot_data_filename,
                         world_data_filename,
                         filename,
                         title,
                         subtitle,subtitle_indent,
                         data,
                         threed,
                         view_longitude, view_latitude,
                         image_width,
                         image_height,
                         range_min, range_max);
}
