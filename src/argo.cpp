/*
  Interface for Argo observations
  Copyright (C) 2014 Bob Mottram
  bob@freedombone.net

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "argo.h"

argo::argo() {
}

argo::~argo() {
}

// Returns whether a file exists
bool argo::file_exists(string filename)
{
    FILE * fp = fopen(filename.c_str(),"r");
    if (fp!=NULL) {
        fclose(fp);
        return true;
    }
    return false;
}

// Convert a string to lower case
void argo::string_to_lower(char * str, char * result)
{
    int i = 0;
    while (str[i]!=0) {
        result[i] = tolower(str[i]);
        i++;
    }
    result[i]=0;
}

// Removes any preceding spaces from the given string
void argo::remove_preceding_spaces(char * str)
{
    int i=0;
    while (i < (int)strlen(str)) {
        if (((str[i]>='a') && (str[i]<='z')) ||
            ((str[i]>='A') && (str[i]<='Z'))) {
            break;
        }
        i++;
    }

    if (i < (int)strlen(str)) {
        int diff = i;
        while (i < (int)strlen(str)) {
            str[i-diff] = tolower(str[i]);
            i++;
        }
        str[i-diff]=0;
    }
}

// Extract a series of float values from the given string
bool argo::extract_series(char * target, char * str, int &records,
                          float * result, int max_records, FILE * fp)
{
    int i,n = 0;
    char numberstr[256];

    records=0;
    if (strncmp(target,str,strlen(target))==0) {
        i = strlen(target);
        while (!feof(fp)) {

            if ((str[i]==',') || (str[i]==';')) {
                numberstr[n]=0;
                if (records<max_records) {
                    result[records++] = atof(numberstr);
                }
                n=0;
                if (str[i]==';') {
                    break;
                }
            }
            else {
                if (((str[i]>='0') && (str[i]<='9')) || (str[i]=='.')) {
                    numberstr[n++] = str[i];
                }
            }

            i++;
            if (i == (int)strlen(str)) {
                if (fgets(str , 255 , fp) == NULL ) {
                    break;
                }
                i=0;
            }
        }
    }
    return records>0;
}

// Only allow valid characters through
// This avoids having any unexpected characters within returned parameters
bool argo::valid_character(char c)
{
    if (((c>='0') && (c <= '9')) ||
        ((c>='A') && (c <= 'Z')) ||
        ((c>='a') && (c <= 'z')) ||
        (c=='.') || (c==' ') || (c=='-')) {
        return true;
    }
    return false;
}

// Extract a string parameter from the given string
bool argo::extract_parameter(char * target, char * str, char * result,
                             int max_result_length, FILE * fp)
{
    unsigned int ctr = 0;
    if (strncmp(target,str,strlen(target))==0) {
        int start = -1;
        unsigned int n = strlen(target);
        bool eol_found=false;
        while ((!eol_found) && (!feof(fp))) {
            for (unsigned int i = n; i < strlen(str); i++) {
                if (str[i]==';') {
                    eol_found = true;
                    break;
                }
                if (str[i] == '"') {
                    if (start == -1) {
                        start = i;
                    }
                    else {
                        eol_found = true;
                        break;
                    }
                }
                else {
                    if (start != -1) {
                        if (valid_character(str[i])) {
                            if (ctr < (unsigned int)max_result_length) {
                                result[ctr++] = tolower(str[i]);
                            }
                        }
                    }
                }
            }
            if (!eol_found) {
                if (fgets(str , 255 , fp)!=NULL) {
                    n=0;
                }
            }
        }

        // No quotes
        if ((ctr==0) && (eol_found)) {
            for (unsigned int i = n; i < strlen(str); i++) {
                if (str[i]==';') {
                    break;
                }
                if (valid_character(str[i])) {
                    if (ctr < (unsigned int)max_result_length) {
                        result[ctr++] = tolower(str[i]);
                    }
                }
            }
        }

    }
    result[ctr]=0;

    if (ctr > 0) {
        while (result[ctr-1]==' ') {
            ctr--;
            result[ctr] = 0;
            if (ctr==0) break;
        }
    }
    if (ctr>0) return true;
    return false;
}

// Extract a date and time from the given string
bool argo::extract_parameter_date_time(char * target, char * str,
                                       int & year, int & month, int & day,
                                       int & hour, int & min,
                                       FILE *fp)
{
    char result[256],str2[5];
    if (extract_parameter(target, str, result, 256, fp)) {
        if (strlen(result)==14) {
            int i=0,n=0;
            while (n<4) {
                str2[n++]=result[i++];
            }
            str2[n]=0;
            year = atoi(str2);

            n=0;
            while (n<2) {
                str2[n++]=result[i++];
            }
            str2[n]=0;
            month = atoi(str2);

            n=0;
            while (n<2) {
                str2[n++]=result[i++];
            }
            str2[n]=0;
            day = atoi(str2);

            n=0;
            while (n<2) {
                str2[n++]=result[i++];
            }
            str2[n]=0;
            hour = atoi(str2);

            n=0;
            while (n<2) {
                str2[n++]=result[i++];
            }
            str2[n]=0;
            min = atoi(str2);

            return true;
        }
    }
    return false;
}

// return an integer parameter from the given string
bool argo::extract_parameter_integer(char * target, char * str,
                                     int & value, FILE * fp)
{
    char result[256];
    if (extract_parameter(target, str, result, 256, fp)) {
        value = atoi(result);
        return true;
    }
    return false;
}

// return a float parameter from the given string
bool argo::extract_parameter_float(char * target, char * str,
                                   float & value, FILE * fp)
{
    char result[256];
    if (extract_parameter(target, str, result, 256, fp)) {
        value = atof(result);
        return true;
    }
    return false;
}

// recursive directory search
int argo::walk_recur(string dname, regex_t *reg, int spec,
                     vector<string> &filenames)
{
    struct dirent *dent;
    DIR *dir;
    struct stat st;
    char fn[FILENAME_MAX];
    int res = WALK_OK;
    int len = dname.length();
    if (len >= FILENAME_MAX - 1)
        return WALK_NAMETOOLONG;

    strcpy(fn, (char*)dname.c_str());
    fn[len++] = '/';

    if (!(dir = opendir((char*)dname.c_str()))) {
        return WALK_BADIO;
    }

    errno = 0;
    while ((dent = readdir(dir))) {
        if (!(spec & WS_DOTFILES) && dent->d_name[0] == '.')
            continue;
        if (!strcmp(dent->d_name, ".") || !strcmp(dent->d_name, ".."))
            continue;

        strncpy(fn + len, dent->d_name, FILENAME_MAX - len);
        if (lstat(fn, &st) == -1) {
            warn("Can't stat %s", fn);
            res = WALK_BADIO;
            continue;
        }

        /* don't follow symlink unless told so */
        if (S_ISLNK(st.st_mode) && !(spec & WS_FOLLOWLINK))
            continue;

        /* will be false for symlinked dirs */
        if (S_ISDIR(st.st_mode)) {
            /* recursively follow dirs */
            if ((spec & WS_RECURSIVE))
                walk_recur(fn, reg, spec, filenames);

            if (!(spec & WS_MATCHDIRS)) continue;
        }
        else {
            /* pattern match */
            /*if (!regexec(reg, fn, 0, 0, 0)) {*/
            filenames.push_back(string(fn));
            /*}*/
        }
    }

    if (dir) closedir(dir);
    return res ? res : errno ? WALK_BADIO : WALK_OK;
}

// traverse subdirectories
int argo::walk_dir(string dname, char *pattern, int spec,
                   vector<string> &filenames)
{
    regex_t r;
    int res;

    if (regcomp(&r, pattern, REG_EXTENDED | REG_NOSUB))
        return WALK_BADPATTERN;
    res = walk_recur(dname, &r, spec, filenames);
    regfree(&r);

    return res;
}

// is the platform within the given area?
bool argo::inside_area(argodata &data, vector<float> &area)
{
    float north = data.latitude;
    float west = -data.longitude;
    float latitudeN1 = area[0];
    float longitudeW1 = area[1];
    float latitudeN2 = area[2];
    float longitudeW2 = area[3];

    if (latitudeN2 < latitudeN1) {
        float temp = latitudeN1;
        latitudeN1 = latitudeN2;
        latitudeN2 = temp;
    }

    if ((longitudeW1 == 0.0f) &&
        (longitudeW2 == 0.0f)) {
        // between two latitudes
        if ((north >= latitudeN1) &&
            (north <= latitudeN2)) {
            return true;
        }
    }
    else {
        // within an area
        if (longitudeW2 < longitudeW1) {
            float temp = longitudeW1;
            longitudeW1 = longitudeW2;
            longitudeW2 = temp;
        }

        float w = longitudeW2 - longitudeW1;
        if (w > 180) w = 360 - w;
        if (w < -180) w = -360 + w;
        float h = latitudeN2 - latitudeN1;
        if (h > 180) h = 360 - h;
        if (h < -180) h = -360 + h;
        float cx = longitudeW1 + (w/2);
        float cy = latitudeN1 + (h/2);
        float r1 = w/2;
        float r2 = h/2;

        float dy = north - cy;
        if (dy > 180) dy = 360 - dy;
        if (dy < -180) dy = -360 + dy;
        if (fabs(dy) < r2) {
            float dx = west - cx;
            if (dx > 180) dy = 360 - dx;
            if (dx < -180) dy = -360 + dx;
            if (fabs(dx) < r1) {
                return true;
            }
        }
    }
    return false;
}

// Does the given filename match the pattern
// Single letter followed by numbers or underscores, with .nc extension
bool argo::valid_netcdf_profile(string filename)
{
    const char * ch = filename.c_str();
    if (strlen(ch)>4) {
        if ((ch[strlen(ch)-3]=='.') &&
            (ch[strlen(ch)-2]=='n') &&
            (ch[strlen(ch)-1]=='c')) {

            int pos = strlen(ch)-4;
            while ((pos>1) && (ch[pos]!='/') && (ch[pos]!='\\')) pos--;
            if ((ch[pos]!='/') || (ch[pos]!='\\')) pos++;

            if ((ch[pos]<'A') || (ch[pos]>'Z')) return false;

            for (int i = pos+1; i < (int)strlen(ch)-3; i++) {
                if (!(((ch[i]>='0') && (ch[i]<='9')) ||
                      (ch[i]=='_'))) {
                    return false;
                }
            }

            return true;
        }
    }
    return false;
}

// Loads all .nc files within a directory
int argo::load_netcdf_directory(string directory, vector<argodata> &data,
                                bool verbose,
                                int start_year, int end_year, int month,
                                vector<float> &area, vector<int> &platforms,
                                string temporary_filename,
                                string export_filename)
{
    vector<string> filenames;
    vector<string> valid_filenames;
    FILE * fp;
    int retval = walk_dir(directory, (char*)".\\.$", WS_DEFAULT|WS_MATCHDIRS,filenames);

    switch(retval) {

    case WALK_BADIO: {

        fprintf(stderr,"Directory '%s' Error %d: %s\n",
                directory.c_str(),errno,strerror(errno));

        return -1;

    }

    case WALK_NAMETOOLONG: {

        fprintf(stderr,"Directory name '%s' is too long\n", directory.c_str());

        return -1;

    }

    case WALK_BADPATTERN: {

        fprintf(stderr,"Bad directory '%s'\n", directory.c_str());

        return -1;

    }

    }

    if ((retval==WALK_OK) && (filenames.size()==0)) {

        /* directory exists but contains no files */

        fprintf(stderr,"No files found within the directory '%s'\n",
                directory.c_str());

        return -1;
    }

    // get a list of valid filenames
    for (unsigned int i=0; i < filenames.size(); i++) {
        if (valid_netcdf_profile(filenames[i])) {
            valid_filenames.push_back(filenames[i]);
        }
    }

    FILE * fp_export = NULL;
    if (export_filename!="") {
        fp_export = fopen(export_filename.c_str(),"wb");
    }

    // import data from files
    printf("Loading profiles\n");
    unsigned int progress=0;
    for (unsigned int i=0; i < valid_filenames.size(); i++) {
        load_netcdf(valid_filenames[i], data, verbose,
                    start_year, end_year, month,
                    area, platforms,
                    temporary_filename,
                    fp_export);
        unsigned int position = i*CONSOLE_COLUMNS/valid_filenames.size();
        while (progress<position) {
            printf("=");
            progress++;
        }
        flush(cout);
    }
    printf("\n");

    if (fp_export!=NULL) {
        fclose(fp_export);
    }

    // delete the temporary file
    fp = fopen((char*)temporary_filename.c_str(),"r");
    if (fp!=NULL) {
        fclose(fp);
        char commandstr[256];
        sprintf(commandstr,"rm %s", temporary_filename.c_str());
        if (system(commandstr)!=0) {
            return -1;
        }
    }

    return 0;
}

// Returns the range of cycle numbers within the given data
int argo::cycle_range(vector<argodata> &data, int &min_cycle_number,
                      int &max_cycle_number)
{
    if (data.size()>0) {
        min_cycle_number = data[0].cycle_number;
        max_cycle_number = min_cycle_number;
        for (unsigned int i=1; i<data.size(); i++) {
            if (data[i].cycle_number<min_cycle_number) {
                min_cycle_number = data[i].cycle_number;
            }
            if (data[i].cycle_number>max_cycle_number) {
                max_cycle_number = data[i].cycle_number;
            }
        }
        return 0;
    }
    return -1;
}

// return the minimum and maximum year/month
// returns the total number of months
int argo::date_range(vector<argodata> &data, int &min_year,
                     int &min_month, int &max_year, int &max_month)
{
    if (data.size()>0) {
        min_year = data[0].year;
        min_month = data[0].month;
        max_year=min_year;
        max_month = min_month;
        // year
        for (unsigned int i=1; i<data.size(); i++) {
            if (data[i].year<min_year) {
                min_year = data[i].year;
                min_month = data[i].month;
            }
            if (data[i].year>max_year) {
                max_year = data[i].year;
                max_month = data[i].month;
            }
        }
        // month
        for (unsigned int i=1; i<data.size(); i++) {
            if ((data[i].year==min_year) &&
                (data[i].month < min_month)) {
                min_month = data[i].month;
            }
            if ((data[i].year==max_year) &&
                (data[i].month > max_month)) {
                max_month = data[i].month;
            }
        }

        if (min_year==max_year) {
            return max_month - min_month;
        }
        else {
            return (12-min_month) + ((max_year-min_year-1)*12) + max_month;
        }
    }
    min_year=-9999;
    max_year = min_year;
    return -1;
}

// Returns an interpolated measurement at a given depth (pressure)
float argo::value_at_pressure(float pressure, argodata &data,
                              int measurement_type)
{
    float diff, interpolated;
    float * array = &data.temperature[0];

    // get the appropriate data array
    switch(measurement_type) {
    case MEASUREMENT_TEMPERATURE: {
        array = &data.temperature[0];
        break;
    }
    case MEASUREMENT_SALINITY: {
        array = &data.salinity[0];
        break;
    }
    case MEASUREMENT_OXYGEN: {
        array = &data.oxygen[0];
        break;
    }
    }

    for (unsigned int p = 1; p < (unsigned int)data.records; p++) {
        if ((pressure>=data.pressure[p-1]) && (pressure<data.pressure[p])) {
            // interpolate the temperature
            if (data.pressure[p]>data.pressure[p-1]) {
                float fraction =
                    (pressure - data.pressure[p-1])/(data.pressure[p] -
                                                     data.pressure[p-1]);
                diff = array[p] - array[p-1];
                interpolated = array[p-1] + (diff*fraction);
                if ((measurement_type == MEASUREMENT_TEMPERATURE) &&
                    (interpolated > 50)) {
                    return -9999.0f;
                }
                return interpolated;
            }
            break;
        }
    }
    return -9999.0f;
}

// return the interpolated salinity at a given pressure within a single profile
float argo::salinity_at_pressure(float pressure, argodata &data)
{
    return value_at_pressure(pressure, data, MEASUREMENT_SALINITY);
}

// return the interpolated oxygen at a given pressure within a single profile
float argo::oxygen_at_pressure(float pressure, argodata &data)
{
    if (data.oxygen[0]!=-9999.0f) {
        return value_at_pressure(pressure, data, MEASUREMENT_OXYGEN);
    }
    return -9999.0f;
}

// return the interpolated temperature at a given pressure within a single profile
float argo::temperature_at_pressure(float pressure, argodata &data)
{
    return value_at_pressure(pressure, data, MEASUREMENT_TEMPERATURE);
}

float argo::measurement_at_pressure(float pressure, vector<argodata> &data,
                                    int measurement_type)
{
    int hits = 0;
    float average = 0;

    for (unsigned int i = 0; i < data.size(); i++) {
        float interpolated = -9999.0f;
        switch(measurement_type) {
        case MEASUREMENT_TEMPERATURE: {
            interpolated = temperature_at_pressure(pressure,data[i]);
            break;
        }
        case MEASUREMENT_SALINITY: {
            interpolated = salinity_at_pressure(pressure,data[i]);
            break;
        }
        case MEASUREMENT_OXYGEN: {
            interpolated = oxygen_at_pressure(pressure,data[i]);
            break;
        }
        }
        if (interpolated>-9998.0f) {
            average += interpolated;
            hits++;
        }
    }
    if (hits>0) {
        return average / hits;
    }
    return -9999.0f;
}

// returns the interpolated salinity at a given pressure value
float argo::salinity_at_pressure(float pressure, vector<argodata> &data)
{
    return measurement_at_pressure(pressure, data,
                                   MEASUREMENT_SALINITY);
}

// returns the interpolated oxygen at a given pressure value
float argo::oxygen_at_pressure(float pressure, vector<argodata> &data)
{
    return measurement_at_pressure(pressure, data,
                                   MEASUREMENT_OXYGEN);
}

// returns the interpolated salinity at a given pressure value
float argo::temperature_at_pressure(float pressure, vector<argodata> &data)
{
    return measurement_at_pressure(pressure, data,
                                   MEASUREMENT_TEMPERATURE);
}

void argo::latitude_anomalies_subprocess(vector<argodata> &data,
                                         vector<vector<vector<float> > > &anomalies,
                                         unsigned int pressure_steps,
                                         float max_pressure,
                                         int start_year,
                                         int end_year,
                                         int reference_start_year,
                                         int reference_end_year,
                                         vector<vector<vector<vector<float> > > > &model,
                                         unsigned int data_start,
                                         unsigned int data_end)
{
    // create anomalies array
    anomalies.clear();
    for (unsigned int i = 0; i < 180; i++) {
        vector<vector<float> > l1;
        for (unsigned int p = 0; p < pressure_steps; p++) {
            vector<float> l2;
            for (unsigned int j = 0; j < 8; j++) {
                l2.push_back(0);
            }
            l1.push_back(l2);
        }
        anomalies.push_back(l1);
    }

    // populate the anomalies
    for (unsigned int i = data_start; i < data_end; i++) {
        if ((data[i].year >= start_year) &&
            (data[i].year <= end_year)) {
            int latitude_index = (int)(data[i].latitude+90.5f);
            if (latitude_index<0) latitude_index=0;
            if (latitude_index>=180) latitude_index=179;
            for (unsigned int p = 0; p < pressure_steps; p++) {
                float pressure = p*max_pressure/pressure_steps;
                float temp = temperature_at_pressure(pressure,data[i]);
                float sal = salinity_at_pressure(pressure,data[i]);
                float oxy = oxygen_at_pressure(pressure,data[i]);
                int m = data[i].month-1;
                if ((temp>-9999.0f) && (model[latitude_index][p][m][1]>0)) {
                    anomalies[latitude_index][p][0] +=
                        temp - model[latitude_index][p][m][0];
                    anomalies[latitude_index][p][1]++;
                }
                if ((sal>-9999.0f) && (model[latitude_index][p][m][3]>0)) {
                    anomalies[latitude_index][p][2] +=
                        sal - model[latitude_index][p][m][2];
                    anomalies[latitude_index][p][3]++;
                }
                if (((temp>-9999.0f) && (model[latitude_index][p][m][1]>0)) &&
                    ((sal>-9999.0f) && (model[latitude_index][p][m][3]>0))) {
                    anomalies[latitude_index][p][4] +=
                        density(temp,sal) - model[latitude_index][p][m][4];
                    anomalies[latitude_index][p][5]++;
                }
                if ((oxy>-9999.0f) && (model[latitude_index][p][m][7]>0)) {
                    anomalies[latitude_index][p][6] +=
                        oxy - model[latitude_index][p][m][6];
                    anomalies[latitude_index][p][7]++;
                }
            }
        }
    }

    // calculate the average anomalies
    for (unsigned int i = 0; i < 180; i++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            for (unsigned int j = 0; j < 8; j += 2) {
                if (anomalies[i][p][j+1] > 0) {
                    anomalies[i][p][j] /= anomalies[i][p][j+1];
                }
            }
        }
    }
}

void argo::latitude_anomalies(vector<argodata> &data,
                              vector<vector<vector<float> > > &anomalies,
                              unsigned int pressure_steps,
                              float max_pressure,
                              int start_year,
                              int end_year,
                              int reference_start_year,
                              int reference_end_year)
{
    vector<vector<vector<vector<float> > > > model;
    latitude_model(data, model,
                   pressure_steps,
                   max_pressure,
                   reference_start_year,
                   reference_end_year);

    // create anomalies array
    anomalies.clear();
    for (unsigned int i = 0; i < 180; i++) {
        vector<vector<float> > l1;
        for (unsigned int p = 0; p < pressure_steps; p++) {
            vector<float> l2;
            for (unsigned int j = 0; j < 8; j++) {
                l2.push_back(0);
            }
            l1.push_back(l2);
        }
        anomalies.push_back(l1);
    }

    // populate the anomalies
    vector<vector<vector<float> > > subprocess_anomalies[ARGO_CPU_CORES];
#pragma omp parallel for
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        unsigned int data_start = i * data.size() / ARGO_CPU_CORES;
        unsigned int data_end = (i+1) * data.size() / ARGO_CPU_CORES;
        latitude_anomalies_subprocess(data, subprocess_anomalies[i],
                                      pressure_steps,
                                      max_pressure,
                                      start_year, end_year,
                                      reference_start_year,
                                      reference_end_year,
                                      model, data_start, data_end);
    }

    // sum the results
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        for (unsigned int k = 0; k < 180; k++) {
            for (unsigned int p = 0; p < pressure_steps; p++) {
                for (unsigned int j = 0; j < 8; j += 2) {
                    anomalies[k][p][j] +=
                        subprocess_anomalies[i][k][p][j];
                    anomalies[k][p][j+1] +=
                        subprocess_anomalies[i][k][p][j+1];
                }
            }
        }
    }

    // divide by the number of cores
    for (unsigned int k = 0; k < 180; k++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            for (unsigned int j = 0; j < 8; j += 2) {
                anomalies[k][p][j] /= ARGO_CPU_CORES;
                anomalies[k][p][j+1] /= ARGO_CPU_CORES;
            }
        }
    }
}

void argo::latitude_model_subprocess(vector<argodata> &data,
                                     vector<vector<vector<vector<float> > > > &model,
                                     unsigned int pressure_steps,
                                     float max_pressure,
                                     int reference_start_year,
                                     int reference_end_year,
                                     unsigned int data_start,
                                     unsigned int data_end)
{
    model.clear();
    for (unsigned int i = 0; i < 180; i++) { // latitude
        vector<vector<vector<float> > > l1;
        for (unsigned int p = 0; p < pressure_steps; p++) { // pressure
            vector<vector<float> > l2;
            for (unsigned int m = 0; m < 12; m++) { // month
                vector<float> l3;
                for (unsigned int j = 0; j < 8; j++) {
                    l3.push_back(0);
                }
                l2.push_back(l3);
            }
            l1.push_back(l2);
        }
        model.push_back(l1);
    }

    for (unsigned int i = data_start; i < data_end; i++) {
        if ((data[i].year >= reference_start_year) &&
            (data[i].year <= reference_end_year)) {
            for (unsigned int p = 0; p < pressure_steps; p++) {
                float pressure  = p * max_pressure / pressure_steps;
                float temperature = temperature_at_pressure(pressure, data[i]);
                float salinity = salinity_at_pressure(pressure, data[i]);
                float oxygen = oxygen_at_pressure(pressure, data[i]);
                int lat = (int)(data[i].latitude + 90.5f);
                int m = data[i].month-1;
                if (lat < 0) lat = 0;
                if (lat >= 180) lat = 179;
                if (temperature > -9999.0f) {
                    model[lat][p][m][0] += temperature;
                    model[lat][p][m][1]++;
                }
                if (salinity > -9999.0f) {
                    model[lat][p][m][2] += salinity;
                    model[lat][p][m][3]++;
                }
                if ((temperature > -9999.0f) &&
                    (salinity > -9999.0f)) {
                    model[lat][p][m][4] += density(temperature,salinity);
                    model[lat][p][m][5]++;
                }
                if (oxygen > -9999.0f) {
                    model[lat][p][m][6] += oxygen;
                    model[lat][p][m][7]++;
                }
            }
        }
    }

    for (unsigned int i = 0; i < 180; i++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            for (unsigned int m = 0; m < 12; m++) {
                for (unsigned int j = 0; j < 8; j += 2) {
                    if (model[i][p][m][j+1] > 0) {
                        model[i][p][m][j] /= model[i][p][m][j+1];
                    }
                }
            }
        }
    }
}

// Creates a model which can be used to calculate anomalies for each latitude and depth
void argo::latitude_model(vector<argodata> &data,
                          vector<vector<vector<vector<float> > > > &model,
                          unsigned int pressure_steps,
                          float max_pressure,
                          int reference_start_year,
                          int reference_end_year)
{
    model.clear();
    for (unsigned int i = 0; i < 180; i++) { // latitude
        vector<vector<vector<float> > > l1;
        for (unsigned int p = 0; p < pressure_steps; p++) { // pressure
            vector<vector<float> > l2;
            for (unsigned int m = 0; m < 12; m++) { // month
                vector<float> l3;
                for (unsigned int j = 0; j < 8; j++) {
                    l3.push_back(0);
                }
                l2.push_back(l3);
            }
            l1.push_back(l2);
        }
        model.push_back(l1);
    }

    vector<vector<vector<vector<float> > > > subprocess_model[ARGO_CPU_CORES];
#pragma omp parallel for
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        unsigned int data_start = i * data.size() / ARGO_CPU_CORES;
        unsigned int data_end = (i+1) * data.size() / ARGO_CPU_CORES;
        latitude_model_subprocess(data, subprocess_model[i],
                                  pressure_steps,
                                  max_pressure,
                                  reference_start_year,
                                  reference_end_year,
                                  data_start, data_end);
    }

    // sum the results
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        for (unsigned int k = 0; k < 180; k++) {
            for (unsigned int p = 0; p < pressure_steps; p++) {
                for (unsigned int m = 0; m < 12; m++) {
                    for (unsigned int j = 0; j < 8; j += 2) {
                        model[k][p][m][j] +=
                            subprocess_model[i][k][p][m][j];
                    }
                }
            }
        }
    }

    // divide by the number of cores
    for (unsigned int k = 0; k < 180; k++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            for (unsigned int m = 0; m < 12; m++) {
                for (unsigned int j = 0; j < 8; j += 2) {
                    model[k][p][m][j] /= ARGO_CPU_CORES;
                }
            }
        }
    }
}

// Returns the total number of active platforms in the given year and month
int argo::active_platforms(vector<argodata> &data,
                           int year, int month)
{
    int n, prev_platform_number=-1;
    vector<int> platform_number;

    for (unsigned int i = 0; i < data.size(); i++) {
        if ((data[i].year == year) &&
            ((month == -1) || (data[i].month == month)) &&
            (data[i].platform_number != prev_platform_number)) {

            for (n = 0; n < (int)platform_number.size(); n++) {
                if (data[i].platform_number==platform_number[n]) {
                    break;
                }
            }
            if (n == (int)platform_number.size()) {
                platform_number.push_back(data[i].platform_number);
                prev_platform_number = data[i].platform_number;
            }
        }
    }
    return (int)platform_number.size();
}

void argo::get_data_centres(vector<argodata> &data,
                            int start_year, int end_year,
                            vector<string> &names,
                            vector<vector<int> > &qty)
{
    int month[100][12],n;

    // extract the names
    names.clear();
    qty.clear();
    for (unsigned int i = 0; i < data.size(); i++) {
        if ((data[i].year >= start_year) && (data[i].year <= end_year)) {
            for (n = 0; n < (int)names.size(); n++) {
                if (strcmp(names[n].c_str(), data[i].data_centre)==0) {
                    break;
                }
            }
            if (n == (int)names.size()) {
                if (names.size() < 100) {
                    names.push_back(string(data[i].data_centre));
                    vector<int> q;
                    qty.push_back(q);
                }
            }
        }
    }

    if (names.size()==0) return;

    for (unsigned int year = (unsigned int)start_year;
         year <= (unsigned int)end_year; year++) {
        for (n = 0; n < (int)names.size(); n++) {
            for (unsigned int m = 0; m < 12; m++) {
                month[n][m] = 0;
            }
        }
        for (unsigned int i = 0; i < data.size(); i++) {
            if ((unsigned int)data[i].year == year) {
                for (n = 0; n < (int)names.size(); n++) {
                    if (strcmp(data[i].data_centre,names[n].c_str())==0) {
                        break;
                    }
                }
                if (n < (int)names.size()) {
                    month[n][data[i].month-1]++;
                }
            }
        }
        for (n = 0; n < (int)names.size(); n++) {
            for (unsigned int m = 0; m < 12; m++) {
                qty[n].push_back(month[n][m]);
            }
        }
    }
}

string argo::get_data_centre(string data_centre)
{
    if (data_centre=="ao") {
        return "AOML, USA";
    }
    if (data_centre=="bo") {
        return "BODC, United Kingdom";
    }
    if (data_centre=="ci") {
        return "Institute of Ocean Sciences, Canada";
    }
    if (data_centre=="cs") {
        return "CSIRO, Australia";
    }
    if (data_centre=="ge") {
        return "BSH, Germany";
    }
    if (data_centre=="gt") {
        return "GTS : used for data coming from WMO GTS network";
    }
    if (data_centre=="hz") {
        return "CSIO, China Second Institute of Oceanography";
    }
    if (data_centre=="if") {
        return "Ifremer, France";
    }
    if (data_centre=="in") {
        return "INCOIS, India";
    }
    if (data_centre=="ja") {
        return "JMA, Japan";
    }
    if (data_centre=="jm") {
        return "Jamstec, Japan";
    }
    if (data_centre=="km") {
        return "KMA, Korea";
    }
    if (data_centre=="ko") {
        return "KORDI, Korea";
    }
    if (data_centre=="me") {
        return "MEDS, Canada";
    }
    return "Unknown";
}

string argo::get_instrument_type(int wmo_instrument_type)
{
    switch(wmo_instrument_type) {
    case 831:
        return "P-Alace float";
    case 840:
        return "Provor, no conductivity";
    case 841:
        return "Provor, Seabird conductivity sensor";
    case 842:
        return "Provor, FSI conductivity sensor";
    case 843:
        return "POPS ice Buoy/Float";
    case 844:
        return "Arvor, Seabird conductivity sensor";
    case 845:
        return "Webb Research, no conductivity";
    case 846:
        return "Webb Research, Seabird sensor";
    case 847:
        return "Webb Research, FSI sensor";
    case 850:
        return "Solo, no conductivity";
    case 851:
        return "Solo, Seabird conductivity sensor";
    case 852:
        return "Solo, FSI conductivity sensor";
    case 853:
        return "Solo2, Seabird conductivity sensor";
    case 855:
        return "Ninja, no conductivity sensor";
    case 856:
        return "Ninja, SBE conductivity sensor";
    case 857:
        return "Ninja, FSI conductivity sensor";
    case 858:
        return "Ninja, TSK conductivity sensor";
    case 859:
        return "Profiling Float, NEMO, no conductivity";
    case 860:
        return "Profiling Float, NEMO, SBE conductivity sensor";
    }
    return "Unknown";
}

void argo::update_latitudes_subprocess(vector<argodata> &data,
                                       vector<vector<vector<float> > > &latitudedata,
                                       unsigned int pressure_steps,
                                       float max_pressure,
                                       unsigned int data_start,
                                       unsigned int data_end,
                                       int start_year, int end_year, int month)
{
    // clear the latitudes
    latitudedata.clear();
    for (unsigned int i = 0; i < 180; i++) {
        vector<vector<float> > l1;
        for (unsigned int p = 0; p < pressure_steps; p++) {
            vector<float> l2;
            for (unsigned int j = 0; j < 8; j++) {
                l2.push_back(0);
            }
            l1.push_back(l2);
        }
        latitudedata.push_back(l1);
    }

    // populate the latitudes
    for (unsigned int i = data_start; i < data_end; i++) {
        if ((data[i].year < start_year) ||
            (data[i].year > end_year)) {
            continue;
        }
        if (!((month==-1) ||
              ((month > 0) && (data[i].month == month)))) {
            continue;
        }

        int latitude_index = (int)(data[i].latitude+90.5f);
        if (latitude_index<0) latitude_index=0;
        if (latitude_index>=180) latitude_index=179;
        for (unsigned int p = 0; p < pressure_steps; p++) {
            float pressure = p*max_pressure/pressure_steps;
            float temp = temperature_at_pressure(pressure,data[i]);
            float sal = salinity_at_pressure(pressure,data[i]);
            float oxy = oxygen_at_pressure(pressure,data[i]);
            if (temp>-9999.0f) {
                latitudedata[latitude_index][p][0] += temp;
                latitudedata[latitude_index][p][1]++;
            }
            if (sal>-9999.0f) {
                latitudedata[latitude_index][p][2] += sal;
                latitudedata[latitude_index][p][3]++;
            }
            if ((temp>-9999.0f) && (sal>-9999.0f)) {
                latitudedata[latitude_index][p][4] += density(temp,sal);
                latitudedata[latitude_index][p][5]++;
            }
            if (oxy>-9999.0f) {
                latitudedata[latitude_index][p][6] += oxy;
                latitudedata[latitude_index][p][7]++;
            }
        }
    }

    // calculate the averages
    for (unsigned int i = 0; i < 180; i++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            for (unsigned int j = 0; j < 8; j += 2) {
                if (latitudedata[i][p][j+1] > 0) {
                    latitudedata[i][p][j] /= latitudedata[i][p][j+1];
                }
            }
        }
    }
}

// populates an array containing temperatures and salinity at each
// pressure at each latitude
void argo::update_latitudes(vector<argodata> &data,
                            vector<vector<vector<float> > > &latitudedata,
                            unsigned int pressure_steps,
                            float max_pressure,
                            int start_year, int end_year, int month)
{
    // clear the latitudes
    latitudedata.clear();
    for (unsigned int i = 0; i < 180; i++) {
        vector<vector<float> > l1;
        for (unsigned int p = 0; p < pressure_steps; p++) {
            vector<float> l2;
            for (unsigned int j = 0; j < 8; j++) {
                l2.push_back(0);
            }
            l1.push_back(l2);
        }
        latitudedata.push_back(l1);
    }

    // populate the latitudes
    vector<vector<vector<float> > > subprocess_latitudedata[ARGO_CPU_CORES];
#pragma omp parallel for
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        unsigned int data_start = i * data.size() / ARGO_CPU_CORES;
        unsigned int data_end = (i+1) * data.size() / ARGO_CPU_CORES;
        update_latitudes_subprocess(data,
                                    subprocess_latitudedata[i],
                                    pressure_steps,
                                    max_pressure,
                                    data_start, data_end,
                                    start_year, end_year, month);
    }

    // sum the results
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        for (unsigned int k = 0; k < 180; k++) {
            for (unsigned int p = 0; p < pressure_steps; p++) {
                for (unsigned int j = 0; j < 8; j += 2) {
                    latitudedata[k][p][j] +=
                        subprocess_latitudedata[i][k][p][j];
                    latitudedata[k][p][j+1] +=
                        subprocess_latitudedata[i][k][p][j+1];
                }
            }
        }
    }

    // divide by the number of cores
    for (unsigned int k = 0; k < 180; k++) {
        for (unsigned int p = 0; p < pressure_steps; p++) {
            for (unsigned int j = 0; j < 8; j += 2) {
                latitudedata[k][p][j] /= ARGO_CPU_CORES;
                latitudedata[k][p][j+1] /= ARGO_CPU_CORES;
            }
        }
    }
}

// export the given profile to a file
void argo::export_to_file(FILE * fp, argodata &data)
{
    fwrite ((void*)&data , sizeof(argodata) , 1, fp );
}

// Load profiles from a binary file
int argo::load(string filename, vector<argodata> &data,
               int start_year, int end_year, int month,
               vector<float> &area, vector<int> &platforms)
{
    FILE * fp;

    data.clear();
    fp = fopen(filename.c_str(),"rb");
    if (fp!=NULL) {
        while (!feof(fp)) {
            argodata record;
            bool valid_record = true;
            if (fread((void*)&record, sizeof(argodata),1,fp)>0) {
                valid_record = false;
                if ((record.year>=start_year) &&
                    (record.year<=end_year) &&
                    ((month==-1) || ((month>0) && (record.month==month)))) {
                    valid_record = true;
                }
                if (record.records==0) valid_record = false;
                if ((valid_record) && (platforms.size()>0)) {
                    valid_record=false;
                    for (unsigned int i = 0; i < platforms.size(); i++) {
                        if (platforms[i]==record.platform_number) {
                            valid_record = true;
                            break;
                        }
                    }
                }
                if ((valid_record) && (area.size()==4)) {
                    valid_record = inside_area(record,area);
                }
                for (int i = 0; i < record.records; i++) {
                    if ((record.temperature[i] < 0.1f) ||
                        (record.temperature[i] > 40.0f)) {
                        valid_record = false;
                        break;
                    }
                    if ((record.salinity[i] < 1.0f) ||
                        (record.salinity[i] > 50.0f)) {
                        valid_record = false;
                        break;
                    }
                }

                if (valid_record) {
                    data.push_back(record);
                }
            }
        }
        fclose(fp);
        return 0;
    }
    return -1;
}

// Return the number of seconds between two dates
int argo::seconds_between_dates(
    int Y1, int M1, int D1, int H1, int m1, int S1,
    int Y2, int M2, int D2, int H2, int m2, int S2)
{
    time_t raw;
    time(&raw);

    struct tm t1 = *gmtime(&raw), t2 = t1;

    t1.tm_year = Y1 - 1900;
    t1.tm_mon = M1 - 1;
    t1.tm_mday = D1;
    t1.tm_hour = H1;
    t1.tm_min = m1;
    t1.tm_sec = S1;

    t2.tm_year = Y2 - 1900;
    t2.tm_mon = M2 - 1;
    t2.tm_mday = D2;
    t2.tm_hour = H2;
    t2.tm_min = m2;
    t2.tm_sec = S2;

    time_t tt1, tt2;
    tt1 = mktime(&t1);
    tt2 = mktime(&t2);

    if (tt2 > tt1) {
        return (int)(tt2 - tt1);
    }
    return (int)(tt1 - tt2);
}

// Sort profiles into order of cycle number
void argo::sort(vector<argodata> &data)
{
    int start = 0;
    while (start<(int)data.size()) {
        int platform_number = data[start].platform_number;
        int end = start;
        while ((data[end].platform_number == platform_number) &&
               (end<(int)data.size())) end++;

        for (int i = start; i < end; i++) {
            int min = i;
            for (int j = i+1; j < end; j++) {
                if (data[j].cycle_number < data[min].cycle_number) {
                    min = j;
                }
            }
            if (min!=i) {
                argodata temp = data[min];
                data[min] = data[i];
                data[i] = temp;
            }
        }
        start=end;
    }
}

// subprocess for calculating speeds
void argo::calculate_speeds_subprocess(vector<argodata> &data,
                                       unsigned int data_start,
                                       unsigned int data_end,
                                       vector<vector<float> > &speeds)
{
    // depth at which the platform spends most of its cycle time
    const double depth = 1000;
    const double radius_of_earth = 6378137;

    // create a 2D array
    int hits[360][180];
    speeds.clear();
    for (unsigned int lng = 0; lng < 360; lng++) {
        vector<float> v1;
        for (unsigned int lat = 0; lat < 180; lat++) {
            v1.push_back(0);
            hits[lng][lat]=0;
        }
        speeds.push_back(v1);
    }

    int current_platform_number=-1;
    float dist_metres;

    // Because platform profiles are in a sequential order
    // it's difficult to parallelise this

    for (unsigned int i = data_start; i < data_end; i++) {
        // same platform
        if (data[i].platform_number == current_platform_number) {
            // consecutive cycles
            if (data[i].cycle_number == data[i-1].cycle_number+1) {

                // calculate distance
                float A = data[i-1].latitude * 3.1415927f / 180.0f;
                float B = data[i-1].longitude * 3.1415927f / 180.0f;
                float C = data[i].latitude * 3.1415927f / 180.0f;
                float D = data[i].longitude * 3.1415927f / 180.0f;

                if ((A == C) && (B == D)) {
                    dist_metres = 0;
                }
                else {
                    if (sin(A)*sin(C) + cos(A)*cos(C)*cos(B-D) > 1.0f) {
                        dist_metres =
                            (float)((radius_of_earth-depth)*acos(1));
                    }
                    else {
                        dist_metres =
                            (float)((radius_of_earth-depth)*
                                    acos(sin(A)*sin(C) + cos(A)*cos(C)*cos(B-D)));
                    }
                }
                if (dist_metres < 0) dist_metres = -dist_metres;

                // calculate time
                int time_secs =
                    seconds_between_dates(
                                          data[i-1].year, data[i-1].month,
                                          data[i-1].day, data[i-1].hour,
                                          data[i-1].min, 0,
                                          data[i].year, data[i].month,
                                          data[i].day, data[i].hour,
                                          data[i].min, 0);
                if (time_secs>0) {
                    int lng2 = (int)(data[i-1].longitude+180.5f);
                    int lat2 = (int)(data[i-1].latitude+90.5f);
                    if (lng2<0) lng2=0;
                    if (lng2>=360) lng2 = 359;
                    if (lat2<0) lat2=0;
                    if (lat2>=180) lat2 = 179;

                    float s = dist_metres / time_secs;
                    if (s < MAX_SPEED) {
                        speeds[lng2][lat2] += s;
                        hits[lng2][lat2]++;
                    }
                }
            }
        }
        else {
            current_platform_number = data[i].platform_number;
        }
    }

    // update averages
    for (unsigned int lng = 0; lng < 360; lng++) {
        for (unsigned int lat = 0; lat < 180; lat++) {
            if (hits[lng][lat] > 0) {
                speeds[lng][lat] /= hits[lng][lat];
            }
        }
    }
}

// Uses consecutive movements of platforms to calculate average speed
// for each location on the map
void argo::calculate_speeds(vector<argodata> &data,
                            vector<vector<float> > &speeds)
{
    // create a 2D array
    speeds.clear();
    for (unsigned int lng = 0; lng < 360; lng++) {
        vector<float> v1;
        for (unsigned int lat = 0; lat < 180; lat++) {
            v1.push_back(0);
        }
        speeds.push_back(v1);
    }

    sort(data);

    vector<vector<float> > subprocess_speeds[ARGO_CPU_CORES];
#pragma omp parallel for
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        unsigned int data_start = i * data.size() / ARGO_CPU_CORES;
        unsigned int data_end = (i+1) * data.size() / ARGO_CPU_CORES;
        calculate_speeds_subprocess(data, data_start, data_end,
                                    subprocess_speeds[i]);
    }

    // sum the results
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        for (unsigned int lng = 0; lng < 360; lng++) {
            for (unsigned int lat = 0; lat < 180; lat++) {
                speeds[lng][lat] += subprocess_speeds[i][lng][lat];
            }
        }
    }

    // divide by number of cores
    for (unsigned int lng = 0; lng < 360; lng++) {
        for (unsigned int lat = 0; lat < 180; lat++) {
            speeds[lng][lat] /= ARGO_CPU_CORES;
        }
    }
}

void argo::calculate_currents_subprocess(vector<argodata> &data,
                                         unsigned int data_start,
                                         unsigned int data_end,
                                         vector<vector<vector<int> > > &currents)
{
    // create a 2D array
    currents.clear();
    for (unsigned int lng = 0; lng < 360; lng++) {
        vector<vector<int> > v1;
        for (unsigned int lat = 0; lat < 180; lat++) {
            vector<int> v2;
            // direction histogram, 8 degree increments
            for (unsigned int ang = 0; ang < 45; ang++) {
                v2.push_back(0);
            }
            v1.push_back(v2);
        }
        currents.push_back(v1);
    }

    int current_platform_number=-1;
    int lng2, lat2, angle_index;
    float angle, dlat, dlng, dist2;
    const float max_dist = 5*5;
    for (unsigned int i = data_start; i < data_end; i++) {
        // same platform
        if (data[i].platform_number == current_platform_number) {
            // consecutive cycles
            if (data[i].cycle_number == data[i-1].cycle_number+1) {
                // distance moved
                dlat = data[i].latitude - data[i-1].latitude;
                dlng = data[i].longitude - data[i-1].longitude;
                dist2 = (dlat*dlat) + (dlng*dlng);
                if (dist2 < max_dist) {
                    // direction of motion
                    angle = (float)acos(dlat/sqrt(dist2));
                    if (dlng<0) angle = (2*3.1415927f)-angle;
                    // latitude and longitude array index
                    lng2 = (int)(data[i-1].longitude+180.5f);
                    lat2 = (int)(data[i-1].latitude+90.5f);
                    if (lat2<0) lat2=0;
                    if (lat2>=180) lat2=179;
                    if (lng2<0) lng2 = 0;
                    if (lng2>=360) lng2 = 359;
                    // index within the direction histogram
                    angle_index = (int)(angle * 180 / 3.1415927)/8;
                    if (angle_index<0) angle_index += 45;
                    if (angle_index>=45) angle_index -= 45;
                    if ((angle_index>=0) && (angle_index<45)) {
                        // update
                        for (int dx = -1; dx <= 1; dx++) {
                            int lng3 = lng2 + dx;
                            if (lng3<0) lng3 += 360;
                            if (lng3>=360) lng3 -= 360;
                            for (int dy = -1; dy <= 1; dy++) {
                                int lat3 = lat2 + dy;
                                if (lat3<0) lat3 += 180;
                                if (lat3>=180) lat3 -= 180;
                                currents[lng3][lat3][angle_index]++;
                                if ((dx==0) && (dy==0)) {
                                    currents[lng3][lat3][angle_index]+=2;
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            current_platform_number = data[i].platform_number;
        }
    }
}

// Uses consecutive movements of platforms to calculate current direction histograms
// for each location on the map
void argo::calculate_currents(vector<argodata> &data,
                              vector<vector<vector<int> > > &currents)
{
    // create a 2D array
    currents.clear();
    for (unsigned int lng = 0; lng < 360; lng++) {
        vector<vector<int> > v1;
        for (unsigned int lat = 0; lat < 180; lat++) {
            vector<int> v2;
            // direction histogram, 8 degree increments
            for (unsigned int ang = 0; ang < 45; ang++) {
                v2.push_back(0);
            }
            v1.push_back(v2);
        }
        currents.push_back(v1);
    }

    sort(data);

    vector<vector<vector<int> > > subprocess_currents[ARGO_CPU_CORES];
#pragma omp parallel for
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        unsigned int data_start = i * data.size() / ARGO_CPU_CORES;
        unsigned int data_end = (i+1) * data.size() / ARGO_CPU_CORES;
        calculate_currents_subprocess(data, data_start, data_end,
                                      subprocess_currents[i]);
    }

    // sum the results
    for (unsigned int i = 0; i < ARGO_CPU_CORES; i++) {
        for (unsigned int lng = 0; lng < 360; lng++) {
            for (unsigned int lat = 0; lat < 180; lat++) {
                for (unsigned int ang = 0; ang < 45; ang++) {
                    currents[lng][lat][ang] +=
                        subprocess_currents[i][lng][lat][ang];
                }
            }
        }
    }

    // divide by number of cores
    for (unsigned int lng = 0; lng < 360; lng++) {
        for (unsigned int lat = 0; lat < 180; lat++) {
            for (unsigned int ang = 0; ang < 45; ang++) {
                currents[lng][lat][ang] /= ARGO_CPU_CORES;
            }
        }
    }
}

float argo::sigmat(float temperature, float salinity)
{
    double S0,E1,E2,B1,B2;
    double t = (double)temperature;
    double s = (double)salinity;

    S0=((6.76786136E-6*s - 4.8249614E-4)*s+0.814876577)*s - 0.0934458632;
    E1=(((-1.4380306E-7*t - 0.00198248399)*t-0.545939111)*t + 4.53168426)*t;
    B1=((-1.0843E-6*t + 9.8185E-5)*t - 0.0047867)*t + 1.0;
    B2=((1.667E-8*t - 8.164E-7)*t + 1.803E-5)*t;
    E2=(B2*S0+B1)*S0;
    return (float)(E1/(t + 67.26) + E2);
}

float argo::density(float temperature, float salinity)
{
    return (sigmat(temperature, salinity)/1000.0f) + 1;
}

float argo::ocean_heat_content(float temperature, float salinity, float pressure)
{
    const float specific_heat_of_sea_water = 3.996f * 1000;
    return density(temperature, salinity)*specific_heat_of_sea_water*temperature;
}

// Transfer data from the values array to the result array
// sub-sampling if necessary
void argo::transfer_values(int &records, float * values, float * result)
{
    if (records < MAX_PROFILE_RECORDS) {
        // copy to the result array
        memcpy((void*)result,(void*)values,sizeof(float)*records);
    }
    else {
        // subsample to the result array
        for (int i = 0; i < MAX_PROFILE_RECORDS-1; i++) {
            result[i] = values[i*(records-1)/(MAX_PROFILE_RECORDS-1)];
        }
        // ensure that the full range is preserved
        result[MAX_PROFILE_RECORDS-1] = values[records-1];
        // new smaller size
        records = MAX_PROFILE_RECORDS;
    }
}


// Load a NetCDF formatted file and return the data
// Note: it's easier to use ncdump, then parse the text file
// rather than to use libnetcdf-dev, which seems broken
int argo::load_netcdf(string filename, vector<argodata> &data, bool verbose,
                      int start_year, int end_year, int month,
                      vector<float> &area, vector<int> &platforms,
                      string temporary_filename,
                      FILE * fp_export)
{
    argodata result;
    char command[256];
    char line[256],str[1024];
    vector<float> list;
    FILE * fp;
    int section=0;
    int pressure_records = 0;
    int pressure_records_qc = 0;
    bool pressure_qc = false;
    int temperature_records = 0;
    int temperature_records_qc = 0;
    bool temperature_qc = false;
    int salinity_records = 0;
    int salinity_records_qc = 0;
    bool salinity_qc = false;
    int oxygen_records = 0;
    int oxygen_records_qc = 0;
    bool oxygen_qc = false;
    float values[1024];
    bool reject_profile = false;

    result.oxygen[0]=-9999.0f;
    sprintf(command,"ncdump %s > %s",filename.c_str(),
            temporary_filename.c_str());

    if (system(command)!=0) return -1;
    fp = fopen((char*)temporary_filename.c_str(),"r");
    if (fp==NULL) return -1;

    while ((!feof(fp)) && (!reject_profile)) {
        if (fgets(line , 255 , fp) != NULL ) {
            remove_preceding_spaces(line);
            if (section==0) {
                if (strncmp(line,"data:",5)==0) {
                    section = 1;
                }
            }
            else {
                if (result.wmo_inst_type==0) {
                    if (extract_parameter_integer((char*)"wmo_inst_type",
                                                  line,
                                                  result.wmo_inst_type,fp)) {
                        if (verbose) printf("WMO instrument type: %d\n",
                                            result.wmo_inst_type);
                    }
                }
                if (result.direction==0) {
                    if (extract_parameter((char*)"direction",line,
                                          str, 1024, fp)) {
                        result.direction = toupper(str[0]);
                        if (verbose) printf("Direction: %c\n",
                                            result.direction);
                    }
                }
                if (result.platform_number==0) {
                    if (extract_parameter_integer((char*)"platform_number",
                                                  line,
                                                  result.platform_number,
                                                  fp)) {
                        bool platform_found = true;
                        if (platforms.size()>0) {
                            platform_found = false;
                            for (unsigned int p = 0;
                                 p < platforms.size(); p++) {
                                if (platforms[p]==result.platform_number) {
                                    platform_found = true;
                                    break;
                                }
                            }
                        }
                        if (!platform_found) {
                            fclose(fp);
                            return -1;
                        }
                        if (verbose) {
                            printf("platform_number: %d\n",
                                   result.platform_number);
                        }
                    }
                }
                /*
                if (extract_parameter((char*)"project_name",line, str, 1024, fp)) {
                    result.project_name = string(str);
                    if (verbose) printf("project_name: '%s'\n",result.project_name.c_str());
                }
                if (extract_parameter((char*)"positioning_system",line, str, 1024, fp)) {
                    result.positioning_system = string(str);
                    if (verbose) printf("positioning_system: '%s'\n",result.positioning_system.c_str());
                }
                */
                if (result.data_centre[0]==0) {
                    if (extract_parameter((char*)"data_centre",line,
                                          result.data_centre, 4, fp)) {
                        if (verbose) printf("data_centre: '%s'\n",
                                            result.data_centre);
                    }
                }
                if (result.cycle_number==0) {
                    if (extract_parameter_integer((char*)"cycle_number",
                                                  line,
                                                  result.cycle_number,fp)) {
                        if (verbose) printf("cycle_number: %d\n",
                                            result.cycle_number);
                    }
                }
                if (result.latitude==-9999) {
                    if (extract_parameter_float((char*)"latitude",
                                                line, result.latitude,fp)) {
                        if ((area.size()==4) && (result.longitude>-9999)) {
                            if (!inside_area(result,area)) {
                                fclose(fp);
                                return -1;
                            }
                        }
                        if (verbose) printf("latitude: %.3f\n",
                                            result.latitude);
                    }
                }
                if (result.longitude==-9999) {
                    if (extract_parameter_float((char*)"longitude",
                                                line, result.longitude,fp)) {
                        if ((area.size()==4) && (result.latitude>-9999)) {
                            if (!inside_area(result,area)) {
                                fclose(fp);
                                return -1;
                            }
                        }
                        if (verbose) printf("longitude: %.3f\n",result.longitude);
                    }
                }

                if (result.year==0) {
                    if (extract_parameter_date_time((char*)"date_creation", line,
                                                    result.year, result.month,
                                                    result.day,
                                                    result.hour, result.min,fp)) {
                        if (((result.year<start_year) || (result.year>end_year)) ||
                            ((month>0) && (result.month!=month))) {
                            fclose(fp);
                            return -1;
                        }
                        if (verbose) {
                            printf("%d-%d-%d %d:%d\n",
                                   result.year,result.month,result.day,
                                   result.hour,result.min);
                        }
                    }
                }
                // pressure values
                if (pressure_records==0) {
                    if (!extract_series((char*)"pressure_adjusted =",line,
                                        pressure_records, (float*)values,
                                        1024, fp)) {
                        if (!extract_series((char*)"pres_adjusted =",line,
                                            pressure_records, (float*)values,
                                            1024, fp)) {
                            if (!extract_series((char*)"pressure =",line,
                                                pressure_records, (float*)values,
                                                1024, fp)) {
                                extract_series((char*)"pres =",line,
                                               pressure_records, (float*)values,
                                               1024, fp);
                            }
                        }
                    }
                    if (pressure_records > 0) {
                        pressure_records_qc = pressure_records;
                        transfer_values(pressure_records,values,
                                        (float*)&result.pressure);
                        if (verbose) {
                            printf("pressure: ");
                            for (int i = 0; i < pressure_records; i++) {
                                printf("%.2f, ",result.pressure[i]);
                            }
                            printf("\n");
                        }
                    }
                }
                // pressure quality control
                if ((!pressure_qc) && (pressure_records>0)) {
                    if (!extract_parameter((char*)"pressure_adjusted_qc",line,
                                           str, 1024, fp)) {
                        if (!extract_parameter((char*)"pres_adjusted_qc",line,
                                               str, 1024, fp)) {
                            if (!extract_parameter((char*)"pressure_qc",line,
                                                   str, 1024, fp)) {
                                extract_parameter((char*)"pres_qc",line,
                                                  str, 1024, fp);
                            }
                        }
                    }
                    if ((int)strlen(str)==pressure_records_qc) {
                        for (int i = 0; i < pressure_records_qc; i++) {
                            if (str[i]!=QC_GOOD_DATA) {
                                fclose(fp);
                                return -1;
                            }
                        }
                        pressure_qc=true;
                    }
                }
                // salinity values
                if (salinity_records==0) {
                    if (!extract_series((char*)"salinity_adjusted =",line,
                                        salinity_records, (float*)values,
                                        1024, fp)) {
                        if (!extract_series((char*)"psal_adjusted =",line,
                                            salinity_records, (float*)values,
                                            1024, fp)) {
                            if (!extract_series((char*)"salinity =",line,
                                                salinity_records, (float*)values,
                                                1024, fp)) {
                                extract_series((char*)"psal =",line,
                                               salinity_records, (float*)values,
                                               1024, fp);
                            }
                        }
                    }
                    if (salinity_records > 0) {
                        salinity_records_qc = salinity_records;
                        transfer_values(salinity_records,values,
                                        (float*)&result.salinity);
                        if (verbose) {
                            printf("salinity: ");
                            for (int i = 0; i < salinity_records; i++) {
                                printf("%.2f, ",result.salinity[i]);
                            }
                            printf("\n");
                        }
                    }
                }
                // salinity quality control
                if ((!salinity_qc) && (salinity_records>0)) {
                    if (!extract_parameter((char*)"salinity_adjusted_qc",line,
                                           str, 1024, fp)) {
                        if (!extract_parameter((char*)"dsal_adjusted_qc",line,
                                               str, 1024, fp)) {
                            if (!extract_parameter((char*)"salinity_qc",line,
                                                   str, 1024, fp)) {
                                extract_parameter((char*)"psal_qc",line,
                                                  str, 1024, fp);
                            }
                        }
                    }
                    if ((int)strlen(str)==salinity_records_qc) {
                        for (int i = 0; i < salinity_records_qc; i++) {
                            if (str[i]!=QC_GOOD_DATA) {
                                fclose(fp);
                                return -1;
                            }
                        }
                        salinity_qc=true;
                    }
                }

                // temperature
                if (temperature_records==0) {
                    if (!extract_series((char*)"temperature_adjusted =",line,
                                        temperature_records, (float*)values,
                                        1024, fp)) {
                        if (!extract_series((char*)"temp_adjusted =",line,
                                            temperature_records, (float*)values,
                                            1024, fp)) {
                            if (!extract_series((char*)"temperature =",line,
                                                temperature_records, (float*)values,
                                                1024, fp)) {
                                extract_series((char*)"temp =",line,
                                               temperature_records,
                                               (float*)values, 1024, fp);
                            }
                        }
                    }
                    if (temperature_records > 0) {
                        temperature_records_qc = temperature_records;
                        transfer_values(temperature_records,
                                        values,(float*)&result.temperature);
                        for (int i = 0; i < temperature_records; i++) {
                            if ((result.temperature[i] < 0.1f) || (result.temperature[i] > 40)) {
                                reject_profile = true;
                                break;
                            }
                        }

                        if (verbose) {
                            printf("temperature: ");
                            for (int i = 0; i < temperature_records; i++) {
                                printf("%.2f, ",result.temperature[i]);
                            }
                            printf("\n");
                        }
                    }
                }
                // temperature quality control
                if ((!temperature_qc) && (temperature_records>0)) {
                    if (!extract_parameter((char*)"temperature_adjusted_qc",line,
                                           str, 1024, fp)) {
                        if (!extract_parameter((char*)"temp_adjusted_qc",line,
                                               str, 1024, fp)) {
                            if (!extract_parameter((char*)"temperature_qc",line,
                                                   str, 1024, fp)) {
                                extract_parameter((char*)"temp_qc",line,
                                                  str, 1024, fp);
                            }
                        }
                    }
                    if ((int)strlen(str)==temperature_records_qc) {
                        for (int i = 0; i < temperature_records_qc; i++) {
                            if (str[i]!=QC_GOOD_DATA) {
                                fclose(fp);
                                return -1;
                            }
                        }
                        temperature_qc=true;
                    }
                }

                // oxygen values
                if (oxygen_records==0) {
                    if (!extract_series((char*)"oxygen_adjusted =",line,
                                        oxygen_records, (float*)values,
                                        1024, fp)) {
                        if (!extract_series((char*)"doxy_adjusted =",line,
                                            oxygen_records, (float*)values,
                                            1024, fp)) {
                            if (!extract_series((char*)"oxygen =",line,
                                                oxygen_records, (float*)values,
                                                1024, fp)) {
                                extract_series((char*)"doxy =",line,
                                               oxygen_records, (float*)values,
                                               1024, fp);
                            }
                        }
                    }
                    if (oxygen_records > 0) {
                        oxygen_records_qc = oxygen_records;
                        transfer_values(oxygen_records,
                                        values,(float*)&result.oxygen);
                        if (verbose) {
                            printf("oxygen: ");
                            for (int i = 0; i < oxygen_records; i++) {
                                printf("%.2f, ",result.oxygen[i]);
                            }
                            printf("\n");
                        }
                    }
                }
                // oxygen quality control
                if ((!oxygen_qc) && (oxygen_records>0)) {
                    if (!extract_parameter((char*)"oxygen_adjusted_qc",line,
                                           str, 1024, fp)) {
                        if (!extract_parameter((char*)"doxy_adjusted_qc",line,
                                               str, 1024, fp)) {
                            if (!extract_parameter((char*)"oxygen_qc",line,
                                                   str, 1024, fp)) {
                                extract_parameter((char*)"doxy_qc",line,
                                                  str, 1024, fp);
                            }
                        }
                    }

                    if ((int)strlen(str)==oxygen_records_qc) {
                        oxygen_qc=true;
                        for (int i = 0; i < oxygen_records_qc; i++) {
                            if ((str[i]!=QC_GOOD_DATA) && (str[i]!=QC_NONE)) {
                                oxygen_qc = false;
                                break;
                            }
                        }
                    }
                }

            }
        }
    }
    fclose(fp);

    // If there are bad oxygen records
    if ((!oxygen_qc) && (oxygen_records == temperature_records) &&
        (temperature_records > 0)) {
        oxygen_records = 0;
        result.oxygen[0] = -9999.0f;
    }

    // only use records which have passed quality control as being "good data"
    if ((result.platform_number > 0) &&
        (pressure_qc) && (temperature_qc) && (salinity_qc) &&
        (temperature_records > 0) &&
        (temperature_records == pressure_records) &&
        (temperature_records == salinity_records) &&
        ((oxygen_records == temperature_records) || (oxygen_records == 0))) {
        result.records = pressure_records;
        if (fp_export==NULL) {
            data.push_back(result);
        }
        else {
            export_to_file(fp_export, result);
        }
        return 0;
    }

    return -1;
}
